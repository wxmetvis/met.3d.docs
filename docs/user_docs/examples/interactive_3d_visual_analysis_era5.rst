Interactive 3-D visual analysis of ERA5 reanalysis data
=======================================================

The visualization software Met.3D can be used for interactive visual exploration of 
meteorological phenomena in ERA5. 
This page provides a short guide towards using Met.3D for interactive 3D visual analysis of ERA5 data.


Introduction
------------

The ERA5 reanalysis dataset (ECMWF Reanalysis 5th Generation) is the most recent 
global reanalysis data covering the time-period from 1950 until today. 
The data is obtained by combining a numerical weather prediction model with empirical observations 
(4DVar data assimilation in CY41R2 of ECMWFs IFS). 
It contains estimates of atmospheric variables, such as air temperature, pressure, and wind, 
as well as surface variables such as rainfall and soil moisture. 
The meteorological variables are available with hourly temporal resolution on a global data grid 
with approximate horizontal resolution of 31km and 137 vertical model levels.

.. note:: 

    For more information about ERA5, please see the ECMWFs documentation: 
    `<https://confluence.ecmwf.int/display/CKB/ERA5%3A+data+documentation>`_


Met.3D can be used for interactive visual exploration of meteorological phenomena in ERA5. 
Examples of available visualization methods include:

* Sliding of horizontal and vertical cross-sections through 3D data-cubes.
* Computation of 3-D iso-surfaces with interactive variation of iso-values.
* Direct volume rendering of clouds using ray-casting.
* Flow visualization via wind barbs and trajectories.

In contrast to the widely-used visualization software packages, 
Met.3D allows visual data exploration at interactive frame-rates, 
enabling you to quickly discover interesting features and explore system dependencies.


Step by step procedure
----------------------

The following steps are necessary for analyzing ERA5 data with Met.3D.

#. ERA5 data acquisition
#. Pre-processing of ERA5 data to a regular grid (Met.3D currently requires data on regular grids)
#. Visual exploration of ERA5 using the graphical user interface of Met.3D


1. ERA5 data acquisition
~~~~~~~~~~~~~~~~~~~~~~~~

ERA5 data is freely available for download.

* For a detailed description on how to download ERA5 data, please see: 
  `<https://confluence.ecmwf.int/display/CKB/How+to+download+ERA5>`_.
* Alternatively, the ERA5 data is also available at the German Climate Computing Center (DKRZ). 
* For details, please see: `<https://www.dkrz.de/up/services/data-management/projects-and-cooperations/era>`_


2. Pre-processing of ERA5 to a regular grid
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Met.3D requires meteorological (simulation) data on a regularly spaced grid.
The :doc:`../data_handling` section of this documentation provides an overview about the supported data and file types.

* If you download ERA5 data on a regular grid, as provided by the ECMWFS download options, 
  you should be able to directly load the data into Met.3D without any other processing, 
  and start your interactive visual data exploration.
* If the data is on a Gaussian or Spectral grid, 
  you need to interpolate the data to a regular grid before you can analyze it using Met.3D.

The Climate Data Operators (CDO) provide a convenient way to interpolate ERA5 data onto a regular grid.

.. note:: 
    For a detailed documentation of CDO, please see: `<https://code.mpimet.mpg.de/projects/cdo/>`_.

The section :doc:`../data_handling/remapping_spectral_grid_to_regular_grid` 
of this documentation outlines the procedure for the conversion of spectral 
or gaussian grids to regular lat lon grids.

The section :doc:`../data_handling/remapping_spectral_grid_to_regular_grid` of this documentation 
outlines the procedure for the conversion of a regular grid to a stereographic grid.


3. Visual exploration of ERA5 using the features of Met.3D
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Start your 3D interactive visual data analysis. The :doc:`../getting_started` section summarizes first steps.
