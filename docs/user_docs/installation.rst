Installation
============

System requirements
-------------------

.. note:: 
    Met.3D is software from university research. 
    We make it freely available as open-source to the community, however, 
    keep in mind that we only have limited resources for software development and support. 
    We do our best to make the code stable and easy to use, however, 
    please don't expect polished commercial-grade software. 
    As with all open-source projects, help from anyone who would like to contribute is much appreciated.

Met.3D is implemented in C++ (11) using OpenGL (v4.3 or higher) as graphics API and Qt (v5) for the GUI, 
which in theory means that it should compile on all major platforms that support these. 
There are, however, some caveats, and currently (2024), we are only working with Linux. 
We are hence only providing installation instructions for Linux. 
If you are a Windows expert, help would be much appreciated figuring out how to compile Met.3D 
on this system. Concerning macOS, to the best of our knowledge, 
Apple only supports OpenGL up to version 4.1 - since Met.3D uses functionality up to version 4.3, 
adapting the code to compile on macOS would require more effort. 
Again, if you are an expert, help would be much appreciated.


**Hardware requirements** for Met.3D are:

* The main requirement is a graphics card (GPU) supporting OpenGL 4.3. 
  Note that since we only have Nvidia hardware, we haven't been able to test the code on AMD and Intel GPUs.

* The graphics card needs to have enough video memory to accommodate all data that 
  should be visualized in a single image 
  (e.g., if the size of your simulation data is on the order of 500 MB per variable and time step, 
  and you would like to visualize u and v wind components, temperature and cloud cover simultaneously, 
  you will need at least 5x500MB = 2.5 GB (Met.3D also requires pressure) of video memory - 
  more memory will increase performance during animation).

* The main system memory needs to be at least as large as the video memory. 
  Data is cached on system memory, hence, the more, the better the performance.

* Met.3D makes some use of CPU multithreading to parallelize computations. 
  Multi-core CPUs will hence also increase performance.


.. _install_linux:

Linux
-----

We provide a :doc:`binary distribution for Linux as a conda package <installation/conda>`, 
which is the simplest way to use Met.3D. Note that this distribution corresponds to the 
latest tagged version in the `"master" branch in the git repository 
<https://gitlab.com/wxmetvis/met.3d/-/tree/master>`_.

If you would like to compile the software yourself 
(e.g., to use a different version than the "master" branch or to make changes by yourself), 
detailed :doc:`installation instructions can be found here <installation/src_conda>`.

.. toctree::
    :maxdepth: 1

    installation/conda
    installation/src_conda
    installation/src_linux


Windows
-------

As noted above, Met.3D should theoretically compile and run on Windows, however, 
some adaptations will likely be necessary. 
If you are a Windows expert, we would greatly appreciate any contributions
to help make Met.3D run natively on Windows.
In the meantime, we provide experimental instructions on how Met.3D can be installed
under Windows using the WSL 2 system:

.. toctree:: 
  :maxdepth: 1

  installation/windows_wsl

MacOS
-----

As noted above, adapting Met.3D to macOS will likely require some effort. 
Again, if you would like to contribute, help would be much appreciated.