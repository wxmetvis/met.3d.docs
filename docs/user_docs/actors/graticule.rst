Graticule
=========

.. _fig_graticule_actor:
.. figure:: ./figs/graticule/actorref_graticule.webp
    :figwidth: 100%
    :align: left
    :alt: A figure should appear here

    Graticule actor.


Description
-----------

The graticule actor draws a graticule and coast and border-lines into a scene.

* The distance and colour of the graticule lines can be customized.
* Coast and border-lines are read from a shapefile 
  (in above image data from `Natural Earth`_ is used. Other shapefile datasets can be used as well).


Adjusting graticule spacing
---------------------------

.. figure:: ./figs/graticule/graticule_spacing.webp
    :figwidth: 100%
    :align: left
    :alt: A figure should appear here

* Open **Graticule** → **actor properties**
* The user can adjust the start coordinate, end coordinate, the spacing of the graticule, and the graticule labels
* To change the spacing of the graticule or labels of the graticule click into the corresponding "Value" box 
  and adjust the values [western boundary,eastern boundary,spacing] or [southern boundary,northern boundary,spacing]
* After the adjustment of the properties click the re-compute graticule "execute" button

.. _`Natural Earth`: http://www.naturalearthdata.com/