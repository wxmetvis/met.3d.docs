.. _vsec_actor:

Vertical cross section
======================

.. _fig_vertical_cross_section:
.. figure:: ./figs/vertical_cross_section/actorref_vsec.webp
    :figwidth: 100%
    :align: left
    :alt: A figure should appear here

    Vertical cross-section actor, together with a transfer function.


Description
-----------

The vertical section actor draws vertical 2D cross-sections of multiple forecast data fields along an arbitrary path.

* The actor supports multiple forecast data fields.
* Each data field can be rendered as line contours or filled contours.
* A vertical section can be drawn along arbitrarily many waypoints.
* The waypoints can be interactively moved by the user 
  (to move the waypoints, enable a scene view’s “interaction mode” by double-click on the scene).
* Waypoints can be exported in “flight track file” for the `DLR Mission Support System`_ (MSS), 
  or read from files exported by the MSS.
* Bottom and top pressure limits of the section can be adjusted.


Adding waypoints
----------------

The waypoints manager can be activated by using F2 or from the menu **View** → **Show waypoints**. 
The waypoints manager has options to insert, delete or edit waypoints. 
Also, it is possible to export or import the waypoints stored as a file.

.. figure:: ./figs/vertical_cross_section/waypoints.webp
    :figwidth: 100%
    :align: left
    :alt: A figure should appear here

.. _`DLR Mission Support System`: http://mss.readthedocs.io/