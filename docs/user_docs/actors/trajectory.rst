Trajectory actor
================

.. _fig_trajectory_actor:
.. figure:: ./figs/trajectory/actorref_trajectories.webp
    :figwidth: 100%
    :align: left
    :alt: A figure should appear here

    Trajectory actor, together with a transfer function.


Description
-----------

The trajectory actor draws Lagrangian particle trajectories (path lines) or streamlines as 3-D tubes into a scene.
Besides the tubes, particle positions at selected times can also be rendered.
Trajectories can either be computed on the fly, or loaded as precomputed trajectories from a supported NetCDF format.

**Selected features:**
  
* Supports 3-D visual analysis of precomputed and dynamically computed particle trajectories or particle-clouds.
* A converter is available for LAGRANTO trajectories (please contact us).
* Trajectories can be drawn as 3-D tubes, colored by pressure elevation
  or any other variable that is provided as auxiliary input data.
* The individual positions of the trajectory particles can also be drawn, 
  either for the entire trajectories or for individual time steps.
* Different computation modes are available to compute trajectories in Met.3D.
* Trajectory filters can be used:
  For example, they can be filtered by ascent (i.e., change in pressure)
  which is particularly useful to extract trajectories being part of a rapid
  ascending or descending air mass (tropical cyclones, warm conveyor belts).
* For the computation, different sources can be selected as seed point sources
  for the trajectories. Furthermore, different filters can be applied to the
  chosen set of seed point sources.


Supported trajectory data
-------------------------

Precomputed trajectories in NetCDF format
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Precomputed trajectories can be read from NetCDF files,
as outlined in :ref:`here<loading_precomp_traj_data>`.

Note that to enable color mapping on precomputed trajectories according to 
precomputed auxiliary data variables, the auxiliary data must be included as a variable 
in the NetCDF-CF file with an attribute "auxiliary_data" set to "yes" and the same 
dimensions as the trajectory vertex information.

Trajectories computed within Met.3D
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

For computation of trajectories (streamlines or pathlines) in Met.3D, 
gridded 3-D fields of the three wind velocity components are required
(:math:`u, v` in :math:`m\,s^{-1}`, :math:`\omega` in :math:`Pa\,s^{-1}`).
Optionally, additional data fields can be used as auxiliary data
for color mapping or for filtering seed points. Please also refer to 
:ref:`how to load datasets<loading_data>`.


Choosing a dataset from the trajectory actor
--------------------------------------------

When creating a trajectory actor in Met.3D, you can switch the mode 
(first setting in the actor properties) to either precomputed or computed.
If you select the precomputed mode, you must choose a trajectory data source in the
precomputation settings submenu. If no dataset is available, the precomputed
trajectories could not be loaded; check the log output for troubleshooting.

If you opt to let Met.3D compute trajectories, ensure you have loaded a 
gridded dataset. Select the corresponding 3-D wind components in the computation 
settings. Additionally, remember to select a valid bounding box. When computing 
trajectories, you must also add at least one seed point source where the trajectories 
should start (refer to the section below for more details).


Adding seed points and point filters for “on-the-fly” computed trajectories
---------------------------------------------------------------------------

To compute trajectories with Met.3D, you need to specify starting points.
To add a seed point source, open the computation settings and click “Add seed source.”
A window will appear displaying all available seed sources.
Currently, you can generate seed points from:

* :doc:`A horizontal cross-section</user_docs/actors/horizontal_cross_section>`:
  This option allows you to set the latitude and longitude spacing for the seed points.
* :doc:`A vertical cross-section</user_docs/actors/vertical_cross_section>`: 
  This allows you to specify both the pressure spacing (z direction) and the horizontal spacing along the cross-section.
* :doc:`A moveable pole actor</user_docs/actors/moveable_poles>`: This lets you define the pressure spacing along the poles of the actor.
* A bounding box: Here, you can choose the latitude, longitude, and vertical spacing of seed points within the bounding box.

If no sources are available, ensure one is created (e.g., by setting up one of the actors).
New seed sources can be implemented as well; consult the developer documentation.

Next, you can add point filters to the computation pipeline.
These filters take a list of points as input and return modified points.
Filters can create subsets of input points or alter them completely.
In the pipeline, points from the seed sources pass through various point filters before reaching the trajectory computation module. 
Supported filters include:

- A bounding box filter: Filters points by a selected bounding box.
- A threshold filter: Allows selection of a gridded data variable and setting of a lower and/or upper threshold, retaining only seed points within the range.

To add a filter, click “Add point filter” in the computation menu.
Note that you can add multiple filters of the same type.


Configuring trajectory visualization
------------------------------------

Trajectory data can be visualized in several ways:

* **Tubes** (as in the sample image above), either showing the entire trajectories (i.e. all time steps) 
  or only part of the trajectories.
* **Particles** at a specified time step.
* Tubes and particles **combined**.

To display trajectory data, you need to select a valid bounding box.
  
.. figure:: ./figs/trajectory/traj_ui.webp
    :figwidth: 100%
    :align: left
    :alt: A figure should appear here

* To switch between tubes and particles, select the corresponding setting in the "render mode" property.
* If the dataset provides auxiliary variables that have been traced along the trajectories, 
  you can switch colouring using the "colour according to" and "auxiliary variable" properties. 
  You also need to select a suitable transfer function that fits the range of the variable values. 
  See :doc:`transfer_function_color_map`.
* You can configure tube and particle radius using the properties "tube/sphere radius".

Working with "on-the-fly" computed trajectories and streamlines
---------------------------------------------------------------

To configure trajectory/streamline computation:

* The required properties are available in the "trajectory computation" property group.
* In the "seed points" group, select "add seed actor" to add an actor that will provide the seed points. 
  In the example below, a pole actor is chosen, and a number of vertical levels for the seeds are specified.
* Select whether trajectories or streamlines shall be computed ("type").
* Select the integration method (Euler integration as well as Runge-Kutta 4th-order integration is available).
* Select the interpolation method - "as LAGRANTO v2" will use the same interpolation as the `LAGRANTO model`_, 
  alternatively, simple trilinear interpolation can be used.
* For trajectories, select the integration time (e.g. 48 hours from the start time).
* For streamlines, select a segment length (for the numerical integration) and a number of segments that shall be computed.
* Example:

.. figure:: ./figs/trajectory/traj_seeds.webp
    :figwidth: 100%
    :align: left
    :alt: A figure should appear here

To interactively explore the flow field:

* Simply switch to the "actor interaction mode" (double-click in the scene view),
  grab the pole (or other seed actor), and move it around.

.. warning:: 
    The computational performance of re-computing trajectories can be low, depending on your hardware. 
    Trajectories at the current time are computed on the CPU, 
    all time-steps required for the computation need to fit into CPU main memory. 
    Please carefully monitor the "processing" signal in the upper right corner of the Met.3D window 
    as well as the console output. 
    We suggest that you start with only few trajectories 
    and slowly increase their number to get a feeling of how much your hardware can handle.


Trajectory filtering
--------------------

Computed trajectories can be filtered by adding trajectory filters from the trajectory 
actor properties. These filters are applied both to precomputed and on-the-fly computed
trajectories. Currently implemented filters are:

* :ref:`trajectory ascent filtering<ascent_filter>`.
* ...


..  _ascent_filter:

Filtering trajectories according to their ascent
------------------------------------------------

This functionality is mainly of interest to visually analyze Warm Conveyor Belts (WCBs). 
Also see our `publication on this topic <https://gmd.copernicus.org/articles/8/2355/2015/>`_.

* In the trajectory filter settings set the desired elevation and time intervals. 
  This will only show trajectories that ascent at least PP hPa in HH hours 
  (e.g., a typical value for WCBs is 500 hPa in 48 hours).
* Example:
  
.. figure:: ./figs/trajectory/traj_filter.webp
    :figwidth: 100%
    :align: left
    :alt: A figure should appear here




.. _`LAGRANTO model`: https://gmd.copernicus.org/articles/8/2569/2015/