Volume raycaster (3D isosurfaces and DVR)
=========================================

.. _fig_volume_raycaster:
.. figure:: ./figs/volume_raycaster/actorref_volume.webp
    :figwidth: 100%
    :align: left
    :alt: A figure should appear here

    Volume isosurface actor, together with a transfer function.


Description
-----------

The volume actor renders (a) direct volume rendering or 
(b) (multiple) isosurfaces (shown above) of a data field and provides normal curve functionality (not shown).

* The actor renders multiple isosurfaces of a data field via raycasting.
* An isosurface can be coloured according to another data field 
  (e.g. an isosurface of wind speed can be coloured by temperature or pressure).
* Shadows of the isosurface and normal curves are rendered to the (earth) surface.
* Normal curves are supported.
* Interactive region contribution analysis is supported.


Render mode and rendered variables
------------------------------------------

Using the render mode option the user can change the render mode among the available render modes isosurface, 
Direct Volume rendering and bitfield rendering. 
Also, the observed variable and shading variable can be set to the appropriate option.

.. figure:: ./figs/volume_raycaster/volume_render_mode.webp
    :figwidth: 100%
    :align: left
    :alt: A figure should appear here


Isosurface raycaster parameters
---------------------------------------

The isosurface raycaster has options to choose the isovalue for which the visualization is intended. 
There are options to set the color, size of the sampling step as well as handling the shadows.

.. figure:: ./figs/volume_raycaster/volume_isosurface_settings.webp
    :figwidth: 100%
    :align: left
    :alt: A figure should appear here


Normal curve parameters
-----------------------

Met.3D provides the option to draw the normal curves between two isosurfaces or 
limit the normal curve length based on the number of line segments. 
It is possible to render the normal curves as tubes, lines or as boxes + slices. 
The user can also set the seed point spacing.

.. figure:: ./figs/volume_raycaster/volume_normal_curve_settings.webp
    :figwidth: 100%
    :align: left
    :alt: A figure should appear here


Direct volume rendering of radar data
-------------------------------------

Example and suitable settings for rendering radar data with DVR:

.. figure:: ./figs/volume_raycaster/radar_dvr.webp
    :figwidth: 100%
    :align: left
    :alt: A figure should appear here

    Settings used: step size=0,01, interaction ray step size=0,01, use dithering=true, dithering strength=1, reference scale unit=World, reference scale=4

Multiple datasets (radar sites) can be shown at the same time in a scene either by adding multiple actors, or by more multiple variables to the same scene:

.. figure:: ./figs/volume_raycaster/two_radar_vars.webp
    :figwidth: 100%
    :align: left
    :alt: Activate second observed variable to add another radar station 

    Example image of two (in this case copied) variables in the same scene

