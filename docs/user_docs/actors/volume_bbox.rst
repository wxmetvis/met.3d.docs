Volume bounding box
===================

.. _fig_volume_bbox_actor:
.. figure:: ./figs/volume_bbox/actorref_volumebbox.webp
    :figwidth: 100%
    :align: left
    :alt: A figure should appear here

    Volume bounding box actor.

The volume bounding box actor draws a bounding box into the scene. 
It can be used to illustrate the region covered by a data volume, or to increase spatial perception for horizontal sections.

.. note::
    The vertical tick marks of a bounding box are labelled according to pressure.