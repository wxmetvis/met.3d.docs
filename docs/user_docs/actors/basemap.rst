Basemap
=======

.. _fig_basemap_actor:
.. figure:: ./figs/basemap/actorref_basemap.webp
    :figwidth: 100 %
    :align: left
    :alt: A figure should appear here
    
    Basemap actor.


Description
-----------

As the name says, the base map actor draws a base map into a scene.

* Map data can be read from an arbitrary geo-referenced GeoTIFF file 
  (in the above image data from `Natural Earth`_ is used. Other GeoTIFF datasets can be used as well).
* The bounding box can be customised, i.e. the map file can be larger than the displayed region.
* Colour saturation of the loaded base map image can be adjusted (for example, to desaturate the image colours).


Loading a map
-------------

.. figure:: ./figs/basemap/basemap_load_map.webp
    :figwidth: 100%
    :align: left
    :alt: A figure should appear here
    

* Open **Base map** → **actor properties**
* Under the tab load map click on execute. 
  A file browser opens and you can choose an arbitrary GeoTIFF file from your file system. 


Adjusting map projection
------------------------

.. figure:: ./figs/basemap/basemap_projection.webp
    :figwidth: 100%
    :align: left
    :alt: A figure should appear here


* Open **Base map** → **actor properties** → **map projection support**
* You can choose between three different projection types: **cylindrical**, **rotated lat.-lon**. and **proj.org projection**
* Choose the projection according to your needs. 
  For rotated lat.-lon. projection you may need to adjust the **rotated north pole** option, 
  the coordinates of the rotated North Pole. 


.. _`Natural Earth`: http://www.naturalearthdata.com/