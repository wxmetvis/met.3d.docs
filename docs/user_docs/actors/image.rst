Image
=====

.. _fig_image_actor:
.. figure:: ./figs/image/actorref_image_actor.webp
    :figwidth: 100 %
    :align: left
    :alt: A figure should appear here
    
    Image actor.

Description
-----------

This actor draws an image on top of the scene.
It can be used to display logos or watermarks on images created using Met.3D.

* Supports multiple image file types.
* Can be placed freely on the viewport.
* Can be placed interactively in actor interaction mode by dragging it around.
* Can be scaled to a user specified scale.
* Can be anchored to different points in the viewport and always stays in the same relative position to that anchor.