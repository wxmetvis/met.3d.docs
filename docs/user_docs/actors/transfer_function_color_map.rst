1D transfer function (Colour map)
=================================

.. _fig_transferfunction_actor:
.. figure:: ./figs/transfer_function_colour_map/actorref_transferfunction.webp
    :figwidth: 100 %
    :align: left
    :alt: A figure should appear here
    
    1D transfer function (colour map) actor.


Description
-----------

The 1D transfer function actor provides a colour map that is (a) displayed in a scene, 
and that (b) can be used in other actors to map a data value to a colour.

* Data scalar range can be adjusted, as well as number of colour steps, labels, 
  tick marks and position of the colour map in the visualization.
* The actor provides a number of predefined colour maps, as well as Hue-Chroma-Luminance (HCL) colour maps. 
  For the latter, specification of HCL colour maps follows the website of Reto Stauffer.


Adjusting the value range
-------------------------

.. figure:: ./figs/transfer_function_colour_map/tf_value_range.webp
    :figwidth: 100%
    :align: left
    :alt: A figure should appear here


* Open **Transfer function scalar to colour (colour map)** → **actor properties** → **range**
* Set the **minimum** and **maximum** value according to the needs


Changing the colour map type
----------------------------

.. figure:: ./figs/transfer_function_colour_map/tf_colourmap_type.webp
    :figwidth: 100%
    :align: left
    :alt: A figure should appear here


* Open **Transfer function scalar to colour (colour map)** → **actor properties**
* Choose a **colourmap type** (options: "HLC", "Editor", "predefined", "HSV"). We recommend to choose a HCL colourmap.
  
  #. **HCL**: Choose the type of the HCL color map 
     (options: "diverging", "qualitative", "sequential single hue", "sequential multiple hue"). 
     We recommend to use a tool like hclwizard to create a colourmap.
  #. **Editor**: an integrated colourmap editor opens to create own colourmaps.
  #. **Predefined**: Choose from predefined colourmaps.
  #. **HSV**: Load a transfer function from Vapor XLM file.


Adjusting the position
----------------------

.. figure:: ./figs/transfer_function_colour_map/tf_position.webp
    :figwidth: 100%
    :align: left
    :alt: A figure should appear here


* Open **Transfer function scalar to colour (colour map)** → **actor properties** → **position**
  
  * Change the **X** coordinate from -1 (left) to 1 (right)
  * Change the **Y** coordinate from -1 (bottom) to 1 (top)
  * Adjust the **Width** and **Height** of the colourbar displayed in the scene