Skew-T Diagrams
===============

.. _fig_skewt_actor:

.. figure:: ./figs/skewt/actorref_skewt.webp
    :figwidth: 100%
    :align: left
    :alt: A figure should appear here

    Skew-T Actor.


Description
-----------

Met.3D supports the plotting of Skew-T diagrams using the Skew-T diagram actor. 
The user needs to add the temperature and dew point temperature variables to the actor. 
By positioning the actor at the desired geographic location and choosing the pressure levels of interest, 
appropriate Skew-T could be created.


Appearance
----------

Skew-T diagram actor provides various options to customize the way the diagram has to appear. 
By changing the appropriate parameters, which are self-explanatory, the user can do the customization.

.. figure:: ./figs/skewt/skewt_appearance.webp
    :figwidth: 100%
    :align: left
    :alt: A figure should appear here


Adjusting location
------------------

.. figure:: ./figs/skewt/skewt_location.webp
    :figwidth: 100%
    :align: left
    :alt: A figure should appear here

The location of the plot can be set by adjusting the lo lat values in the user interface. 
Also, the Skew-T actor has a 'profile pole', which when enabled, 
the user can use to position the plot in the desired geographic location, while interactive mode(double click).

.. figure:: ./figs/skewt/skewt_location_interactive.webp
    :figwidth: 100%
    :align: left
    :alt: A figure should appear here

