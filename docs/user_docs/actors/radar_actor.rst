Radar actor
===========

The radar actor visualizes 3D volume scan radar data, which is loaded as a dataset as described in :doc:`/user_docs/data_handling/radar_data`.

The radar actor can be found in the scene management or in the toolbar:


.. figure:: ./figs/radar_actor/radar_toolbar_icon.webp
    :figwidth: 10%
    :align: left
    :alt: Menu to load a new radar dataset.

    Toolbar icon

Like in other actors, variables from the dataset can be loaded into the radar actor in the variables sub-property. 
Remember to set a transfer function with a suitable colourmap for each variable you 
want to visualize, as otherwise the variable is not shown.

To properly display radar data, the vertical scaling of the scene should be reduced: 
Change **System -> Scene view #<number of your scene> -> vertical scaling** to a new value between 1 and 5.

.. figure:: ./figs/radar_actor/vertical_scaling.webp
    :figwidth: 40%
    :align: left
    :alt: How to change vertical scaling: System -> Scene view #1 -> vertical scaling

.. raw:: html

    <p style="clear: both;"></p>

In the actor properties of the radar actor, you can now select the variable you 
want to analyze. By default, the first variable is selected. 

You can choose between four render modes:

* By default, **pseudocolour 3D** is selected, which shows the full scan. 
* Alternatively, the data of different elevation angles can also be drawn each in 2D by selecting **pseudocolour 2D**, which ignores the vertical extent of the radar beam.
* A **horizontal section** can be drawn at an altitude that can be set in the actor properties->altitude range->height/pressure sub-properties. You can decide if you want to set it via height in metres or pressure in hPa.
* A **vertical section** at an azimuth angle (angle of the radar beam 360° rotation around the radar station) selected in actor properties->azimuth range->vertical section azimuth angle.

.. figure:: ./figs/radar_actor/radarActor.webp
    :figwidth: 100%
    :align: left
    :alt: The 4 render options in the radar actor.

Furthermore, the following settings are customizable in the actor properties:

* **Use colour for cells without signal**: adds a background colour. This is helpful to see the full extent of the scanned area.
* This combines well with **graticule**, which draws lines at an adjustable number of azimuth angles and range steps, helping to grasp distance information.
* **Elevations**: select a range of elevation angles to display, or to only draw a singular elevation of the scan.
* **Azimuth angle**: choose a range of azimuth angles for the modes pseudocolour 3D, pseudocolour 2D, and horizontal section. This can be used to visualized a side profile and a top view of part of the data at the same time. For vertical section, a singular angle can be selected.
* **Altitude range** allows you to select a a singular height for horizontal section, and a minimum/maximum height or a singular height for the other render modes.
* The actor includes two **shadows**, which can be customized. One is shown where data is visible, and the other is rendered for the full grid of the currently selected elevation(s). Both can be turned on and off, and you can change their colours.

If you want to look at another time point, change the valid time either in the Synchronization box on top, or, if the data is not synchronized, in variables->synchronization->valid.
