.. _pole_actor:

Movable poles
=============

.. _fig_moveable_pole_actor:
.. figure:: ./figs/moveable_poles/actorref_poles.webp
    :figwidth: 100%
    :align: left
    :alt: A figure should appear here

    Vertical movable poles actor.


Description
-----------

The movable poles actor draws an arbitrary number of vertical axes into the scene. 
The axes can be used to improve spatial perception and to determine the height of a 3D feature 
(e.g. of an isosurface) in the scene.

* The actor supports an arbitrary number of vertical axes.
* The axes can be labelled by pressure.
* The axes can be interactively moved by the user.


Adding a movable pole and adjusting its position
------------------------------------------------

.. figure:: ./figs/moveable_poles/pole_position_settings.webp
    :figwidth: 100%
    :align: left
    :alt: A figure should appear here


* Open **Movable poles** → **actor properties**
* Click the execute button of **add pole**.
* A new pole is added in the scene and the list below the **add pole** property
* You have two options to change the position of a movable pole:
* 
  #. Under the properties of the pole (**pole** → **position**) it is possible to set the longitude (**lon**) 
     and latitude (**lat**) coordinate of the movable pole
  #. In the *actor interaction mode* you can move the pole interactively 
     by clicking and holding the mouse course on the grey bubble at the bottom or top of the movable pole.


Adjusting the vertical extension of all moveable poles
------------------------------------------------------

.. figure:: ./figs/moveable_poles/pole_vertical_extend.webp
    :figwidth: 100%
    :align: left
    :alt: A figure should appear here


* Open **Movable poles** → **actor properties**
  
  * the value of **bottom pressure** sets the lower extend of all movable poles of this actor
  * the value of **top pressure** sets the upper extend of all movable poles of this actor
