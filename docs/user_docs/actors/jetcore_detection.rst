Jet-stream core line detection
==============================

.. _fig_jetcore_detection_actor:
.. figure:: ./figs/jetcore_detection/actorref_jetcore_actor.webp
    :figwidth: 100 %
    :align: left
    :alt: The jetcore detection actor showing jetcores over the North Atlantic.
    
    Jetcore detection actor with a transfer function.


Description
-----------

The jetcore detection actor detects and visualizes jet-stream core lines in gridded data sets.
See `Robust Detection and Visualization of Jet-stream Core Lines in Atmospheric Flow 
<http://dx.doi.org/10.1109/tvcg.2017.2743989>`_ for a detailled description of the algorithm.

It requires several derivatives of wind speed. See the :ref:`list of available derived variables` for more info.

* Jet-stream core lines are detected by intersecting zero-isosurfaces of horizontal and vertical derivatives of wind speed.
* The resulting jetcores can be filtered by properties including as line length, angle or pressure difference.
* The jetcore color can depend on another variable such as wind speed or pressure.
* The jetcores can display an arrow that indicates their direction.
* Drop lines can be rendered below the jetcores to improve spatial perception.