Isosurface intersection
=======================

.. _fig_isosurface_intersection_actor:
.. figure:: ./figs/isosurface_intersection/actorref_isosurface_intersection.webp
    :figwidth: 100%
    :align: left
    :alt: This figure shows a screenshot of the isosurface intersection actor.

    Isosurface intersection actor.


Description
-----------

The isosurface intersection actor intersects two variables at specific isovalues.

* Intersects two variables at specified isovalues.
* Result can be filtered according to line length and a filter variable.
* Can render drop lines to improve spatial perception.
* Can be colored by a constant color, by pressure or by a different variable.
* Thickness of intersection lines is configurable.