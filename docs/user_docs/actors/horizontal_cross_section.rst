Horizontal cross section
========================

.. _fig_horizontal_cross_section:
.. figure:: ./figs/horizontal_cross_section/actorref_hsec.webp
    :figwidth: 100%
    :align: left
    :alt: A figure should appear here

    Horizontal cross-section actor, together with a transfer function.


Description
-----------

The horizontal section actor draws a map of multiple forecast data fields at an arbitrary pressure altitude.

* The actor supports multiple forecast data fields.
* Each data field can be rendered as line contours, filled contours, pseudo-colour plot and textured stippling/hatching.
* The horizontal section can be interactively moved up and down by the user.
* Each horizontal section contains an instance of a graticule actor to render graticule lines and coast and border-lines.
* A surface shadow is available to improve spatial perception.
* Multiple stacked horizontal sections are allowed in a single scene.
* Wind barbs and arrows are supported.


Rendering
---------

.. figure:: ./figs/horizontal_cross_section/hsec_render_settings.webp
    :figwidth: 100%
    :align: left
    :alt: A figure should appear here

Met.3D currently supports render modes consisting of filled contours, 
line contours, texture contours and their combinations. 
After choosing the render mode, the user needs to select an appropriate transfer function 
or texture function (which should have been already created). 
There are options to choose the various characteristics of the contour sets.