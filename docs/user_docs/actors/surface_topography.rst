Surface topography
==================

.. _fig_surface_topography:
.. figure:: ./figs/surface_topography/actorref_surfacetopography.webp
    :figwidth: 100%
    :align: left
    :alt: A figure should appear here

    Surface topography (terrain) actor, together with a transfer function.


Description:
------------

The surface topography actor renders a terrain, using pressure as the vertical coordinate.

* The actor renders the surface pressure field as terrain.


Rendering
---------

Two data fields are required: 

#. Surface pressure (or a similar pressure surface) 
#. A variable that maps to colour (via a transfer function)
