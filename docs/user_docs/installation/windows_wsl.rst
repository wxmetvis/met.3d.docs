Installing Met.3D under WSL 2
=============================

.. attention:: 
    The following instructions are **experimental** and provided for your convenience.
    Please only follow them if you know what you are doing.


Met.3D can be run on Windows under WSL 2.
This installation guide will cover the steps required to run it on Ubuntu under WSL 2.
This guide assumes you are using the latest WSL version and Ubuntu distribution.
We only tested this guide using an NVIDIA GPU.

.. important:: 
    
    Although Met.3D works under WSL 2, it is likely to be less performant than running natively on Linux.


Installing WSL 2
----------------

First, install WSL 2 on Windows, if you have not done so already. Please follow the guide by Microsoft
available `here <https://learn.microsoft.com/de-de/windows/wsl/install>`_.

Alternatively, open the Powershell and install WSL 2 with the default Ubuntu distribution using WSL Install::

    wsl --install



Setting up WSL with a suitable graphics driver
----------------------------------------------

Since Met.3D requires OpenGL 4.3 as a minimum requirement, we will need to do some setup on WSL 2 to support this.
THe following commands are all from within the Ubuntu shell.

First, update your system repository using

System update::

    sudo apt update && sudo apt upgrade

Afterwards install ``mesa-utils`` to be able to check the supported OpenGL version.

::

    sudo apt install mesa-utils


Next, verify your OpenGL version using the following command:

::

    glxinfo | grep OpenGL

Your core OpenGL profile version should be 4.2, unless WSL got updated since writing this guide.
We hence need to install a GPU driver that supports OpenGL 4.3 or higher.
For that, we add the PPA ``kisak-mesa``, which provides a more up-to-date GPU driver.
More information is available `on the kisak-mesa launchpad <https://launchpad.net/~kisak/+archive/ubuntu/kisak-mesa>`_.

::
    
    sudo add-apt-repository ppa:kisak/kisak-mesa
    sudo apt update

Now you need to do a full system upgrade to install the updated driver:

::

    sudo apt dist-upgrade


This should have updated your GPU driver in WSL 2 to the latest version.
You can now re-verify your supported OpenGL version by running ``glxinfo | grep OpenGL``.
For recent GPU models, the core OpenGL profile should now be 4.6.

If your core OpenGL profile version is still 4.2, you can try to enable software rendering by running

::
    
    export LIBGL_ALWAYS_SOFTWARE=true


This will cost performance but will allow higher OpenGL versions.
Verify using ``glxinfo | grep OpenGL`` as above.


Installing Met.3D
-----------------

Now follow the :ref:`installation guide for linux <install_linux>` to install Met.3D.
We recommend the straightfoward :doc:`conda` installation.