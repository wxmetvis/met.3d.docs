Linux binary conda package
==========================

System requirements
-------------------

You require an OpenGL 4.3 (or higher) capable graphics card and an appropriate Linux driver to run Met.3D. 
The driver will most likely be a proprietary driver (we've only tested Nvidia so far); 
open-source drivers for Linux currently do not provide the required capabilities. 
Before you continue with the installation, make sure that graphics card and driver are installed. 
If everything is installed correctly, the glxinfo command should output something similar to 
(the important thing is the OpenGL core profile version > 4.3):

::

    # glxinfo | grep OpenGL
 
    OpenGL vendor string: NVIDIA Corporation
    OpenGL renderer string: GeForce GTX TITAN/PCIe/SSE2
    OpenGL core profile version string: 4.4.0 NVIDIA 340.96
    OpenGL core profile shading language version string: 4.40 NVIDIA via Cg compiler

.. important:: 
    
    So far we've only been able to test Nvidia drivers. 
    Met.3D uses an Nvidia-specific OpenGL extension that queries the available GPU memory. 
    If you don't have an NVIDIA GPU, you can set your GPU memory manually in the :ref:`frontend_app_settings`.
    Otherwise, you might encounter the error “GPU memory limit: 0 kb”.


Installation
------------

Our recommended approach to compile Met.3D from source 
and/or to set up a development environment under Linux is using conda. 
The conda system provides package management that is independent from your Linux system 
and safely keeps all packages in an environment. 
This way, everything you install for Met.3D will not interfere with your base system. 
Also, using `conda`_ is independent of your Linux distribution, 
hence the described approach should work with any Linux distribution and version.

If you haven't done so yet, you need to install conda first. 
Here, we use the `miniconda`_ distribution. 
We recommend to install miniconda into a directory that provides enough disk space 
(default is in you home directory, you may want to use a different directory).

Install miniconda::

    wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh
    chmod +x Miniconda3-latest-Linux-x86_64.sh
    ./Miniconda3-latest-Linux-x86_64.sh

After installing conda, you can create a conda environment for Met.3D, 
so that it won't interfere with any other conda environment. 
Afterwards you can activate the environment.

Create and activate Met.3D conda environment::

    conda create -n met3d_binaries
    conda activate met3d_binaries

Afterwards, you can install Met.3D in your conda environment using the following command:

Install Met.3D::

    conda install -c met3d -c conda-forge met3d


Running Met.3D
--------------

To run Met.3D, activate your conda environment and start Met.3D by typing:

::

    # Make sure your "met3d" conda environment is activated:
    conda activate met3d_binaries
    # Start Met.3D:
    met3d

Known issues:
-------------

"met3d: command not found"
~~~~~~~~~~~~~~~~~~~~~~~~~~

Following the above installation instructions, this error may occur when trying to run Met.3D. 
Please try to deactivate and reactivate the conda environment into which you installed Met.3D.

::

    # Deactivate and reactivate your "met3d" conda environment:
    conda deactivate met3d_binaries
    conda activate met3d_binaries


.. _conda: https://docs.conda.io/projects/conda/en/latest/
.. _miniconda: https://docs.conda.io/en/latest/miniconda.html