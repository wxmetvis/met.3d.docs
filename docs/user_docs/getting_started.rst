Getting started
===============

This document will show you how to get up and running with Met.3D, and
provide you with a short overview of how to use the software. 
We provide ready-to-use binaries as a conda package :doc:`installation/conda`.
If you want to compile Met.3D by yourself, please follow the notes provided in the section
:doc:`installation/src_conda`.
The following sections assume that the tool has been successfully installed.

:doc:`getting_started/first_steps` contains a tutorial for the first steps with
the Met.3D. :doc:`getting_started/user_interface` contains information about the
various user interface components.

.. note::
    Please note that Met.3D is being developed within a research project. We
    do our best to fix bugs in the software. However, you may
    encounter bugs when using Met.3D. Please let us know about any bug you
    encounter, so we can improve the software. Please also regularly check
    for software and user guide updates. 
    
.. note::
    There is more functionality in Met.3D (in part experimental) than described in
    this user guide. We will complete the user guide in the future.

.. toctree::
   :maxdepth: 2
   :caption: Chapters

   getting_started/first_steps
   getting_started/tutorial_slides
   getting_started/user_interface
   getting_started/frontend_config

.. 
    Add tutorial slides somewhere too.