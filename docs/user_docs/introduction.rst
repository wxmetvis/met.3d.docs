Introduction
============

The documentation for Met.3D is organized into the following sections:

* :ref:`user-docs`

Information about development is also available:

* :ref:`dev-docs`

.. attention:: 
    The documentation you are reading is **work in progress**. We are
    adding bits and pieces whenever we find time. If you don't find the information
    you are looking for, please `contact us <https://www.hcds.uni-hamburg.de/en/research/fg-vda/people.html>`_. 
    If you like to contribute to the documentation, please let us know as well!

.. _user-docs:

User Documentation
------------------

The user documentation provides :doc:`/user_docs/installation` instructions, 
information on :doc:`/user_docs/getting_started/first_steps` 
and more detailed information about :doc:`supported data </user_docs/data_handling>`, 
:doc:`user interface components </user_docs/getting_started/user_interface>`, 
and :doc:`visualization modules </user_docs/actors>` of Met.3D.

In addition, :ref:`specific tasks` are described, 
and some :doc:`examples </user_docs/examples>` are presented.

If you are missing information relevant to your work, `please let us know`_!

.. _`please let us know`: https://met3d.wavestoweather.de/contact.html

.. _dev-docs:

Developer Documentation
-----------------------

Large parts of the Met.3D source code have already been documented with the `Doxygen`_
code documentation system. A `Doxygen`_ configuration file is available in the
`Met.3D repository`_ (in the ``doc/doxygen`` subdirectory). Please run `Doxygen`_
locally to build the corresponding documentation.

.. _Doxygen: http://www.stack.nl/~dimitri/doxygen/
.. _Met.3D repository: https://gitlab.com/wxmetvis/met.3d/


Contribution
~~~~~~~~~~~~

If you are contributing to the Met.3D code base, please carefully read the
:doc:`/dev_docs/contribution`. They contain information on the used GIT workflow and
coding conventions. As the project is continuously growing, please adhere to
the listed conventions.


Architecture documentation
~~~~~~~~~~~~~~~~~~~~~~~~~~

Conceptual descriptions of the architecture of selected features in Met.3D are 
provided in :doc:`/dev_docs/architecture`. This section will be expanded in the 
future.
