Data handling
=============

Met.3D supports gridded data (e.g., from numerical simulations or from observations) and trajectory data.

This section provides details on:

.. toctree:: 
    :maxdepth: 1

    data_handling/dataformats
    data_handling/loading_data
    data_handling/derived_vars
    data_handling/python_interface

Specific topics
---------------

This section contains more specific topics related to working with gridded data.
These are not specific to Met.3D, but might be useful when working with it.

.. toctree:: 
    :maxdepth: 1

    data_handling/creating_stereographic_data
    data_handling/remapping_spectral_grid_to_regular_grid
    data_handling/remapping_radar_to_regular_grid
    data_handling/working_with_icon_data


.. _specific tasks:

Specific tasks
--------------

This section describes along examples how to conduct specific tasks.

.. toctree:: 
    :maxdepth: 1

    data_handling/computing_diff_fields