Slides: Introductory Met.3D tutorial
====================================

The following slides contain our introductory Met.3D tutorial.
We use the slides for our workshops and hope they are self-explanatory enough to be useful on their own as well.

.. pdf-include:: https://collaboration.cen.uni-hamburg.de/download/attachments/23134497/Met3D_Introductory_Tutorial_v20190320.pdf?api=v2