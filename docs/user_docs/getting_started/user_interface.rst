User interface components
=========================

This section will describe the various user interface components of Met.3D in detail.

.. _Toolbar User Documentation:

Toolbar
-------

The toolbar was introduced in Met.3D version 1.11. Hence note that older resources and screenshots
are missing this UI component.

.. figure:: ./figs/user_interface/toolbar.webp
    :figwidth: 100%
    :align: left
    :alt: A screenshot of Met.3D with the mouse hovering over the toolbar.

The toolbar contains one icon for each actor registered to Met.3D. It provides a straightforward 
way to add new actors to the scenes. By hovering over an actor icon, its name is displayed. By 
clicking on the icon, the actor is added to the scene currently in focus. It can be added to a 
different scene by simply dragging the icon onto the desired scene. Clicking or dragging an icon
while holding the Alt-modifier opens an additional window where the user can rename the actor and
select the scenes the actor should be added to. This window also appears if an actor to be added
requires a different name. The "Load" button can be used to load an actor configuration from file.


Synchronization control
-----------------------

Met3D provides controls to navigate through time as well as ensemble dimensions. 
By default all variables are synchronized (can be disabled in actor properties). 
To restrict possible dates and times to values in the dataset, "connect" dataset with sync control. 
To navigate in time or ensemble use the GUI elements accordingly.

.. figure:: ./figs/user_interface/sync_config_options.webp
    :figwidth: 100%
    :align: left
    :alt: A figure should appear here


It is possible to animate between a specified interval 
of the data available and store the images as well using the options provided in the play GUI.

.. figure:: ./figs/user_interface/sync_play_options.webp
    :figwidth: 100%
    :align: left
    :alt: A figure should appear here


.. TODO: Add documentation of the animation panel.

Waypoints
---------

Met.3D provides the creation of waypoints. 
This can be done using the ``Show waypoints`` menu, 
which in turn attaches the waypoint management options to the application window as shown below. 
Also, this can be accessed by using the short cut key F2.

.. figure:: ./figs/user_interface/waypoints_menu.webp
    :figwidth: 100%
    :align: left
    :alt: A figure should appear here


Bounding boxes
--------------

It is possible to define various bounding boxes. 
They can be used to control the data region that needs to be displayed in the view port. 
This can be done by pressing the ``Show bounding boxes`` menu, also with short cut key F3. 
This attaches the bounding box handling options to the bottom of the application window as shown below.

.. figure:: ./figs/user_interface/bbox_menu.webp
    :figwidth: 100%
    :align: left
    :alt: A figure should appear here


The user can define the latitude, longitude bounds of a bounding box.
They can edit the pre-defined bounding boxes or create new ones.
Existing bounding boxes can also be copied, deleted or saved to a file.

Scene Management
----------------

Met.3D view port is classified as views and scene. 
The management of the scenes can be done using the scene management menu.

.. figure:: ./figs/user_interface/scene_management.webp
    :figwidth: 100%
    :align: left
    :alt: A figure should appear here


The scenes can be added or deleted as well as their sequence can be adjusted. 
Also, the actors and their appearance in each of the scenes can be handled with toggle buttons. 
The scenes can have user defined names also and these can be assigned to scene views. 
This dialog can also be accessed by using the F4 shortcut key.

Session Management
------------------

The actions performed in creating various actors and their properties are recorded in session files. 
The sessions menu lists the previously stored session files, the user can choose a session which will then be executed.

.. figure:: ./figs/user_interface/session_management_mainwindow.webp
    :figwidth: 100%
    :align: left
    :alt: A figure should appear here

It is also possible to revert the current session.
To handle the session file storage, the session manager provides the dialog shown below.
This has options to change the storage location, to switch between sessions, to create, copy or delete a session. 
Also, the time limit to trigger the auto save can be set.
Apart from that, options exist to also handle the current session.

.. figure:: ./figs/user_interface/session_manager.webp
    :figwidth: 100%
    :align: left
    :alt: A figure should appear here


The current session also can be saved from the session menu submenu *save session as*.

.. figure:: ./figs/user_interface/create_session_dialog.webp
    :figwidth: 100%
    :align: left
    :alt: A figure should appear here