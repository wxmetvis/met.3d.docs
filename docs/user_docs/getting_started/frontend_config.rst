Frontend configuration
======================

The frontend configuration file provides several settings to Met.3D.
By default, the configuration file located at ``$MET3D_HOME/config/default_frontend.cfg.template`` is used.
Alternatively, you can create a user-specified configuration file and specifiy its name
using the ``--frontend`` command line argument.

Example::

    --frontend=/path/to/your/frontend.cfg

This section will explain the different settings in detail.


.. _frontend_app_settings:

Application settings
--------------------

The ``[ApplicationSettings]`` section provides some general settings.

* ``workingDirectory``: 
  Specifies Met.3D's working directory, where config files, screenshots, etc. are saved.
  
  **Default:** ``$HOME/met3d``

* ``fallbackGpuMemoryLimit_Gb``: 
  Specifies a fallback GPU memory limit, in-case that your GPU is a non NVIDIA GPU.
  This should ideally be set to your GPU memory. 
  This setting will not be used if the GPU memory can be automatically determined
  (as is the case for NVIDIA GPUs). 
  
  **Default:** ``8``

* ``showLogOutputOnStart``: 
  Whether to show the log output window in Met.3D on startup. 
  
  **Default:** ``true``


.. _frontend_session_manager:

Session manager
---------------

The ``[SessionManager]`` section provides default settings Met.3D's session manager.

* ``pathToSessionConfigurations``: 
  The path to a folder that stores your session files.
  
  **Default:** ``$HOME/met3d/sessions``

* ``loadSessionOnStart``: 
  A name of a session to load on application start, or ``None``.
  If set to ``None``, a default session is loaded, that is specified through the following sections.
  
  **Default:** ``None``

* ``autoSaveSessionIntervalInSeconds``: 
  An interval in seconds at which Met.3D automatically saves the current session.
  If set to ``0``, auto saving will be disabled.
  
  **Default:** ``0``

* ``saveSessionOnApplicationExit``: 
  Whether to save the current session automatically on application exit.
  
  **Default:** ``false``

* ``maximumNumberOfSavedRevisions``: 
  Maximum number of revision files stored for one session.
  
  **Default:** ``12``


Synchronization
---------------

The ``[Synchronization]`` section specifies a list of synchronization controls initialized in the default session.

The section itself has one setting that needs to be set:

* ``size``: 
  The number of synchronization controls that are specified in this section.
  NOTE: Using a size other than "1" currently (v1.13) has no effect.
  
  **Default:** ``1``

Each synchronization control has the following settings.
The name of each setting needs to have the prefix ``i\``, where ``i`` is the index of the synchronization control starting with 1.

* ``i\name``: 
  The name of the synchronization control.
  
  **Default:** ``Synchronization``

* ``i\initialiseFromDatasource``: 
  Optional: 
  Configure which data sources should be used to restrict selectable init
  times, valid times, and members with which actors and actor variables can synchronize. 
  To use more than one data source separate their names with '/'.
  See :ref:`loading data from command line` for more information on how to load the required data source.

  **Default:** ``ECMWF ENS EUR_LL10 ENSFilter``


Scene navigation
----------------

The ``[SceneNavigation]`` section is used to configure several scene navigation settings.

* ``mouseButtonRotate``: 
  The mouse button used to rotate the scene view. 
  Possible options are ``left``, ``middle`` and ``right``.

  **Default:** ``left``

* ``mouseButtonPan``:
  The mouse button used to pan the scene view.
  Possible options are ``left``, ``middle`` and ``right``.

  **Default:** ``right``

* ``reverseDefaultPanDirection``:
  Reverses the pan direction. 
  By default, when panning, the scene moves with the mouse.
  When reversing it, the scene instead moves in the opposite direction.

  **Default:** ``false``

* ``mouseButtonZoom``:
  Configure which mouse button is used to zoom. 
  Possible options are ``left``, ``middle`` and ``right``.
  If set to ``middle``, the mouse wheel is used to zoom.
  Otherwise, the specified button + move up/down is used.
  In scene navigation mode ``MOVE_CAMERA``, mouse wheel forward zooms out.
  Meanwhile, in scene navigation mode ``ROTATE_SCENE``, mouse wheel forward zooms in.
  See :ref:`frontend_scene_views` for more info.

  **Default:** ``middle``

* ``reverseDefaultZoomDirection``:
  Reverses the zoom direction.

  **Default:** ``false``

.. _frontend_scene_views:

Scene views
-----------

The ``[SceneViews]`` section specifies the scene views that are created in the default session.

* ``size``:
  The number of scene views specified.

  **Default:** ``6``

The following settings are available for each scene view, and are prefixed with ``i\``, 
where i is the index of the scene view, starting with 1.

* ``i\sceneNavigation``:
  The navigation mode of the scene. Set to either ``MOVE_CAMERA`` or ``ROTATE_SCENE``.
  If set to ``ROTATE_SCENE``, ``sceneRotationCentre`` needs to be set, too.

  **Default:** ``MOVE_CAMERA``

* ``i\sceneRotationCentre``:
  Required only if ``sceneNavigation`` is set to ``ROTATE_SCENE``.
  Specifies the rotation center in lon/lat/pressure.

  **Default:** ``0./45./500.``


Scenes
------

The ``[Scenes]`` section specifies a list of scenes that are created in the default session.

* ``size``:
  The number of scenes specified.

  **Default:** ``6``

The following settings are available for each scene, and are prefixed with ``i\``, 
where i is the index of the scene view, starting with 1.

* ``i\name``:
  The name of the scene.

  **Default:** ``Scene i``, where i is the index.


Waypoints model
---------------

The ``[WaypointsModel]`` section provides settings for waypoint tables used for vertical sections.

* ``size``:
  The number of waypoint tables specified.

  **Default:** ``1``

The following settings are available for each entry, and are prefixed with ``i\``, 
where i is the index of the waypoint table, starting with 1.

* ``i\name``:
  Name of the waypoint table.

  **Default:** ``Waypoints``

* ``i\datafile``:
  Path to the waypoints table *.ftml* file.

  **Default:** ``$MET3D_HOME/config/default_waypoints.ftml``


Bounding boxes
--------------

The ``[BoundingBoxes]`` section specifies the default bounding boxes.


* ``config``:
  Path to the default bounding boxes config file.

  **Default:** ``$MET3D_HOME/config/default_boundingboxes.bbox.conf``


Text
----

The ``[Text]`` section provides settings for text rendering.

* ``fontfile``:
  Path to the font file to use for text rendering.

  **Default:** ``$MET3D_BASE/third-party/freefont-20120503/FreeSans.ttf``

* ``fontsize``:
  The font size used for text rendering.

  **Default:** ``24``


Coast and country borderlines
-----------------------------

The ``[CoastCountryLines]`` section configures the geometry of coastlines and country borders.

* ``coastfile``:
  Path to the coastlines geometry file.

  **Default:** ``$MET3D_BASE/third-party/naturalearth/ne_50m_coastline.shp``

* ``countryfile``:
  Path to the country borders geometry file.

  **Default:** ``$MET3D_BASE/third-party/naturalearth/ne_50m_admin_0_boundary_lines_land.shp``


Colourmaps
----------

The ``[Colourmaps]`` section is used to define color map settings.

* ``scientificcolourmapdir``:
  Path to the scientific colourmaps by Fabio Crameri (`<https://doi.org/10.5281/zenodo.5501399>`_).
  Colourmaps will be automatically loaded by Met.3D if found at this path.

  **Default:** ``$MET3D_BASE/third-party/ScientificColourMaps7``


Colour map directories
----------------------

The ``[ColourMapDirectories]`` section defines a list of directories for Met.3D to search for colourmaps.
Supported colourmap files are Vapor XML transfer functions (``.vtf``) and ParaView colourmap XML files.

* ``size``:
  The number of directories specified.

  **Default:** ``0``

The following settings are available for each entry, and are prefixed with ``i\``, 
where i is the index of the entry, starting with 1.

* ``i\colourmapdirectory``:
  The directory, where colourmaps are located at.
  This directory will be scanned for supported colourmaps and these will be loaded into Met.3D.

  **Default:** ``$MET3D_BASE/third-party/ScientificColourMaps7``


Actors
------

The ``[Actors]`` section can be used to specify actors that should be loaded on system start-up.
Specify paths to actor configuration files that have previously been saved from Met.3D.

* ``size``:
  The number of actors specified.

  **Default:** ``0``

The following settings are available for each entry, and are prefixed with ``i\``, 
where i is the index of the entry, starting with 1.

* ``i\config``:
  Path to the actor configuration file.
  
  **Default:** ``$MET3D_BASE/config/graticule_default.actor.conf``

* ``i\scenes``:
  The scenes this actor will be added to.
  For multiple scenes, separate them with ``/``, e.g. ``Scene 1/Scene 2``.

  **Default:** ``Scene 1``


Predefined actors
-----------------

The ``[PredefinedActors]`` section is mainly intended for development purposes.

**This section still needs documentation.**


Batch mode
----------

The ``[BatchMode]`` section can be used to specify settings used when running Met.3D in batch mode.

If the batch mode is used, make sure to also load a pipeline on startup, see :ref:`loading data from command line` for more info.
A session needs to be specified as well, see :ref:`frontend_session_manager` for more info.

Met.3D will load the pipeline and session first, then trigger the synchronization controls time animation
and write out the generated images.

* ``runInBatchMode``:
  Whether batch mode is enabled or not.

  **Default:** ``false``

* ``batchModeAnimationType``:
  The animation type of the batch mode.
  Can be ``timeAnimation`` for a normal animation, that will generate an image each time step.
  Alternatively can be set to ``cameraSequenceTimeAnimation``, which will use the camera sequence
  that is set in the scene. These still need to be documented.

  **Default:** ``timeAnimation``

* ``batchModeSychronizationName``:
  The name of the synchronization control used for the animation.

  **Default:** ``Synchronization``

* ``startAnimationAtMostRecentInitTimeOfDataSource``:
  Start the animation at the most recent available init time of the specified data source.
  The data source needs to be loaded as specified in :ref:`loading data from command line`.

  **Default:** ``ECMWF ENS ENSFilter``

* ``animationTimeRangeSeconds``:
  The number of seconds from the init time that the animation should run.
  Only used in ``timeAnimation`` animation mode.

  **Default:** ``+432000``

* ``batchModeQuitWhenCompleted``:
  Whether Met.3D will quit, when the batch mode has completed the animation.

  **Default:** ``true``

* ``overwriteExistingImageFiles``:
  Whether to overwrite existing image files or not.

  **Default:** ``true``