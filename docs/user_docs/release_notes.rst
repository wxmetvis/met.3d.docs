Release notes
=============

Release notes are listed on the `News`_ page of the Met.3D main website.

.. _News: https://met3d.wavestoweather.de/news.html
