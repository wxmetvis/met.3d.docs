Working with ICON data
======================

Currently though Met3D doesn't support the native unstructured grid of ICON model, 
it is still possible to remap the grid onto a regular lat lon grid format supported by Met3D.

The following are the steps that outline this procedure:

* The 'reference time' of the 'time' variable should be renamed to a valid units as per CF convention. 
  For example, below to set to 'hours' since '2020-02-01,00:00:00', 
  the CDO command would be ``cdo -setreftime,2020-02-01,00:00:00,hours in_file out_file``

The following example shows the time attributes in the original data file

.. code-block:: 
    :linenos:

    netcdf output_DOM01_PL_0001 {
    dimensions:
            time = UNLIMITED ; // (1 currently)
            lon = 360 ;
            lat = 71 ;
            plev = 52 ;
    variables:
            double time(time) ;
                    time:standard_name = "time" ;
                    time:units = "day as %Y%m%d.%f" ;
                    time:calendar = "proleptic_gregorian" ;
                    time:axis = "T" ;
            float lon(lon) ;
                    lon:standard_name = "longitude" ;
                    lon:long_name = "longitude" ;
                    lon:units = "degrees_east" ;
                    lon:axis = "X" ;
            float lat(lat) ;
                    lat:standard_name = "latitude" ;
                    lat:long_name = "latitude" ;
                    lat:units = "degrees_north" ;
                    lat:axis = "Y" ;
            double plev(plev) ;
                    plev:standard_name = "air_pressure" ;
                    plev:long_name = "pressure" ;
                    plev:units = "Pa" ;
                    plev:positive = "down" ;
                    plev:axis = "Z" ;
            float u(time, plev, lat, lon) ;
                    u:standard_name = "eastward_wind" ;
                    u:long_name = "Zonal wind" ;
                    u:units = "m s-1" ;
                    u:param = "2.2.0" ;
            float v(time, plev, lat, lon) ;
                    v:standard_name = "northward_wind" ;
                    v:long_name = "Meridional wind" ;
                    v:units = "m s-1" ;
                    v:param = "3.2.0" ;


The modified attributes for the time variable should appear as below

.. code-block:: 
    :linenos:

    netcdf output_DOM01_PL_0001 {
    dimensions:
            time = UNLIMITED ; // (1 currently)
            lon = 360 ;
            lat = 71 ;
            plev = 52 ;
    variables:
            double time(time) ;
                    time:standard_name = "time" ;
                    time:units = "hours since 2020-2-1 00:00:00" ;
                    time:calendar = "proleptic_gregorian" ;
                    time:axis = "T" ;
            float lon(lon) ;
                    lon:standard_name = "longitude" ;
                    lon:long_name = "longitude" ;
                    lon:units = "degrees_east" ;
                    lon:axis = "X" ;
            float lat(lat) ;
                    lat:standard_name = "latitude" ;
                    lat:long_name = "latitude" ;
                    lat:units = "degrees_north" ;
                    lat:axis = "Y" ;
            double plev(plev) ;
                    plev:standard_name = "air_pressure" ;
                    plev:long_name = "pressure" ;
                    plev:units = "Pa" ;
                    plev:positive = "down" ;
                    plev:axis = "Z" ;
            float u(time, plev, lat, lon) ;
                    u:standard_name = "eastward_wind" ;
                    u:long_name = "Zonal wind" ;
                    u:units = "m s-1" ;
                    u:param = "2.2.0" ;
            float v(time, plev, lat, lon) ;
                    v:standard_name = "northward_wind" ;
                    v:long_name = "Meridional wind" ;
                    v:units = "m s-1" ;
                    v:param = "3.2.0" ;

* The files, if existing in different directories for different ensemble members, 
  can be accessed by Met3D data pipeline, by using wild card characters as shown in the Figure...

**TODO: Add Figure**

* Also, if the user wants to create a single file containing all the ensemble members 
  and all time steps (say for a given variable), 
  then the following preprocessing steps need to be done:

  #. Concatenate all time steps of given member using 
      ``ncrcat -n nsteps,4,1 ${idir}/output_DOM01_PL_0001.nc  ${odir}/output_DOM01_PL_all.nc``
      where

      * ``nsteps``:  number of time steps.
      * ``4``: the numeric suffix digit position that is to be incremented, here '**0001**' in the filename '**output_DOM01_PL_0001.nc**'.
      * ``1``: increment.
      * ``idir``: Input data files directory.
      * ``odir``: Ouput data files directory.
      
  #. Concatenate all ensemble member's all timesteps file, generated in the above step  into single file using
     ``ncecat -3 -u ensemble ${ifiles} ${odir}/output_DOM01_PL_ens.nc``
     where

     * ``3``: netcdf3 format.
     * ``7``: netcdf4 classic.
     * ``u``: name of new dimension, here 'ensemble'.
     * ``odir``: Ouput data files directory and 'output_DOM01_PL_ens.nc' is the name of the output file.

  #. The record dimension of the file needs to changed to the newly created 'ensemble' dimension using
     ``ncpdq -a time,ensemble ${idir}/output_DOM01_PL_ens.nc ${odir}/output_DOM01_PL_final.nc``
     where

     * ``-a time,ensemble``: Indication that 'time' dimension needs to be exchange position with 'ensemble' dimension, in the order of the dimensions.
     * ``idir``: Input data files directory and 'output_DOM01_PL_ens.nc' is the file with all time steps for all ensemble members.
     * ``odir``: Ouput data files directory and 'output_DOM01_PL_final.nc' is the name of the output file.