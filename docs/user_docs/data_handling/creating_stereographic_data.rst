Creating stereographic data from regular lat lon data
=====================================================

Met3D supports various projection types that are available in the 'proj' library. 
If the user intends to explore these features, 
then the regular lat lon grid first needs to be regridded onto the appropriate projection and then visualized in Met3D.

This remapping can be done as outlined below, for example from regular lat lon to stereographic using CDO command

.. code-block:: bash
    
    cdo -P 4 -O remapbil,${TARGETGRIDFILE} ${INFILE} ${OUTFILE}

where

* ``-P 4``: The number of pthreads to speed up the processing (here **4**).
* ``remapbil``: CDO operator to do remapping to a particular grid defined in the **TARGETGRIDFILE**.
* ``TARGETGRIDFILE``: A text file containing the target grid description , for example:

.. code-block:: 
    :linenos:

    gridtype = projection
    gridsize = 25600
    xsize = 160
    ysize = 160
    xname = xc
    xlongname = "x coordinate of projection (eastings)"
    xunits = "km"
    yname = yc
    ylongname = "y coordinate of projection (northings)"
    yunits = "km"
    xfirst = -8000
    xinc = 100
    yfirst = 8000
    yinc = -100
    grid_mapping = Polar_Stereographic_Grid
    grid_mapping_name = polar_stereographic
    straight_vertical_longitude_from_pole = 0.f
    latitude_of_projection_origin = 90.f
    standard_parallel = 70.f
    false_easting = 0.f
    false_northing = 0.f
    semi_major_axis = 6378273.f
    semi_minor_axis = 6356890.f
    proj4_string = "+proj=stere +a=6378273 +b=6356889.44891 +lat_0=90 +lat_ts=70 +lon_0=0"
    
    # distances in km and grid res from here: https://nsidc.org/data/polar-stereo/ps_grids.html
    # other example grid, see: https://code.mpimet.mpg.de/projects/cdo/embedded/index.html#x1-130001.3