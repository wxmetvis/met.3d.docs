
Python Interface
================

**Not functional yet, expected in v1.16.**

Met.3D provides a Python interface, which allows users to export gridded data 
from Met.3D to Python for further data analysis and plotting. This gridded data
can also include computed derived variables.

To export data to Python, create an actor and add some variables to export.
Note that only the currently active timestep of each variable will be 
exported. In the variables *Export to Python* menu, select the python file
and a method within this file to invoke. This file will then be imported 
in the Met.3D Python interface. 

The method will be invoked with an *xarray.Dataset* (see the `xarray documentation`_)
as function parameter. Thus, to export data from Met.3D to Python, you can
use the following Python file stub:

.. code-block:: python

   import xarray as xr

   # Import the logger endpoint, so we can log to the Met.3D logger.
   # You can log via .info(), .debug(), .warn(), and .err()
   import m3d_endpoints
   log = m3d_endpoints.Logger

   def myFunction(dataset: xr.Dataset):
     log.info("Received dataset:")
     log.info(str(dataset))
     # Data analysis, plotting etc...
     return


.. _`xarray documentation`: docs.xarray.dev