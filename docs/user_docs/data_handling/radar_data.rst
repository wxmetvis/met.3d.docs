Weather radar data
------------------

Met.3D supports polar (volume) scans of weather radar data in HDF5 files
that follow the ODIM_H5 information model, such as the daily volume and precipitation
scans made publicly available by the German Weather Service (DWD).

A detailed description of the ODIM_H5 information model can be found here:
http://eumetnet.eu/wp-content/uploads/2017/01/OPERA_hdf_description_2014.pdf

The dataset can be loaded in File -> New Dataset -> Radar.

.. figure:: ./figs/radar_data/AddRadarDataset.webp
    :figwidth: 100%
    :align: left
    :alt: Met.3D dataset dialog to load radar data.

Met.3D currently only supports datasets that separate volume data into separate files for each 
elevation angle sweep (files with object=SCAN in the metadata). Data of different radar sites should be loaded in separate datasets.

The files need to include the following metadata:

* lat, lon, height (of the radar site)
* quantity (variable name)
* source (radar site identifier)
* scan_count (of all sweeps in the volume of one time step)
* scan_index (of the current sweep)
* elangle (elevation angle [°] of the current sweep)
* starttime, endtime (of the current sweep)
* range (maximum beam range [m])
* beamwV (vertical beamwidth [°])
* beamwH (horizontal beamwidth [°])
* nbins (number of bins along the beam)
* rscale (distance between range bins [m])
* rstart (distance of start of the first range bin to the antenna [m])
* nrays (number of azimuth angles)
* startazA, stopazA (lists of start and stop azimuth angles)
* gain, offset (to transform raw values to variable values)
* undetect (raw value of grid points below the measurement detection threshold)
* nodata (raw value of points where no data was recorded)


.. note:: Please contact us if you want to load radar data in other formats, or if you face problems loading data.

Internally, the data of all elevation angles of a volume scan of one time step are grouped together.
They are stored as an object of the class MRadarGrid which also stores metadata such as coordinates 
of the radar site, beamwidth, and at which elevation angles, azimuth angles and distance from the antenna
each data point was recorded.


.. figure:: ./figs/radar_data/radar_grid.webp
    :figwidth: 100%
    :align: left
    :alt: A Met.3D image of the radar grid cells in a volume scan.

    Grid structure of a DWD volume scan: 10 elevations with differing numbers of range bins. (step size was increased for visibility)

To properly display radar data, the vertical scaling of the scene should be reduced: 
Change **System -> Scene view #<number of your scene> -> vertical scaling** to a new value between 1 and 5.