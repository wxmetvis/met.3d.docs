Computing Derived Variables
===========================

Deriving pre-defined atmospheric quantities
-------------------------------------------

Computation of several derived atmospheric quantities 
(e.g., potential temperature, equivalent potential temperature) is implemented in Met.3D. 
Prerequisite is that corresponding input variables are specified by the user for the dataset 
for which derived quantities shall be computed. 
Below we provide a (possibly incomplete) :ref:`list of available derived variables` 
and potential restrictions to grid types on which the variables can be computed. 
If you are interested in the specific definitions, 
please refer to the corresponding source code referenced below.

Defining input variables for computations
-----------------------------------------

The input variables to compute derived quantities need to be specified when the dataset is loaded. 
The following example shows the "Add datset" dialog in Met.3D v1.13. 
The input variables need to be specified by a "/"-separated string 
using the mapping "CF standard name:NetCDF or GRIB input variable name".

**TODO: Add image of dataset dialog Met.3D v1.13**

For example, assume that you are loading ECMWF ensemble forecast data from GRIB files. 
The variables will be named "u", "v", etc. in the GRIB files. 
Hence, your mapping string could look like:

.. code-block:: 

    eastward_wind:u/northward_wind:v/air_temperature:t/specific_humidity:q/surface_geopotential:z/surface_air_pressure:sp/surface_temperature:2t/lwe_thickness_of_precipitation_amount:tp/geopotential:z/atmosphere_relative_vorticity:vo

If your input data is read from NetCDF files, you would use the NetCDF variable names directly. 
E.g., if you u-wind variable is called "``u_wind``" in the input file, 
the corresponding part in the mapping would be "``eastward_wind:u_wind``".


.. _list of available derived variables:

List of derived variables
----------------------------

As of Met.3D v1.13, the following derived variables are implemented 
(all input variable names follow the `CF standard names`_):

.. note:: 
    The variables in the following list support all grid and vertical level types unless otherwise noted.
    
* Horizontal wind speed ("wind_speed")
  
  * Requires eastward_wind, northward_wind
  
* 3D wind velocity ("magnitude_of_air_velocity")
  
  * Requires eastward_wind, northward_wind, upward_air_velocity
  
* Potential temperature ("air_potential_temperature")
  
  * Requires air_temperature

* Equivalent potential temperature ("equivalent_potential_temperature")
  
  * Requires air_temperature, specific_humidity
  * Computed using equation (43) of Bolton (MWR, 1980), 
    "The Computation of Equivalent Potential Temperature".

* Relative humidity ("relative_humidity")
  
  * Requires air_temperature, specific_humidity
  * Computed using saturation vapour pressure by Huang (JAMC, 2018), 
    "A Simple Accurate Formula for Calculating Saturation VaporPressure of Water and Ice", 
    DOI: 10.1175/JAMC-D-17-0334.1

* Potential vorticity ("ertel_potential_vorticity")
 
  * Requires eastward_wind, northward_wind, air_temperature, surface_air_pressure
  * NOTE: Only available for regular lon-lat grids defined on hybrid sigma-pressure levels (e.g. ECMWF, ERA-5)
  * Computed using the `ETH Lagranto formulation`_

* Surface geopotential ("surface_geopotential")
  
  * Requires height_above_mean_sea_level, the height of the earth's surface above mean sea level (m).
  * Computed using the implementation used by MetPy 1.6, referring to Wallace & Hobbs (2006): 
    Atmospheric Science: An Introductory Survey

* Geopotential height ("geopotential_height")
  
  * Requires air_temperature, specific_humidity, surface_geopotential, surface_air_pressure, surface_temperature

* Geopotential height ("geopotential_height_from_geopotential")
  
  * Requires geopotential
  * Simply divides input by 9.81m^2/s^2
  
* Dew point temperature ("dew_point_temperature")
  
  * Requires specific_humidity

* Hourly total precipitation ("lwe_thickness_of_precipitation_amount_Nh")
  
  * Requires lwe_thickness_of_precipitation_amount
  * Computes N-hourly precipitation from accumulated total precipitation 
    (i.e., difference in the accumulated amount over N hours)

* Vertically integrated horizontal moisture flux 
  ("magnitude_of_vertically_integrated_horizontal_transport_of_moisture")

  * Requires surface_air_pressure, eastward_wind, northward_wind, specific_humidity
  
* Air pressure ("air_pressure")
  
  * Requires air_temperature
  * Reconstructs 3D pressure field from grid level specification

* Vertical air velocity in pressure coordinates ("omega_hydrostatic_approximation")

  * Requires upward_air_velocity, air_temperature
  * Computes the upward component of air velocity when air pressure is being used as
    the vertical coordinate (Pa s-1) from the upward air velocity in geometric units
    (m s-1). Assumes simple hydrostatic conditions.
    Computation is based on MetPy 1.6, referring to Wallace & Hobbs (2006):
    Atmospheric Science: An Introductory Survey

* First partial derivative of wind speed, horizontally perpendicular to wind direction ("dVs_dn")

  * Requires eastward_wind and northward_wind
  
* First partial derivative of wind speed, vertical (upward) ("dVs_dz")

  * Requires eastward_wind, northward_wind and geopotential_height
  
* Second partial derivative of wind speed, horizontally perpendicular to wind direction ("d2Vs_dn2")

  * Requires eastward_wind and northward_wind
  
* Second partial derivative of wind speed, vertical (upward) ("d2Vs_dz2")

  * Requires eastward_wind, northward_wind and geopotential_height

* Second partial derivative of wind speed, mixed horizontally perpendicular to wind direction and vertical (upward) ("d2Vs_dnz")

  * Requires eastward_wind, northward_wind and geopotential_height


Source code links
-----------------

Computation of derived variables is implemented in the "derivedvars" source code subdirectory:

* `<https://gitlab.com/wxmetvis/met.3d/-/tree/master/src/data/derivedvars>`_

Variable computation is implemented for each derived variable in a class derived from MDerivedDataFieldProcessor. 
The actual implementations are distributed over multiple files. 
A basic set of common quantities is implemented in

* `<https://gitlab.com/wxmetvis/met.3d/-/blob/master/src/data/derivedvars/derivedmetvars_standard.h>`_
* `<https://gitlab.com/wxmetvis/met.3d/-/blob/master/src/data/derivedvars/derivedmetvars_standard.cpp>`_

For each quantity, the implementation lists which input variables are required for computation. 
For example, the computation of horizontal wind speed requires the eastward 
and northward wind components as inputs.

.. code-block:: c++
    :linenos:

    MHorizontalWindSpeedProcessor::MHorizontalWindSpeedProcessor()
    : MDerivedDataFieldProcessor(
          "wind_speed",
          QStringList() << "eastward_wind" << "northward_wind")
    {}

..
    Should these implementation details be listed in the user doc?

All derived variables available are listed in the constructor of ``MDerivedMetVarsDataSource``:

* `<https://gitlab.com/wxmetvis/met.3d/-/blob/master/src/data/derivedvars/derivedmetvarsdatasource.cpp>`_

Generic derivation of quantities (to come)
------------------------------------------

We are working on a Python interface to allow the computation of arbitrary quantities on-the-fly 
in Python scripts executed by Met.3D. Help with the implementation is appreciated!


.. _`CF standard names`: http://cfconventions.org/standard-names.html
.. _`ETH Lagranto formulation`: https://iacweb.ethz.ch/staff/sprenger/lagranto/home.html
