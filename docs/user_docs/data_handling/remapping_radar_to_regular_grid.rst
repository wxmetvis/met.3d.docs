Remapping from radar grid to regular lat lon grid
=================================================

If you want to analyse radar data with actors besides the radar actor or raycaster actor, 
it needs to be remapped to a regular lat lon grid.

When you add a new variable to an actor and have loaded a radar dataset, you can select a radar variable
with "Pressure Levels" instead of "Radar Elevation" as the vertical dimension. This causes the radar data 
to be remapped to a grid that is regular in the horizontal and has pressure levels as its vertical dimension.

After adding the variable, you can modify its **regrid settings**:

* Change the vertical grid between equidistant pressure levels, equidistant height levels and user defined pressure levels.
* Change the number of equidistant pressure/height levels.
* For the horizontal grid dimensions, you can change the number of grid points or the distance between grid points in metres.