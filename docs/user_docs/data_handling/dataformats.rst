Supported data and file formats
===============================

Brief overview
--------------

This section provides an overview of the data and file formats supported
by Met.3D. 

- Met.3D supports gridded, structured data with a **regular, georeferenced 
  longitude-latitude grid** in the horizontal, and either **levels of constant
  pressure** (in the following "pressure levels") or **hybrid sigma-pressure
  levels** (as used, e.g., by the ECMWF integrated forecast system) in the
  vertical.
  
- Multiple datasets (that can have different grids) and ensemble datasets are
  natively supported.

- Also, trajectory data is supported.
  
- Currently, gridded data can be read from **NetCDF** files following the
  `Climate and Forecast (CF) Metadata Conventions`_ and from **ECMWF GRIB files**;
  trajectory data can be read from NetCDF files with a custom formatting.

- Met.3D also supports polar (volume) scans of weather radar data in HDF5 files 
  that follow the ODIM_H5 information model, such as the daily volume and precipitation 
  scans made publicly available by DWD.

.. note:: If your data cannot be read by Met.3D (i.e., if you don't have access
    to your datasets after start-up), a (admittedly currently limited) set of
    error messages is displayed by the software in the start-up output on the
    text console. Please contact us if you need help.
    
.. note:: Display of non-georeferenced data on regular grids (e.g., output
    from **LES models**) is possible but requires some hacking on the NetCDF
    side. Please contact us if you are interested. We are working on making
    data import for such data easily possible in a future Met.3D version.

    
Internal data formats in Met.3D
-------------------------------

Met.3D currently supports the following data:

.. toctree:: 
    :maxdepth: 1

    gridded_data
    trajectory_data
    radar_data

.. _`Climate and Forecast (CF) Metadata Conventions`: http://cfconventions.org/cf-conventions/v1.6.0/cf-conventions.html