.. _trajectory_data:

Trajectory data in NetCDF format
================================

Trajectories are stored as bundles of trajectories (class :code:`MTrajectories`).
A single trajectory in a bundle consists of a simple list of positions in
longitude-latitude-pressure space that resemble the trajectory. All trajectories
in a bundle share the same time information, i.e. the time for position *n* is
the same for all trajectories. This data layout corresponds to the smallest
data entity being the bundle of all trajectories computed for a single ensemble
member for the same time steps.

..
    Should internal data handling really be described with c++ classes in the user documentation?
    That should rather move to the developer documentation and user documentation should be simplified.

Description
-----------

Since the CF-conventions only provide `limited support for trajectory data`_ 
(in particular no ensemble dimension), 
we’re currently using a custom NetCDF layout that follows the CF-versions. 
There are a few limitations:

* There is **only one time dimension per NetCDF file**, 
  which corresponds to the time of the particle positions along the trajectories. 
  If you have a series of trajectory bundles started at different times 
  (i.e., a bundle of trajectories for each forecast time step as used for `WCB detection_`), 
  you need one NetCDF file per trajectory bundle.

The following example contains a NetCDF header of a file containing trajectories computed from an ensemble forecast:

.. code-block:: 
    :linenos:

    netcdf sometrajectorydata {
    dimensions:
        time = 17 ;
        trajectory = 215332 ;
        ensemble = 51 ;
        start_lon = 101 ;
        start_lat = 41 ;
        start_isobaric = 52 ;
        time_interval = 8 ;
    variables:
        double time(time) ;
                time:standard_name = "time" ;
                time:long_name = "time" ;
                time:units = "hours since 2012-10-19 06:00:00" ;
                time:trajectory_starttime = "2012-10-19 06:00:00" ;
                time:forecast_inittime = "2012-10-17 00:00:00" ;
    
        float lon(ensemble, trajectory, time) ;
                lon:standard_name = "longitude" ;
                lon:long_name = "longitude" ;
                lon:units = "degrees_east" ;
        float lat(ensemble, trajectory, time) ;
                lat:standard_name = "latitude" ;
                lat:long_name = "latitude" ;
                lat:units = "degrees_north" ;
        float pressure(ensemble, trajectory, time) ;
                pressure:standard_name = "air_pressure" ;
                pressure:long_name = "pressure" ;
                pressure:units = "hPa" ;
                pressure:positive = "down" ;
                pressure:axis = "Z" ;
    
        float temperature(ensemble, trajectory, time) ;
                temperature:standard_name = "temperature" ;
                temperature:long_name = "temperature" ;
                temperature:auxiliary_data = "yes" ;
                temperature:units = "K" ;
    
        float start_lon(start_lon) ;
                start_lon:long_name = "longitude of start grid" ;
                start_lon:units = "degrees_east" ;
        float start_lat(start_lat) ;
                start_lat:long_name = "latitude of start grid" ;
                start_lat:units = "degrees_north" ;
        float start_isobaric(start_isobaric) ;
                start_isobaric:long_name = "Isobaric surface of start grid" ;
                start_isobaric:units = "hPa" ;
                start_isobaric:positive = "down" ;
                start_isobaric:axistype = "pressure levels" ;
    
        float time_interval(time_interval) ;
                time_interval:long_name = "time interval" ;
                time_interval:units = "hours" ;
        float delta_pressure_per_time_interval(ensemble, trajectory, time_interval) ;
                delta_pressure_per_time_interval:long_name = "max. delta pressure of trajectory in time interval around start time" ;
                delta_pressure_per_time_interval:units = "hPa" ;
    }

**Required** and **optional** data are:

* **Required**: In addition to the air parcel time, Met.3D **requires** 
  the two attributes ``trajectory_starttime`` and ``forecast_inittime`` for the **time variable** 
  that define the time at which the trajectory was started, 
  as well as the forecast initialisation/base time of the forecast data 
  on which the trajectory was computed.

* **Required**: The **vertices** / **particle positions** of the trajectories need to specified 
  in the variables **lon**, **lat**, and **pressure**.

* **Optional**: Additional **variables traced along the trajectories** 
  (e.g., temperature in the example above) need to be identifyable by the attribute ``auxiliary_data = "yes"``.
  
  * You can put an arbitrary number of auxiliary trace variables into your file.
  
* **Optional**: The ``start_lon``, ``start_lat`` and ``start_isobaric`` variables are **optional** 
  and define the grid from which the trajectory bundle was started (used for `WCB detection`_).

* **Optional**: Similarly, the ``time_interval`` and ``delta_pressure_per_time_interval`` 
  variables are **optional** and define pre-computed values for `WCB detection`_. 
  Contact us if you’re interested.


Implementation
--------------

If you want to further understand, how NetCDF files with trajectory data are implemented,
you can read the following source files of the ``MTrajectoryReader``:

* `<https://gitlab.com/wxmetvis/met.3d/-/blob/master/src/data/trajectoryreader.h>`_
* `<https://gitlab.com/wxmetvis/met.3d/-/blob/master/src/data/trajectoryreader.cpp>`_




.. _`limited support for trajectory data`: http://cfconventions.org/cf-conventions/v1.6.0/cf-conventions.html#_trajectory_data
.. _`WCB detection`: http://www.geosci-model-dev.net/8/2355/2015/
