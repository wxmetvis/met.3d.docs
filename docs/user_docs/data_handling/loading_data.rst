.. _loading_data:

Loading data into Met.3D
========================

Overview
--------

Gridded data (both NWP and radar data) and trajectory data is accessed by Met.3D through data pipelines 
that are created for each dataset that the user specifies. 
From the user perspective, multiple datasets can be defined that represent, for example, 
the output data of a specific numerical simulation. 
A dataset can contain multiple variables at multiple time steps and for multiple ensemble members, 
contained in a single file or distributed over multiple files.

Datasets can be defined either during program runtime from the graphical user interface, 
or on program start-up (or in batch mode) by passing dataset configurations using the ``--datasets``
command line argument.


.. _Load from GUI:

Loading datasets from graphical user interface
----------------------------------------------

From the "File" menu, select "New dataset".

.. figure:: ./figs/loading_data/met3d_window_file_menu.webp
    :figwidth: 100%
    :align: left
    :alt: A figure should appear here


Previously stored dataset configurations can be loaded by using "Open Dataset Configuration".

All three options will open a dialog in which

* a name for the dataset,
* the directory containing the data files and a file name filter,
* the type of the data files (NetCDF or GRIB),
* additional options

can be specified.

.. note:: 
    Most options provide tooltips with more detailed descriptions. 
    Try to hover the mouse curser to get more information.

.. figure:: ./figs/loading_data/empty_dataset_dialog.webp
    :figwidth: 100%
    :align: left
    :alt: A figure should appear here


To reuse dataset configurations, use the "save configuration" button on the 
lower left side of the dialog to save a configuration file that can be re-loaded using 
the "Open Dataset Configuration" menu entry.

.. _loading data from command line:

Loading datasets using command line arguments
---------------------------------------------

After creating a dataset using the GUI, as explained in :ref:`Load from GUI`, you can use the saved 
dataset configuration file (file extension `.pipeline.conf`) and automatically load it on programm start-up
using the ``-d`` or ``--datasets`` command line options.
These expect a seimicolon separated list of dataset configuration files.
Example: 
::

    met3d --datasets=/path/to/dataset.pipeline.conf;/path/to/dataset2.pipeline.conf

.. _loading_precomp_traj_data:

Loading precomputed trajectory datasets
---------------------------------------

Met.3D is able to load precomputed trajectories and display them via the :doc:`/user_docs/actors/trajectory`.
These trajectories have to be in a NetCDF-CF like format described in :doc:`/user_docs/data_handling/trajectory_data`.

Similar to the gridded datasets, this data can be loaded using a `.pipeline.conf` file or via the user interface. 
Using the user interface,

* from the main "File" menu, select "New dataset",
* select the "Trajectories" tab,
* and enter the directory path in which the trajectory files are stored. 
  The data can be distributed over multiple files.
