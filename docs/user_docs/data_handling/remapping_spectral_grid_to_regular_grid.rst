Remapping from spectral grid to regular grid
============================================

The data if available on spectral grid or gaussian grid, needs to be remapped onto a regular lat lon grid, 
so that it can be analysed using Met.3D. 
The following code snippet provides a minimal example to perform the remapping.

.. code-block:: bash
    :linenos:

    #!/bin/bash
 
    #####################################################################################
    # Minimal examples of basic CDO commands for remapping ERA5 data onto a regular grid
    #
    # Link to the Met.3D documentation
    # https://collaboration.cen.uni-hamburg.de/display/Met3D/Welcome+to+Met.3D
    #
    # Link to the cdo documentation
    # https://code.mpimet.mpg.de/projects/cdo/wiki/Cdo#Documentation
    #
    # Link to a cdo reference sheet
    # https://code.mpimet.mpg.de/projects/cdo/embedded/cdo_refcard.pdf
    #
    #####################################################################################
    
    
    ###########
    # define path to the folder storing the ERA5 data as provided by the ECMWF
    # (GRIB data with spatial coordinate systems "Gaussian Grid" or "Spherical Harmonics")
    DATAFOLDER=/path/to/ERA5-data-folder/
    
    
    ##########
    # define path to the folder for storing the remapped ERA5 data-files
    POSTPROCESSEDDATAFOLDER=/path/to/Results-Folder/
    mkdir -p ${POSTPROCESSEDDATAFOLDER}
    
    
    ##########
    # define path to a file characterizing the grid to which the ERA5 data will be remapped
    TARGETGRIDFILE=/path/to/targetgridfile.txt
    
    # See below for an examplar gridFile for remapping with CDO to a regular lat-lon grid :
    # --> "cdo_gridFile_regular_LatLon_minimalExample.txt"
    
    
    ##########
    # define input and output data files   
    INFILE=/path/to/ERA5_Data_File
    OUTFILE=/path/to/ERA5_Data_on_regular_grid
    
        
    ########## 
    # bilinear remapping from reduced Gaussian grid to regular Lat-Lon grid
    # and conversion from GRIB to NETCDF
    cdo -f nc4 -remapbil,${TARGETGRIDFILE} -setgridtype,regular ${OUTFILE} ${INFILE}
    
    
    ##########
    # bilinear remapping from spectral grid to Gaussian grid to regular Lat-Lon grid
    cdo -f nc4 -remapbil,${TARGETGRIDFILE} -sp2gpl ${INFILE} ${OUTFILE}


The contents of the ``cdo_gridFile_regular_LatLon_minimalExample.txt`` in this example are as below:


.. code-block:: 
    :linenos:

    gridtype = lonlat
    gridsize = 90381
    xsize = 641
    ysize = 141
    xfirst = -80
    xinc = 0.25
    yfirst = 85
    yinc = -0.25
