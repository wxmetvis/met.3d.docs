About Met.3D: Publications, presentations, credits
==================================================

About Met.3D
------------

Met.3D is being developed as a research effort to improve visual analysis of
3-D meteorological data in research and forecasting.
The software is implemented in `C++`_ and `OpenGL 4`_ and
runs on standard commodity hardware. Its only "special" requirement is an 
OpenGL 4.3 capable graphics card. 
Met.3D currently runs under Linux.
It has originally been designed for weather forecasting during atmospheric 
research field campaigns, however, is
not restricted to this application anymore. Besides being used as a visualization tool,
Met.3D is intended to serve as a framework to implement and evaluate new 3-D and
ensemble visualization techniques for the atmospheric sciences.

Met.3D development is lead by the `Visual Data Analysis Group`_
at the `Hub of Computing and Data Science`_ of the `University of Hamburg`_ (UHH), Germany.
Previously, development was also done within the `Computing Graphics & Visualization Group`_ 
of the `Technical University of Munich`_ (TUM), Germany. 
We much acknowledge funding we have received from the German Research Foundation (DFG), 
from 2015 until 2024 within the Collaborative Research Centre SFB/TRR 165 `Waves to Weather`_ (both at UHH and TUM),
and ongoing with the UHH Cluster of Excellence `CLICCS`_. 
Previously, development at TUM was also partly funded by the European Union under 
the ERC Advanced Grant 291372 SaferVis and the ERC Proof-of-Concept Grant "Vis4Weather".

Met.3D research and development is led by `Marc Rautenhaus`_, UHH (previously TUM).

An entry point to information about Met.3D is the reference publication. 
Also, Marc's PhD thesis contains additional details.

*   Rautenhaus, M., Kern, M., Schäfler, A., and Westermann, R.:
    **Three-dimensional visualization of ensemble weather forecasts -- Part 1:
    The visualization tool Met.3D (version 1.0)**, 
    `Geosci. Model Dev., 8, 2329-2353`_,
    doi:10.5194/gmd-8-2329-2015, 2015.

*   Rautenhaus, M.: **Interactive 3D visualization of ensemble weather forecasts**,
    `Ph.D. thesis`_,
    Technical University of Munich, Munich, Germany, 195pp., 2015.


Release Notes
-------------

.. toctree:: 
  :maxdepth: 1

  release_notes


Publications
------------

The following (likely incomplete) list contains scientific publications 
that are using Met.3D for development, evaluation, or use of new visualization techniques in meteorology.

If you have used Met.3D for a publication please let us know so we can add your paper to the list!

* **2024**

  * Fischer, C., Fink, A. H., Schömer, E., Rautenhaus, M., and Riemer, M.:
    **An objective identification technique for potential vorticity structures associated with African easterly waves**,
    Geosci. Model Dev., 17, 4213–4228, URL `<https://doi.org/10.5194/gmd-17-4213-2024>`_, 2024

* **2023**

  * Schäfler, A. and Rautenhaus, M.: 
    **Interactive 3D Visual Analysis of Weather Prediction Data Reveals Midlatitude 
    Overshooting Convection during the CIRRUS-HL Field Experiment**,
    Bulletin of the American Meteorological Society, 104, E142-6E1434, URL
    `<http://doi.org/10.1175/BAMS-D-22-0103.1>`_, 2023

  * Neuhauser, C., Hieronymus, M., Kern, M., Rautenhaus, M., Oertel, A., and Westermann, R.: 
    **Visual Analysis of Model Parameter Sensitivities along Warm Conveyor Belt Trajectories 
    Using Met.3D (1.6.0-Multivar1)**, Geoscientific Model Development, 16, 4617-4638, 
    URL `<http://doi.org/10.5194/gmd-16-4617-2023>`_, 2023

  * Beckert, A. A., Eisenstein, L., Oertel, A., Hewson, T., Craig, G. C., and Rautenhaus, M.: 
    **The Three-Dimensional Structure of Fronts in Mid-Latitude Weather Systems in 
    Numerical Weather Prediction Models**, Geoscientific Model Development, 16, 4427-4450, 
    URL `<http://doi.org/10.5194/gmd-16-4427-2023>`_, 2023

* **2022**

  * Fischer, C., Fink, A. H., Schömer, E., van der Linden, R., Maier-Gerber, M., Rautenhaus, M., 
    and Riemer, M.: **A Novel Method for Objective Identification of 3-D Potential Vorticity Anomalies**, 
    Geoscientific Model Development, 15, 4447-4468, 
    URL `<http://doi.org/10.5194/gmd-15-4447-2022>`_, 2022

* **2021**

  * Meyer, M., Polkova, I., Modali, K. R., Schaffer, L., Baehr, J., Olbrich, S., and Rautenhaus, M.: 
    **Interactive 3-D Visual Analysis of ERA5 Data: Improving Diagnostic Indices for 
    Marine Cold Air Outbreaks and Polar Lows**, Weather and Climate Dynamics, 2, 867-891, 
    URL `<http://doi.org/10.5194/wcd-2-867-2021>`_, 2021

* **2020**

  * Schäfler, A., Ewald, F., and Rautenhaus, M.: 
    **Die Vermessung von Zyklonen (Observing Cyclones)**, promet (German Weather Service DWD), 
    103, 25-32, 
    URL `<https://www.dwd.de/DE/leistungen/pbfb_verlag_promet/pdf_promethefte/103_pdf.pdf?__blob=publicationFile&v=2>`_, 2020

  * Rautenhaus, M., Hewson, T., and Lang, A.: 
    **Cyclone Workshop showcases 3D visualisation**, ECMWF Newsletter, 162, 4-5, 
    URL `<http://www.ecmwf.int/en/newsletter/162/news/cyclone-workshop-showcases3d-visualisation>`_, 2020

  * Eisenstein, L, Pantillon, F, Knippertz, P.: 
    **Dynamics of sting-jet storm Egon over continental Europe: Impact of surface properties 
    and model resolution.** Q J R Meteorol Soc., 146: 186-210, 
    URL `<https://doi.org/10.1002/qj.3666>`_, 2020

* **2019**

  * Kern, M., Hewson, T., Schäfler, A., Westermann, R., and Rautenhaus, M.: 
    **Interactive 3D Visual Analysis of Atmospheric Fronts**, 
    IEEE Trans. Visual. Comput. Graphics, 25, 1080-1090, 
    URL `<http://doi.org/10.1109/TVCG.2018.2864806>`_, 2019

  * Kumpf, A., Rautenhaus, M., Riemer, M., and Westermann, R.: 
    **Visual Analysis of the Temporal Evolution of Ensemble Forecast Sensitivities**, 
    IEEE Trans. Visual. Comput. Graphics, 25, 98-108, 
    URL `<http://doi.org/10.1109/TVCG.2018.2864901>`_, 2019

* **2018**

  * Rautenhaus, M., Böttinger, M., Siemen, S., Hoffman, R., Kirby, R. M., Mirzargar, M., 
    Röber, N., and Westermann, R.: 
    **Visualization in Meteorology — A Survey of Techniques and Tools for Data Analysis Tasks**, 
    IEEE Trans. Visual. Comput. Graphics, 24, 3268-3296, 
    URL `<http://dx.doi.org/10.1109/tvcg.2017.2779501>`_, 2018

  * Schäfler, A., Craig, G., Wernli, H., Arbogast, P., Doyle, J. D., McTaggart-Cowan, R., Methven, J., 
    Rivière, G., Ament, F., Boettcher, M., Bramberger, M., Cazenave, Q., Cotton, R., Crewell, S., 
    Delanoë, J., Dörnbrack, A., Ehrlich, A., Ewald, F., Fix, A., Grams, C. M., Gray, S. L., Grob, H., 
    Groß, S., Hagen, M., Harvey, B., Hirsch, L., Jacob, M., Kölling, T., Konow, H., Lemmerz, C., 
    Lux, O., Magnusson, L., Mayer, B., Mech, M., Moore, R., Pelon, J., Quinting, J., Rahm, S., 
    Rapp, M., Rautenhaus, M., Reitebuch, O., Reynolds, C. A., Sodemann, H., Spengler,  T., 
    Vaughan, G., Wendisch, M., Wirth, M., Witschas, B., Wolf, K., and Zinner, T.: 
    **The North Atlantic Waveguide and Downstream Impact Experiment**, 
    Bull. American Meteor. Soc., 99, 1607-1637, 
    URL `<http://dx.doi.org/10.1175/BAMS-D-17-0003.1>`_, 2018

  * Kern, M., Hewson, T., Sadlo, F., Westermann, R., and Rautenhaus, M.: 
    **Robust Detection and Visualization of Jet-stream Core Lines in Atmospheric Flow**, 
    IEEE Trans. Visual. Comput. Graphics, 24, 893-902, 
    URL `<http://dx.doi.org/10.1109/tvcg.2017.2743989>`_, 2018

  * Kumpf, A., Tost, B., Baumgart, M., Riemer, M., Westermann, R., and Rautenhaus, M.: 
    **Visualizing Confidence in Cluster-based Ensemble Weather Forecast Analyses**, 
    IEEE Trans. Visual. Comput. Graphics, 24, 109-119, 
    URL `<http://dx.doi.org/10.1109/tvcg.2017.2745178>`_, 2018

* **2017**

  * Ferstl, F., Kanzler, M., Rautenhaus, M., and Westermann, R.: 
    **Time-hierarchical Clustering and Visualization of Weather Forecast Ensembles**, 
    IEEE Trans. Visual. Comput. Graphics, 23, 831-840, 
    URL `<http://dx.doi.org/10.1109/tvcg.2016.2598868>`_, 2017

* **2015**

  * Rautenhaus, M., Kern, M., Schäfler, A., and Westermann, R.: 
    **Three-dimensional visualization of ensemble weather forecasts - Part 1: 
    The visualization tool Met.3D (version 1.0)**, 
    Geoscientific Model Development, 8, 2329-2353, 
    URL `<http://dx.doi.org/10.5194/gmd-8-2329-2015>`_, 2015

  * Rautenhaus, M., Grams, C. M., Schäfler, A., and Westermann, R.: 
    **Three-dimensional visualization of ensemble weather forecasts - Part 2: 
    Forecasting warm conveyor belt situations for aircraft-based field campaigns**, 
    `Geosci. Model Dev., 8, 2355-2377`_,
    URL `<http://dx.doi.org/10.5194/gmd-8-2355-2015>`_, 2015

* **2014**

  * Rautenhaus, M., Grams, G., Schäfler, A., and Westermann, R.: 
    **GPU based interactive 3D visualization of ECMWF ensemble forecasts**, 
    `ECMWF Newsletter, vol. 138 (Winter 2014)`_, 34-38, 
    URL `<http://dx.doi.org/10.21957/ouo7uwp2>`_, 2014


Presentations
-------------

A very incomplete list of publicly available presentations on Met.3D:

* **2022**

  * `Met.3D: Interactive 3D visualization for rapid exploration of 
    atmospheric ensemble simulation data (Recording) <https://vimeo.com/718764679/08ce9e85ae>`_, 
    Using ECMWF’s Forecasts (UEF2022), 7-10 June 2022, Reading, UK.

* **2019**

  * `Is it time for interactivity and 3D? New approaches to analysing NWP data for 
    observational campaigns using 3D and ensemble visualization
    <https://events.ecmwf.int/event/118/contributions/497/attachments/136/238/OCBWF-Rautenhaus.pdf>`_, 
    Workshop on observational campaigns for better weather forecasts, 
    10-13 June 2019, ECMWF, Reading, UK.

* **2018**

  * `Using Met3D at ECMWF <https://www.ecmwf.int/sites/default/files/elibrary/2018/18698-using-met3d-ecmwf.pdf>`_, 
    29th European Working Group on Operational Meteorological Workstations (EGOWS), 
    15-17 October 2018, ECMWF, Reading, UK

* **2017**
  
  * `Ensemble and 3D visualization with Met.3D - recent research and software updates
    <https://www.ecmwf.int/sites/default/files/elibrary/2017/17133-ensemble-and-3d-visualization-met3d-recent-research-and-software-updates.pdf>`_, 
    16th Workshop on Meteorological Operational Systems (MOS), 1-3 March 2017, ECWMF, Reading, UK. 
    `Video available <https://ecmwf.adobeconnect.com/p2cs09c52e7/>`_.

* **2015**

  * `Interactive 3D visualization of ECMWF ensemble weather predictions 
    <https://www.ecmwf.int/sites/default/files/elibrary/2015/13333-interactive-3d-visualization-ecmwf-ensemble-weather-forecasts.pdf>`_, 
    Visualisation in Meteorology week 2015, 28 September - 2 October 2015, ECMWF, Reading, UK.


Credits
-------

The Met.3D core development team currently consists of:

* `Marc Rautenhaus`_, UHH

* `Thorwin Vogt`_, UHH

* `Christoph Fischer`_, UHH

* Maximilian Hartz, UHH student

We acknowledge the following individuals for their contribution:

* Andreas Beckert, UHH (former core member - many thanks!)
* Susanne Fuchs, UHH (former core member - many thanks!)
* Kamesh Modali, UHH (former core member - many thanks!)
* Justus Jakobi, UHH student
* Alexander Klassen, UHH student
* Jonas Lefert, UHH student
* Marcel Meyer, UHH
* Michael Kern, TUM (former core member - many thanks!)
* Alexander Kumpf, TUM (former core member - many thanks!)
* Bianca Tost, TUM (former core member - many thanks!)
* Mathias Kanzler, TUM
* Florian Ferstl, TUM
* Fabian Schöttl, TUM student
* Christoph Heidelmann, TUM student
* Florian Hauer, TUM student
* Max Bandle, TUM student
* Jessica Blankenburg, TUM student
* Raphael Kriegmair, LMU student
* Philipp Kaiser, TUM student
* Theresa Diefenbach, LMU student
* Robert Redl, LMU
* Alexander Thole, TUM student

We acknowledge support from `ECMWF <https://www.ecmwf.int/>`_, 
in particular by Tim Hewson and Stephan Siemen and his group. 
Access to ECMWF data for our work has been kindly provided in the context of 
the ECMWF special project "Support Tool for HALO Missions".

.. _C++: https://isocpp.org/
.. _OpenGL 4: https://www.opengl.org/
.. _Geosci. Model Dev., 8, 2329-2353: http://www.geosci-model-dev.net/8/2329/2015/gmd-8-2329-2015.html
.. _Geosci. Model Dev., 8, 2355-2377: http://www.geosci-model-dev.net/8/2355/2015/gmd-8-2355-2015.html
.. _ECMWF Newsletter, vol. 138 (Winter 2014): http://www.ecmwf.int/en/elibrary/14581-newsletter-no138-winter-2013/14
.. _Visual Data Analysis Group: https://www.hcds.uni-hamburg.de/en/research/fg-vda.html
.. _Hub of Computing and Data Science: https://www.hcds.uni-hamburg.de/en.html
.. _University of Hamburg: https://www.uni-hamburg.de/en.html
.. _Computing Graphics & Visualization Group: https://www.cs.cit.tum.de/cg/cover-page/
.. _Technical University of Munich: https://www.tum.de/
.. _Waves to Weather: http://www.wavestoweather.de/
.. _CLICCS: https://www.cliccs.uni-hamburg.de/research/computing/visualization.html
.. _Marc Rautenhaus: https://www.hcds.uni-hamburg.de/en/research/fg-vda/people/rautenhaus.html
.. _Ph.D. thesis: http://mediatum.ub.tum.de/?id=1253343
.. _Thorwin Vogt: https://www.hcds.uni-hamburg.de/en/research/fg-vda/people/vogt.html
.. _Christoph Fischer: https://www.hcds.uni-hamburg.de/en/research/fg-vda/people/fischer.html
