Actors
======

The visualization objects are called actors in Met.3D. They can be categorized into broadly three categories:

* :ref:`scene_context`
* :ref:`gridded_data`
* :ref:`traj_data`

The subsections of this page provide a short description of the actors, 
as well as descriptions of how to modify common actor properties. 
Major characteristics of the actors are listed, however, many actors provide more properties than currently documented.


.. _scene_context:

Actors providing scene context
------------------------------

These actors as such do not hold any gridded data, but they provide the infrastructure needed for building the scene context.

.. toctree::
   :maxdepth: 1

   actors/transfer_function_color_map
   actors/basemap
   actors/graticule
   actors/image
   actors/moveable_poles
   actors/volume_bbox


.. _gridded_data:

Actors that manage gridded data
-------------------------------

These actors hold the gridded data and provide the various methods needed for visualizing the features.

.. note:: 
   For all actors that visualize gridded data please note the following:

   * The “variables” of the actors can be synchronized with the global time and ensemble settings 
     but don’t have to be synchronized (e.g. if different ensemble members shall be compared).
   * Availability of ensemble functionality depends on the data pipeline 
     (if connected to an ensemble dataset, the pipeline can provide the data of different ensemble members 
     or compute statistical quantities including ensemble mean and probabilities).


The following actors are currently able to visualize gridded data:

.. toctree::
   :maxdepth: 1

   actors/horizontal_cross_section
   actors/isosurface_intersection
   actors/jetcore_detection
   actors/vertical_cross_section
   actors/volume_raycaster
   actors/surface_topography
   actors/skewt
   actors/radar_actor


Adding variables
~~~~~~~~~~~~~~~~

For all these actors, there exists an **add new variable** option.
When clicking on execute, the list of variables available in the data loaded with the data pipeline is displayed 
and the user can choose the variable that needs to be added.

.. figure:: ./figs/actors/add_variable_property.webp
    :figwidth: 100%
    :align: left
    :alt: A figure should appear here
  

.. figure:: ./figs/actors/add_variable_dialog.webp
    :figwidth: 100%
    :align: left
    :alt: A figure should appear here
  

The user needs to repeat this step to add multiple variables or even the same variable multiple times. 
Also, once a variable has been added, it has options so that it can be removed or changed.


Getting information about a variables data
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

It is possible to get some statistics about the data of a given variable.

.. figure:: ./figs/actors/variable_statistics.webp
    :figwidth: 100%
    :align: left
    :alt: A figure should appear here
  

Also, it is possible to export the data to an external file for debugging purposes.

.. figure:: ./figs/actors/export_variable.webp
    :figwidth: 100%
    :align: left
    :alt: A figure should appear here


Smoothing the data
~~~~~~~~~~~~~~~~~~

Met.3D provides options to smoothen the data.

.. figure:: ./figs/actors/smooth_variable.webp
    :figwidth: 100%
    :align: left
    :alt: A figure should appear here


Currently, horizontal smoothing is possible using either distance based or grid cell based modes.
We recommend using the smooth mode "horizontalBoxBlur_distance" with "symmetric" boundary conditions. 


Visualizing ensemble data
~~~~~~~~~~~~~~~~~~~~~~~~~

Met.3D supports ensemble visualization. 
Each variable by default is synchronized to temporal extents and ensemble size 
based on the data loaded using the data pipeline. 
By, turning off the ensemble synchronization, the user can choose the members and the ensemble mode. 
Currently individual member or ensemble mean, standard deviation, min, max, max-min, above or below a threshold are supported.

.. figure:: ./figs/actors/visualize_variable_ensemble.webp
    :figwidth: 100%
    :align: left
    :alt: A figure should appear here


.. _traj_data:

Actors that manage trajectory data
----------------------------------

Met.3D currently includes one actor that implements functionality to visualize trajectory data 
(i.e., Lagrangian particle trajectories / path lines, or streamlines). These can either be
precomputed or computed within the application from a given wind field.

The following actors are currently able to visualize trajectory data:

.. toctree::
   :maxdepth: 1

   actors/trajectory

See the actor documentation on further instruction how to load and display precomputed or
on-the-fly computed trajectories.
