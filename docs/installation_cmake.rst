Met.3D installation with cmake
==============================

This page provides installation guidelines for installing Met.3D on
`openSUSE`_ and `Ubuntu 16.04 LTS`_ systems using the `cmake`_ build system.
Met.3D requires a number of libraries to be
installed and a few external data packages to be downloaded. Most of these
packages can be installed via the respective system package managers (`YaST`_ or
`aptitude`_), however, a few have to be downloaded and
compiled manually.

.. _openSUSE: https://software.opensuse.org
.. _Ubuntu 16.04 LTS: http://releases.ubuntu.com/16.04/
.. _cmake: https://cmake.org/
.. _YaST: http://yast.github.io/
.. _aptitude: https://help.ubuntu.com/lts/serverguide/aptitude.html

.. note::
    The installation guidelines have been tested with openSUSE 13.X systems but
    should be directly applicable to the current
    openSUSE 42. Under Ubuntu, we have tested with version 16.04 LTS.

System requirements: You need an OpenGL 4.3 capable graphics card and an
appropriate Linux driver to run Met.3D. The driver will most likely be a
proprietary driver; open-source drivers for Linux currently do
not provide the required capabilities. Before you continue with the
installation, make sure that graphics card and driver are installed. If
everything is installed correctly, the ``glxinfo`` command should output
something similar to (the important thing is the OpenGL version > 4.3):

::

    glxinfo | grep OpenGL
    
    OpenGL vendor string: NVIDIA Corporation
    OpenGL renderer string: GeForce GTX TITAN/PCIe/SSE2
    OpenGL core profile version string: 4.4.0 NVIDIA 340.96
    OpenGL core profile shading language version string: 4.40 NVIDIA via Cg compiler

    
A) Install available packages via your package manager (YaST, aptitude, ...)
----------------------------------------------------------------------------

For openSUSE 13.X, these repositories are required (similar for other
openSUSE versions):

- http://download.opensuse.org/repositories/Application:/Geo/openSUSE_13.X/
- http://download.opensuse.org/repositories/home:/SStepke/openSUSE_13.X/

Packages that need to be installed via YaST (or the system's repository 
manager):

- libqt4 and libqt4-devel
- liblog4cplus and log4cplus-devel
- gdal, libgdal and libgdal-devel
- netcdf, netcdf-devel
- hdf5, libhdf5 and hdf5-devel
- glew and glew-devel
- libfreetype and freetype-devel
- grib_api and grib_api-devel
- libGLU
- gsl and gsl_devel

- ... and we may have forgotten some. Please tell us!

For Ubutuntu 16.04, packages that need to be installed via aptitude:

- qt4-default, qt4-qmlviewer and libqt4-dev
- liblog4cplus-1.1-9 and liblog4cplus-dev
- gdal-bin, libgdal1i and libgdal-dev (deprecated, recommend to download latest version)
- libhdf5-10 and libhdf5-dev (in case it does not support GEOS, install the package manually)
- glew-utils and libglew-dev
- libfreetype6 and libfreetype6-dev
- libgrib-api0, libgrib-api-dev and libgrib-api-tools
- libglu1-mesa and libglu1-mesa-dev
- libgsl2 and libgsl-dev


B) Install required libraries from their sources
------------------------------------------------

Some libraries in this section may not be available as rpm/apt packages and
need to be complied manually.

1) glfx
~~~~~~~

Get the glfx sources from: https://code.google.com/p/glfx/

::

    cd glfx
    cmake -DCMAKE_INSTALL_PREFIX:PATH=/your/target/dir CMakeLists.txt
    make -j 12
    make install

To make it easier for cmake to automatically find the libraries, choose one directory from ``cmake/common_settings.cmake`` as /your/target/dir. Test


2) qcustomplot
~~~~~~~~~~~~~~

Get the qcustomplot sources from: http://www.qcustomplot.com/

You will need the archives ``QCustomPlot.tar.gz`` and ``QCustomPlot-sharedlib.tar.gz``.

Extract the ``QCustomPlot.tar.gz`` archive (the ``/qcustomplot`` directory)
and put the contents of ``QCustomPlot-sharedlib.tar.gz`` **inside** the
``/qcustomplot`` directory. Go to 

::

    qcustomplot/qcustomplot-sharedlib/sharedlib-compilation

and run:

::

    qmake
    make

Next, copy the resulting libraries and the qcustomplot.h header to directories
where cmake can find them automatically (look at ``cmake/common_settings.cmake``). These files are required:

::

    ./include:
    qcustomplot.h

    ./lib64:
    libqcustomplotd.so -> libqcustomplotd.so.1.2.1*
    libqcustomplotd.so.1 -> libqcustomplotd.so.1.2.1*
    libqcustomplotd.so.1.2 -> libqcustomplotd.so.1.2.1*
    libqcustomplotd.so.1.2.1*
    libqcustomplot.so -> libqcustomplot.so.1.2.1*
    libqcustomplot.so.1 -> libqcustomplot.so.1.2.1*
    libqcustomplot.so.1.2 -> libqcustomplot.so.1.2.1*
    libqcustomplot.so.1.2.1*


3) netcdf-cxx4
~~~~~~~~~~~~~~

Get the sources of the CURRENT (not the old!) NetCDF C++ interface from
http://www.unidata.ucar.edu/downloads/netcdf/index.jsp
(choose the latest stable distribution). The C++ library requires the regular
C library to be installed (see rpm packages above).

::

    ./configure --prefix=/your/target/dir
    make -j 12
    make check
    make install

To make it easier for cmake to automatically find the libraries, choose one directory from ``cmake/common_settings.cmake`` as /your/target/dir.


4) gdal-2.x (only Ubuntu)
~~~~~~~~~~~~~~~~~~~~~~~~~
Get the sources of the current GDAL version from http://download.osgeo.org/ and compile the source code:

::

    ./configure --prefix=/your/target/dir
    make -j 12
    make check
    make install

To make it easier for cmake to automatically find the libraries, choose one directory from ``cmake/common_settings.cmake`` as /your/target/dir.


5) hdf5-1.x (only Ubuntu)
~~~~~~~~~~~~~~~~~~~~~~~~~
Before compiling HDF5, install the package **libgeos-dev** to enable GEOS support.
Get the sources of the current HDF5 version from https://support.hdfgroup.org/HDF5/ and compile the source code:

::

    ./configure --prefix=/your/target/dir
    make -j 12
    make check
    make install

To make it easier for cmake to automatically find the libraries, choose one directory from ``cmake/common_settings.cmake`` as /your/target/dir.


C) Download source and data packages
------------------------------------

We recommend to place the following packages along with the Met.3D sources into
a specific directory structure.

Create a base directory ``met.3d-base`` and a subdirectory ``third-party``:

::

    met.3d-base/
        third-party/

Change into ``third-party`` to execute the following commands.


1) qtpropertybrowser
~~~~~~~~~~~~~~~~~~~~

Met.3D requires the qtpropertybrowser framework from the "qt-solutions"
repository. The qtpropertybrowser sources are directly compiled into the
Met.3D executable and hence do not have to be build beforehand. They can
be downloaded with git:

::

    [in met.3d-base/third-party]
    git clone https://github.com/qtproject/qt-solutions.git


2) Fonts
~~~~~~~~

Met.3D requires a TrueType font file. We recommend the "FreeSans" font from
the GNU FreeFont package. It can be downloaded from
http://ftp.gnu.org/gnu/freefont/. At the time of writing, the most recent
version is 20120503:

::

    [in met.3d-base/third-party]
    wget http://ftp.gnu.org/gnu/freefont/freefont-ttf-20120503.zip
    unzip freefont-ttf-20120503.zip


3) Vector and raster map, coastline and country borderline data
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Met.3D requires a base map image in GeoTIFF format, as well as coastline and
country borderline vector data in shapefile format. we recommend to use the free
data from http://www.naturalearthdata.com. The medium resolution files (50m) work
fine (they require roughly 300 MB of disk space).

For coastline data, we use the "Coastline" dataset
(http://www.naturalearthdata.com/downloads/50m-physical-vectors/):

::

    [in met.3d-base/third-party]
    mkdir naturalearth
    cd naturalearth
    wget http://www.naturalearthdata.com/http//www.naturalearthdata.com/download/50m/physical/ne_50m_coastline.zip
    unzip ne_50m_coastline.zip

For country boundaries, we use the "Admin 0 – Boundary Lines" dataset
(http://www.naturalearthdata.com/downloads/50m-cultural-vectors/):

::

    [in met.3d-base/third-party/naturalearth]
    wget http://www.naturalearthdata.com/http//www.naturalearthdata.com/download/50m/cultural/ne_50m_admin_0_boundary_lines_land.zip
    unzip ne_50m_admin_0_boundary_lines_land.zip

For the raster basemap, we use the "Cross Blended Hypso with Shaded Relief and
Water" dataset
(http://www.naturalearthdata.com/downloads/50m-raster-data/50m-cross-blend-hypso/):

::

    [in met.3d-base/third-party/naturalearth]
    wget http://www.naturalearthdata.com/http//www.naturalearthdata.com/download/50m/raster/HYP_50M_SR_W.zip
    unzip HYP_50M_SR_W.zip


You should now have the following directory structure (``...`` denotes other
files):

::

    met.3d-base/
        third-party/
            qt-solutions/
                qtpropertybrowser/
                    *
                ...
            freefont-20120503/
                FreeSans.ttf
                ...
            naturalearth/
                HYP_50M_SR_W/
                    HYP_50M_SR_W.tif
                    ...
                ne_50m_coastline.shp
                ne_50m_admin_0_boundary_lines_land.shp
                ...


D) Checkout Met.3D from the GIT repository
------------------------------------------

The latest version of Met.3D can be checked out from
https://gitlab.com/wxmetvis/met.3d/.

Place the repository into the ``met.3d-base`` base directory:

::

    [in met.3d-base]
    git clone -b release_1.1 https://gitlab.com/wxmetvis/met.3d.git


E) Configure cmake for Met.3D
-----------------------------

We provide cmake scripts for Makefile creation and compilation of Met.3D. 
You can either build Met.3D from the command line, or use a cmake
GUI (e.g., ``cmake-curses-gui``, ``cmake-gui``) to configure cmake.
We recommend to use a cmake GUI, or, alternatively, start the build process
within C++ IDEs like QtCreator, CLion, or Visual Studio Code (these typically 
provide functionality to open ``CMakeLists.txt`` and to run the build process).

From the command line:

First, in ``met.3d-base/``, create a subdirectory ``build`` into which the executable can
be built and change into this directory:

::

    [in met.3d-base]
    mkdir build
    cd build

Create a ``Makefile`` by:

::

    [in met.3d-base/build]
    cmake -DCMAKE_BUILD_TYPE=RELEASE ../met.3d

Met.3D can also be built in *debug mode*; change ``-DCMAKE_BUILD_TYPE=RELEASE``
to ``-DCMAKE_BUILD_TYPE=DEBUG`` to achieve this.
  
If some libraries are not located within the default header/library folders
(given in common_settings.cmake), it is likely that you have to manually set
the include directory and used libraries for a certain package.
For example, if cmake could not find the include directory of GDAL, it will
output something like ``missing GDAL_INCLUDE_DIR``. In that case, add
``-DGDAL_INCLUDE_DIR=/real/path/to/gdal/includes`` to the makefile command and
run cmake again. Or use the GUI to set the missing directories and libraries
and restart the configuring and generation process.


F) Compile Met.3D
-----------------

After cmake has created the ``Makefile``, run make (the "-j 12" option for make
starts 12 parallel compilation threads, modify this number to match the number
of CPU cores in your system).

::

    [in met.3d-base/build]
    make -j 12

Compilation may take a few minutes. If no errors are reported, an executable
named ``met3D`` should be created in the build directory.

G) Configure Met.3D
-------------------

Modify the software configuration to match your system.

-   In ``met.3d-base/met.3d/config/``, copy the files ``default_frontend.cfg.template``
    and ``default_pipeline.cfg.template`` to local configuration files, e.g. to
    ``met.3d-base/config/frontend.cfg`` and ``met.3d-base/config/pipeline.cfg`` (these
    filenames are used in the following but you can choose other names as well).
    Modify the paths in ``pipeline.cfg`` to match the paths at which the forecast
    data is stored on your system. Modify ``frontend.cfg`` to specify the
    configuration of Met.3D after start-up. If you have followed the suggested
    directory structure, you should not need to modify ``frontend.cfg`` for first
    experiments with Met.3D. Note that you can use environement variables in the
    paths defined in the configuration files. In the example installation described
    in this document, two envrionment variables (``MET3D_HOME`` and ``MET3D_BASE``) are
    used (see below).



H) Start Met.3D
---------------

Before Met.3D is started, the environment variable ``MET3D_HOME`` needs to be set
to the Met.3D source directory (alternatively, at least the subdirectories
``/src/glsl`` and ``/config`` need to be accessible):

::

    export MET3D_HOME=/your/path/to/met.3d-base/met.3d

For the example configuration described in this document, an additional
environment variable ``MET3D_BASE`` is used in the configuration files to refer to
the paths with third-party data (see default_frontend.cfg.template; feel free
to change this if you like):

::

    export MET3D_BASE=/your/path/to/met.3d-base

To start Met.3D, type (change the paths to the configuration files if they
are stored in a different location):

::

    [e.g. in met.3d-base/build]
    ./Met3D --pipeline=$MET3D_HOME/config/pipeline.cfg --frontend=$MET3D_HOME/config/frontend.cfg

.. note::
    Depending on the amount of available forecast data, the software
    may take a while to start. Also, on first startup the compilation of the
    OpenGL shader programs may take a while.

.. note::
    On first start-up, you should only see an empty window. Please follow the user
    guide to learn how to create visualizations.
