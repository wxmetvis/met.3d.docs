Release notes
=============

Version 1.1 - April 25, 2017
----------------------------

New features:
~~~~~~~~~~~~~

- An interactive transfer function editor and functionality for basic direct
  volume rendering (DVR) has been added.

- Support for textured filled contours in horizontal sections to facilitate
  stippling and hatching.

- The synchronization control supports restriction of allowed times to
  available times in selected datasets and provides enhanced animation control.
  
- Visualizations can be directly saved to a graphics file from a scene view
  by pressing "s".

- The trajectory actor can be created at runtime.

- Support for cmake to compile the source code.

- Enhanced support for contour line appearance in 2D sections.

- Support for different camera navigation modes: *move camera*, *rotate scene*,
  *2D top view*.
  
- Many further small usability improvements...

Bugs fixed:
~~~~~~~~~~~

- Lots...
