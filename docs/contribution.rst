Met.3D contribution guidelines
==============================

This section describes basic guidelines for contributing to the Met.3D project.
`GIT workflow`_ describes the basic workflow for fixing bugs or adding
new features to the code, and `C++ coding style`_ and `GLSL coding style`_ 
describe the project's code conventions.

GIT workflow
------------

The Met.3D main repository is hosted on `GitLab`_, with a mirror maintained 
on `Bitbucket`_. We use the `issue tracker`_ on GitLab. 
We welcome bug and issue reports from users. If you want to report an issue,
please create an account on `GitLab`_ and contact us with your account. We will
add you to the project as an issue reporter.

There are many different GIT workflows out there, as described in this GitLab
`overview article`_.
To contribute code to Met.3D, please fork the main repository, change the code
in a feature branch in your own fork, and create a merge request to feed your 
work back into the main repository. 
If you are working on a specific issue, don't forget to assign the issue to 
yourself in the issue tracker.
Read GitLab's `Project forking workflow`_ for more detailed instructions.

Also read and follow the GitLab `documentation for merge requests`_, issues, etc.
when working with the code. Our small difference is that your feature branches live in
your forked repository instead of the main repository.

.. _GitLab: https://gitlab.com/wxmetvis/met.3d
.. _Bitbucket: https://bitbucket.org/wxmetvis/met.3d
.. _issue tracker: https://gitlab.com/wxmetvis/met.3d/issues
.. _overview article: https://gitlab.com/help/workflow/gitlab_flow.md
.. _Project forking workflow: https://gitlab.com/help/workflow/forking_workflow.md
.. _documentation for merge requests: https://gitlab.com/help/workflow/gitlab_flow.md#mergepull-requests-with-gitlab-flow

GIT recipes
~~~~~~~~~~~

This section contains short recipes for common git tasks.

* Synchronize between different repositories

  - To update the master branch of your repository from the public main 
    repository, execute in your local clone:
    
    + ``git remote add public_main https://gitlab.com/wxmetvis/met.3d.git`` (only
      once, check with ``git remote -v`` if you have already added the remote repository)
    
    + ``git checkout master``
    
    + ``git fetch public_main master``
    
    + ``git merge FETCH_HEAD``
    
    + ``git push origin master``
    
  - To move a branch between two repositories A and B, assuming you are working
    in your local copy of A and want to copy branch ``example_branch`` from B
    to A. Assume that ``example_branch`` branches off ``master`` in B and ``master``
    is the same in A and B.
    
    + ``git remote add repo_B <URL of B>`` (only
      once, check with ``git remote -v`` if you have already added the remote repository)
      
    + ``git checkout master``
    
    + ``git checkout -b example_branch``
    
    + ``git fetch repo_B example_branch``
    
    + ``git merge FETCH_HEAD``


C++ coding style
----------------

Met.3D's C++ coding style is based on the `Qt Coding Style`_, which we have modified
in some parts. 

.. note::
    Reference: Some of the following text and examples have been taken and adapted from 
    the `Qt Coding Style`_. Please see the referenced document for the original style.

.. note::
    You may encounter lots of code in Met.3D that breaks the following conventions.
    This is mostly old code, which will be reformatted with time. For new code,
    please use the following conventions.
    
.. _Qt Coding Style: https://wiki.qt.io/Qt_Coding_Style

General file structure
~~~~~~~~~~~~~~~~~~~~~~

Both header (\*.h) and source (\*.cpp) files start with a common header that
indicates license and copyright information of the file:

.. code-block:: C++
    :linenos:

    /******************************************************************************
    **
    **  This file is part of Met.3D -- a research environment for the
    **  three-dimensional visual exploration of numerical ensemble weather
    **  prediction data.
    **
    **  Copyright 2016 ...
    **
    **  Met.3D is free software: you can redistribute it and/or modify
    **  it under the terms of the GNU General Public License as published by
    **  the Free Software Foundation, either version 3 of the License, or
    **  (at your option) any later version.
    **
    **  Met.3D is distributed in the hope that it will be useful,
    **  but WITHOUT ANY WARRANTY; without even the implied warranty of
    **  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    **  GNU General Public License for more details.
    **
    **  You should have received a copy of the GNU General Public License
    **  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
    **
    *******************************************************************************/

After the header, C++ header files follow the structure:

.. code-block:: C++
    :linenos:

    #ifndef MYMET3DCLASS_H
    #define MYMET3DCLASS_H

    // standard library imports
    #include <memory>

    // related third party imports
    #include <GL/glew.h>

    // local application imports
    #include "gxfw/mactor.h"

    namespace Met3D
    {

    /**
     @brief The class MMyMet3DClass is briefly described in this Doxygen 
     comment.
    */
    class MMyMet3DClass
    {
    public:
        MMyMet3DClass();
        ~MMyMet3DClass();

        /**
         This comment documents the following function in Doxygen style. All
         methods should be briefly described in the C++ header file. Do not
         repeat the description in the source file.
        */
        void setMyProperty(int someNumber);

    protected:
        // protected method and variable definitions ...
        
        int myProperty;

    private:
        // private method and variable definitions ...
    };

    } // namespace Met3D

    #endif // MYMET3DCLASS_H

After the header, C++ source files follow the structure:

.. code-block:: C++
    :linenos:

    #include "mymet3dclass.h"

    // standard library imports
    #include <iostream>

    // related third party imports
    #include <log4cplus/loggingmacros.h>

    // local application imports
    #include "util/mutil.h"

    using namespace std;

    namespace Met3D
    {

    /******************************************************************************
    ***                     CONSTRUCTOR / DESTRUCTOR                            ***
    *******************************************************************************/

    MMyMet3DClass::MMyMet3DClass()
        : myProperty(42)
    {
        // ... do something ...
    }


    MMyMet3DClass::~MMyMet3DClass()
    {
        // ... do something ...
    }


    /******************************************************************************
    ***                            PUBLIC METHODS                               ***
    *******************************************************************************/

    void MMyMet3DClass::setMyProperty(int someNumber)
    {
        myProperty = someNumber;
    }

    
    /******************************************************************************
    ***                          PROTECTED METHODS                              ***
    *******************************************************************************/

    // ... some definitions ...
    
    
    /******************************************************************************
    ***                           PRIVATE METHODS                               ***
    *******************************************************************************/

    // ... some definitions ...

    
    } // namespace Met3D


Indentation
~~~~~~~~~~~

* Use four (4) spaces for indentation. 
* Do not use tabs.

Blank lines
~~~~~~~~~~~

* Two (2) blank lines follow each method definition in the source files.
* Use a blank line to separate comments that describe not only the next
  code line but the next section of code.

Variables declaration and naming
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

We use the same variable declaration style as the `Qt Coding Style`_:

* Each variable is declared on a separate line.
* Avoid short or meaningless name (e.g. :code:`r`, :code:`rwqrr`, :code:`laksjdiuqk`).
* Only use single character names (e.g. :code:`i`) for counters and temporaries,
  where the meaning is obvious.
* For pointers or references, always use a single space between the type and 
  ``*`` or ``&``, but no space between the ``*`` or ``&`` and the variable name.
  
.. code-block:: C++

    // Wrong:
    int a, b;
    MTask *c, *d;
    MTask * t1;

    // Correct:
    int height;
    int width;
    MTask *nameOfThisTask;
    MTask *nameOfThatTask;
    MTask &myFancyTaskReference;
  
* Wait to declare a variable until it is needed.
* Use camel-case: Variables and functions start with a lower-case letter. 
  Each consecutive word in a variable's name starts with an upper-case letter.
* Avoid abbreviations.  

.. code-block:: C++

    // Wrong:
    short Cntr;
    char ITEM_DELIM = ' ';
    
    // Correct:
    short counter;
    char itemDelimiter = ' ';

* Classes always start with an upper-case letter. 
* Public classes start with an 'M' (e.g. :code:`MTask`) followed by an upper case letter.
* Acronyms are camel-cased (e.g. :code:`MHsvColourBar`, not :code:`MHSVColourBar`).
    
Line breaks
~~~~~~~~~~~

* Keep all code and comment lines shorter than 80 columns. (Hint: Most IDEs 
  provide an option to highlight the 80-character-border.)
* Place commas at the end of a wrapped line, operators at the beginning of a
  new line.
  
.. code-block:: C++

    // Wrong:
    if (longExpression +
        otherLongExpression +
        otherOtherLongExpression) 
    {
        // .. some code ..
    }
    
    // Correct:
    if (longExpression
        + otherLongExpression
        + otherOtherLongExpression) 
    {
        // .. some code ..
    }  
   
Braces
~~~~~~

* Use separate lines for braces.
* Also use braces if a code block only contains a single line.

.. code-block:: C++

    // Wrong:
    if (condition) {
        // .. some code ..
    } else {
        // .. some code ..
    }   
    
    // Correct:
    if (condition)
    {
        // .. some code ..
    }
    else
    {
        // .. some code ..
    }
    
Parentheses
~~~~~~~~~~~

* Group expressions by using parentheses.

.. code-block:: C++

    // Wrong:
    if (a && b || c)
    
    // Correct:
    if ((a && b) || c)
    
    // Wrong:
    a + b & c
    
    // Correct:
    (a + b) & c

Whitespace
~~~~~~~~~~

* Always use a single white space after a keyword.

.. code-block:: C++

    // Wrong:
    if(condition)
    
    // Correct:
    if (condition)
    
* Use a single white space after a comma.
* Use a single white space before and after a mathematical or logical operator.

.. code-block:: C++

    // Wrong:
    myFunction(1,2,3);
    a= 2+3;
    
    // Correct:
    myFunction(1, 2, 3);
    a = 2 + 3;
    
* You may use additional white space where it enhances readability of the code, but
  please use sparingly.
  
.. code-block:: C++

    MStructuredGrid *mean   = nullptr;
    MStructuredGrid *stddev = nullptr;
  
    
Switch statements
~~~~~~~~~~~~~~~~~

* Case labels are in the same column as the switch statement.
* Every case must have a break statement at the end. If the break is avoided
  intentionally, indicate so by a comment -- unless the next case follows immediately.
  
.. code-block:: C++

    switch (myEnum) 
    {
    case Value1:
        doSomething();
        break;
    case Value2:
    case Value3:
        doSomethingElse();
        // fall through
    default:
        defaultHandling();
        break;
    }

Type casting
~~~~~~~~~~~~

* Avoid C-style casts where possible.

.. code-block:: C++

    // Wrong:
    char* blockOfMemory = (char* ) malloc(data.size());

    // Correct:
    char *blockOfMemory = reinterpret_cast<char*>(malloc(data.size()));
    
    
Qt specifics
~~~~~~~~~~~~

Met.3D heavily builds on Qt.

* Prefer Qt types to standard library types wherever possible (e.g. use :code:`QVector`
  instead of :code:`std::vector`).
* Use the Qt :code:`foreach` statement to iterate over container elements.

.. code-block:: C++

    QList<MTask*> taskQueue;
    foreach(MTask *task, taskQueue)
    {
        doSomethingWith(task);
    }

* To avoid compiler warnings for unused parameters in empty (e.g. virtual)
  functions, use the :code:`Q_UNUSED` macro.
  
.. code-block:: C++

    virtual void onOtherActorCreated(MActor *actor) { Q_UNUSED(actor); }
    
Logging and console output
~~~~~~~~~~~~~~~~~~~~~~~~~~

Met.3D uses the `log4cplus`_ library for logging and console output.

* Do not use :code:`std::cout` to print output, use the log4cplus functions instead.
* Different functions are available for debug, error, info output (and others).
* Use the globally defined log object ``mlog`` for logging output.

.. code-block:: C++

    // Wrong:
    std:cout << "Some debug message." << endl;
    
    // Correct:
    LOG4CPLUS_DEBUG(mlog, "Some debug message.");


.. _log4cplus: https://sourceforge.net/p/log4cplus/wiki/Home/

Comments
~~~~~~~~

* Write enough comments so that your code can be easily understood by a person
  who reads the code for the first time (consider being this person and check
  if you would understand what your code does).
* Doxygen comments used to document methods in the header files use
  the format shown in the template header above.
* Every function of a class (unless very obvious)
  should be commented in the corresponding header file.
* Inline comments are placed in the code by using the :code:`//` indicator. Do not use
  :code:`/*  ... */`.
* Write comments as complete English sentences, starting with a capital letter and ending
  with a period.
* Comment should add information, not state the obvious.
  
.. code-block:: C++

    // Wrong:
    
    // next I call my function
    filterUserComments(comments);
    
    // Correct:
    
    // Users may have left weird comments, so get rid of those.
    filterUserComments(comments);

* Very short comments can be placed at the end of a code line. These do not have
  to be complete sentences.

.. code-block:: C++

    k = 0; // number of ensemble members
    
Compiler warnings
~~~~~~~~~~~~~~~~~

* Write your code such that as few compiler warnings as possible appear.
  Ideally, your code should have no warnings at all.
    
    
GLSL coding style
-----------------

Where applicable, also use the above C++ style for coding GLSL shader programs.

General file structure
~~~~~~~~~~~~~~~~~~~~~~

Met.3D uses the `glfx framework`_. glfx unifies vertex, geometry and fragment 
shader programs in a single source file, a ``.glsl`` file. 
Use the same license and copyright header as for the C++ files. Following the
header, the general structure of the ``.glsl`` files in Met.3D is:

.. _glfx framework: https://code.google.com/archive/p/glfx/

.. code-block:: GLSL
    :linenos:

    /****************************************************************************
    ***                             CONSTANTS                                 ***
    *****************************************************************************/

    // Use the `const` datatype instead of `#define`.
    const float MISSING_VALUE = -999.E9;

    
    /****************************************************************************
    ***                             INTERFACES                                ***
    *****************************************************************************/

    // Interfaces connect different shader stages.
    interface GStoFS
    {
        smooth vec4 colour;
    };


    /****************************************************************************
    ***                             UNIFORMS                                  ***
    *****************************************************************************/

    // Definition of uniform variables common to all shader stages.
    uniform mat4 mvpMatrix;


    /****************************************************************************
    ***                             INCLUDES                                  ***
    *****************************************************************************/

    // Include files.
    #include "filename.glsl"

    /****************************************************************************
    ***                           VERTEX SHADER                               ***
    *****************************************************************************/

    shader VSmain(in vec2 worldXY, out vec3 worldPos)
    {
        // .. some code ..
    }


    /****************************************************************************
    ***                          GEOMETRY SHADER                              ***
    *****************************************************************************/

    shader GSmain(in vec3 worldPos[], out GStoFS output)
    {
        // .. some code ..
    }


    /****************************************************************************
    ***                          FRAGMENT SHADER                              ***
    *****************************************************************************/

    shader FSmain(in GStoFS input, out vec4 fragColor)
    {
        // .. some code ..
    }


    /****************************************************************************
    ***                             PROGRAMS                                  ***
    *****************************************************************************/

    // You can define multiple shader programs using the following syntax:
    //    vs(number_of_gl_version)=functionName();
    //    gs(number_of_gl_version)=functionName() : in(geom_in), out(geom_out, max_vertices=max, stream=num)
    //    fs(number_of_gl_version)=functionName();

    program SomeFancyOpenGlShader
    {
        vs(420)=VSmain();
        gs(420)=GSmain() : in(points), out(line_strip, max_vertices = 4);
        fs(420)=FSmain();
    };

Comments
~~~~~~~~

* Since there are no header files in GLSL, please put Doxygen-style comments
  for a function above the function definition.
    