Met.3D actors
=============

This section provides a short overview of the actors currently
implemented in Met.3D. Major characteristics of the actors are listed,
however, many actors provide more properties than currently documented.

.. note:: For all actors that visualize forecast data please note the following:

    -  The "variables" (see :ref:`first_steps_adding_fc_data`) of the
       actors can be synchronized with the global time and ensemble settings
       but don't have to be synchronized (e.g. if different ensemble members
       shall be compared).

    -  Availability of ensemble functionality depends on the data pipeline
       (if connected to an ensemble dataset, the pipeline can provide the
       data of different ensemble members or compute statistical quantities
       including ensemble mean and probabilities).

Graticule
---------

.. _fig_actorref_graticule:
.. figure:: ./figs/actorref_graticule.png
    :figwidth: 100 %
    :align: left
    :alt: A figure should appear here
    
    Graticule actor.

The graticule actor draws a graticule and coast and border lines into a
scene (:numref:`fig_actorref_graticule`).

-  The distance and colour of the graticule lines can be customised.

-  Coast and border lines are read from a shapefile (in this manual data
   from `Natural Earth`_ (see :doc:`installation`) are used; other shapefile
   datasets can be used as well).

.. _Natural Earth: http://www.naturalearthdata.com


Basemap
-------

.. _fig_actorref_basemap:
.. figure:: ./figs/actorref_basemap.png
    :figwidth: 100 %
    :align: left
    :alt: A figure should appear here
    
    Base map actor.

As the name says, the base map actor draws a base map into a scene (
:numref:`fig_actorref_basemap`).

-  Map data can be read from an arbitrary geo-referenced GeoTIFF file
   (in this manual data from `Natural Earth`_ (see :doc:`installation`) is used;
   other GeoTIFF datasets can be used as well).

-  The bounding box can be customised, i.e. the map file can be larger
   than the displayed region.

-  Colour saturation of the loaded base map image can be adjusted (for
   example, to desaturate the image colours).

   
Volume bounding box
-------------------

.. _fig_actorref_volumebbox:
.. figure:: ./figs/actorref_volumebbox.png
    :figwidth: 100 %
    :align: left
    :alt: A figure should appear here
    
    Volume bounding box actor.

The volume bounding box actor draws a bounding box into the scene (
:numref:`fig_actorref_volumebbox`). It can be used to illustrate the region
covered by a data volume, or to increase spatial perception for
horizontal sections.

-  The vertical tick marks of a bounding box are labelled according to
   pressure.
   
   
.. _actors_transferfunction:

1D transfer function (Colour map)
---------------------------------

.. _fig_actorref_transferfunction:
.. figure:: ./figs/actorref_transferfunction.png
    :figwidth: 100 %
    :align: left
    :alt: A figure should appear here
    
    1D transfer function (colour map) actor.

The 1D transfer function actor provides a colour map that is (a)
displayed in a scene (:numref:`fig_actorref_transferfunction`), and
that (b) can be used in other actors to map a data value to a colour.

-  Data scalar range can be adjusted, as well as number of colour steps,
   labels, tick marks and position of the colour map in the
   visualization.

-  The actor provides a number of predefined colour maps, as well as
   Hue-Chroma-Luminance (HCL) colour maps. For the latter, specification
   of HCL colour maps follows the `website of Reto Stauffer`_.
   
.. _website of Reto Stauffer: http://hclwizard.org/creator/


Horizontal sections
-------------------

.. _fig_actorref_hsec:
.. figure:: ./figs/actorref_hsec.png
    :figwidth: 100 %
    :align: left
    :alt: A figure should appear here
    
    Horizontal cross-section actor, together with a transfer function.

The horizontal section actor draws a map of multiple forecast data
fields at an arbitrary pressure altitude (:numref:`fig_actorref_hsec`).

-  The actor supports multiple forecast data fields.

-  Each data field can be rendered as line contours, filled contours or
   pseudo-colour plot (only one filled contour or pseudo-colour plot is
   allowed per horizontal section).

-  The horizontal section can be interactively moved up and down by the
   user.

-  Each horizontal section contains an instance of a graticule actor to
   render graticule lines and coast and border lines.

-  A surface shadow is available to improve spatial perception.

-  Multiple horizontal sections are allowed in a single scene (cf. 
   :numref:`fig_firststeps_hsec_stacked`).

-  Wind barbs are supported.


Vertical sections
-----------------

.. _fig_actorref_vsec:
.. figure:: ./figs/actorref_vsec.png
    :figwidth: 100 %
    :align: left
    :alt: A figure should appear here
    
    Vertical cross-section actor, together with a transfer function.

The vertical section actor draws vertical 2D cross-sections of multiple
forecast data fields along an arbitrary path (:numref:`fig_actorref_vsec`).

-  The actor supports multiple forecast data fields.

-  Each data field can be rendered as line contours or filled contours.

-  A vertical section can be drawn along arbitrarily many waypoints.

-  The waypoints can be interactively moved by the user (to move the
   waypoints, enable a scene view's "interaction mode", see 
   :ref:`first_steps_scenes`).

-  Waypoints can be exported in "flight track file" for the 
   `DLR Mission Support System`_ (MSS), or read from files exported by the MSS.

-  Bottom and top pressure limits of the section can be adjusted.

.. _DLR Mission Support System: http://mss.readthedocs.io


Surface topography
------------------

.. _fig_actorref_surfacetopography:
.. figure:: ./figs/actorref_surfacetopography.png
    :figwidth: 100 %
    :align: left
    :alt: A figure should appear here
    
    Surface topography (terrain) actor, together with a transfer function.

The surface topography actor renders a terrain, using pressure as the
vertical coordinate (:numref:`fig_actorref_surfacetopography`).

-  The actor renders the surface pressure field as terrain.

-  Two data fields are required: Surface pressure (or a similar pressure
   surface) and a variable that maps to colour (via a transfer
   function).

   
Volume actor
------------

.. _fig_actorref_volume:
.. figure:: ./figs/actorref_volume.png
    :figwidth: 100 %
    :align: left
    :alt: A figure should appear here
    
    Volume isosurface actor, together with a transfer function.

The volume actor renders multiple isosurfaces of a data field (
:numref:`fig_actorref_volume`) and provides normal curve functionality
(not shown).

-  The actor renders multiple isosurfaces of a data field via
   raycasting.

-  An isosurface can be coloured according to another data field (e.g.
   an isosurface of wind speed can be coloured by temperature or
   pressure).

-  Shadows of the isosurface and normal curves are rendered to the
   (earth) surface.

-  Normal curves are supported.

-  Interactive region contribution analysis is supported.


Movable poles
-------------

.. _fig_actorref_poles:
.. figure:: ./figs/actorref_poles.png
    :figwidth: 100 %
    :align: left
    :alt: A figure should appear here
    
    Vertical movable poles actor.

The movable poles actor draws an arbitrary number of vertical axes into
the scene (:numref:`fig_actorref_poles`).

-  The actor supports an arbitrary number of vertical axes.

-  The axes can be labelled by pressure.

-  The axes can be interactively moved by the user.


Trajectory actor
----------------
   
.. _fig_actorref_trajectories:
.. figure:: ./figs/actorref_trajectories.png
    :figwidth: 100 %
    :align: left
    :alt: A figure should appear here
    
    Trajectory actor, together with a transfer function.

The trajectory actor draws precomputed Lagrangian particle trajectories
into a scene, either as 3D tubes (
:numref:`fig_actorref_trajectories`) or as particle positions.

.. note:: The trajectory actor currently cannot be created during
   runtime. It needs to be specified as a predefined actor in the
   frontend configuration file (we are working on fixing this issue).

-  Supports precomputed Lagrangian particle trajectories that are
   available in a file format similar to NetCDF-CF. Currently, a
   converter is available for LAGRANTO trajectories (please contact me).

-  Trajectories can be drawn as 3D tubes, coloured by pressure
   elevation.

-  Shadows of the trajectories are rendered to the surface.

-  The individual positions of the trajectories can also be drawn,
   either for the entire trajectories or for individual time steps.

-  Animations of particle positions are possible. Also, trajectory tubes
   can be drawn for the part of the trajectory between trajectory start
   and current time.

   
   
   
   
   
   
   
   