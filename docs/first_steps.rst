First steps with Met.3D
=======================

This section introduces the basic functionality of Met.3D. For the sections
`Starting Met.3D`_, `Adding actors to the scene`_ and 
`Working with scenes and scene views`_, no forecast data is required.

Starting Met.3D
---------------

For the first steps with Met.3D, simply copy the default configuration files 
``default_pipeline.cfg.template`` and ``default_frontend.cfg.template`` that are 
provided with the Met.3D source code (see :doc:`installation`). In the following, We 
assume that you have followed the directory structure suggested in :doc:`installation` and 
that your copies are named ``pipeline.cfg`` and ``frontend.cfg``. Let the files be 
placed in the ``met.3d-base/met.3d/config/`` subdirectory. From 
``met.3d-base/met.3d/``, start Met.3D by entering::

    $ ../build/met3D --pipeline=config/pipeline.cfg --frontend=config/frontend.cfg

You will see some debug and info output on the console.  
:numref:`fig_firststeps_emptywindow` shows the Met.3D window that appears on 
first start-up. It contains an empty visualization area.

.. _fig_firststeps_emptywindow:
.. figure:: ./figs/firststeps_emptywindow.png
    :figwidth: 100 %
    :align: left
    :alt: A figure should appear here
    
    The (empty) Met.3D main window that appears when first starting the 
    software.


Adding actors to the scene
--------------------------

Next, add first visualization components to the empty visualization area. In Met.3D, 
visualization components are called **actors**. Actors represent, for example, a 
graticule, a base map, a 2D horizontal or vertical cross-section, or 3D isosurfaces. 
Currently available actors are listed in :doc:`actors`. Actors are grouped into 
**scenes**, which are simply a collection of actors. A single actor can be assigned to 
one or to multiple scenes. A scene can be visualized by one or by multiple \emph{scene 
views}, which are the visualization areas in the Met.3D window. In 
:numref:`fig_firststeps_emptywindow`, one scene view ("view 1") is 
visible. It displays "Scene 1", which currently does not contain any actors.
To add actors to this scene, follow these steps:

-  Select the menu entry "View/Scene Management" (or press "F4") to open
   the scene management dialog displayed in :numref:`fig_firststeps_scenemanagement`.
   The dialog shows available
   scenes on the right hand side, and available actors on the left (the
   listed "Labels" actor corresponds to a special system actor that is
   always present and that is responsible for text rendering). The
   buttons below the list of actors provide functionality to create new
   default actors, to load actor configurations from file, and to delete
   existing actors.

.. _fig_firststeps_scenemanagement:
.. figure:: ./figs/firststeps_scenemanagement.png
    :figwidth: 100 %
    :align: left
    :alt: A figure should appear here
    
    The scene management dialog. Open with "View/Scene Management" or by 
    pressing "F4".

-  First, create a new (default) graticule actor. Click on the "create"
   button in the "Actor settings" section. In the dialog that appears,
   select "Graticule" and name the actor correspondingly.

-  Next, add the graticule actor to "Scene 1". To do this, select the
   actor in the list of actors, then check the scenes in which it should
   appear in the list of scenes below (:numref:`fig_firststeps_scenemanagement_02`). When you
   click on "Scene 1", the graticule will appear in the Met.3D window in
   the background.

.. _fig_firststeps_scenemanagement_02:
.. figure:: ./figs/firststeps_scenemanagement_02.png
    :figwidth: 100 %
    :align: left
    :alt: A figure should appear here
    
    After the new default graticule actor has been created, it can be assigned 
    to "Scene 1" by selecting the actor and checking the corresponding scene(s).
   
-  Repeat the steps for a volume bounding box actor and a base map
   actor.

-  Close the dialog. Your Met.3D window now looks like :numref:`fig_firststeps_graticule_bbox_map`. The
   base map is black, as no map has been loaded yet. To load a map, the
   actor needs to be configured. On the left hand side of the Met.3D
   window, below the navigation controls for time and ensemble member,
   the "Scene 1" tab lists the actors that are assigned to the scene. It
   provides access to the actors' properties. The properties are
   organised in a tree structure (in the following called "property
   tree") and grouped into different categories, depending on their
   function.

.. _fig_firststeps_graticule_bbox_map:
.. figure:: ./figs/firststeps_graticule_bbox_map.png
    :figwidth: 100 %
    :align: left
    :alt: A figure should appear here
    
    The Met.3D main window after graticule, volume bounding box and base map 
    have been added. No base map file has been loaded yet, hence the map appears
    black.
   
-  Open the property tree for the base map actor. In the "actor
   properties" group, find the "load map file" entry. Click on the
   button to open a file dialog.

-  Choose the raster base map that you have downloaded from `Natural Earth`_
   (see :doc:`installation`) and confirm. After the file has been loaded, your Met.3D
   window looks like :numref:`fig_firststeps_choose_mapfile`.

.. _Natural Earth: http://www.naturalearthdata.com
   
.. _fig_firststeps_choose_mapfile:
.. figure:: ./figs/firststeps_choose_mapfile.png
    :figwidth: 100 %
    :align: left
    :alt: A figure should appear here
    
    The same as Fig. :numref:`fig_firststeps_graticule_bbox_map` with a base
    map file loaded.
   
-  The configured base map actor can be saved to an actor configuration
   file, so that the configuration steps do not have to be repeated each
   time a base map actor is required (this is particularly important if
   more properties than just the path to a map file have to be set for a
   given actor). In the base map actor's "configuration" group, select
   the "save" entry. For this tutorial, create a new subdirectory
   ``met.3d-base/config/`` in your ``met.3d-base/`` directory (see
   :doc:`installation`), and store the configuration to
   ``basemap_default.actor.conf``.

-  To check whether the configuration has been stored successfully, open
   the scene management dialog again. Select the base map actor in the
   list of actors in the "Actor settings" section and click "remove".
   Next, click "create from file" and select the file that you have just
   stored. The actor reappears in the list and can be reassigned to
   "Scene 1".

-  Saved configurations can also be loaded automatically during the
   Met.3D start process. This is useful to let Met.3D recreate your
   favourite configuration each time it is started. Save the actor
   configuration for the volume bounding box and the graticule actors as
   well. We assume that the configurations are saved to
   ``volumeboundingbox_default.actor.conf`` and
   ``graticule_default.actor.conf``, respectively. Next, close Met.3D
   and open the ``frontend.cfg`` file with a text editor. Find the
   ``[Actors]`` section and modify it according to (the ``size=3`` entry
   is important):

   ::

       [Actors]
       size=3

       1\config=$MET3D_BASE/config/graticule_default.actor.conf
       1\scenes=Scene 1

       2\config=$MET3D_BASE/config/basemap_default.actor.conf
       2\scenes=Scene 1

       3\config=$MET3D_BASE/config/volumeboundingbox_default.actor.conf
       3\scenes=Scene 1

   The listed configuration instructs Met.3D to create actors according
   to the specified configuration files on start-up. To check, restart
   Met.3D. The actors are loaded as previously in :numref:`fig_firststeps_choose_mapfile`.

   
.. _first_steps_scenes:
   
Working with scenes and scene views
-----------------------------------

Met.3D provides standard mouse interaction techniques to change the
observer's point of view within a scene view. Hold the left mouse button
and drag to rotate the scene, the right button to pan, and use the
scroll wheel to zoom. 

.. hint::
    The default mouse interaction for navigation can be changed in the 
    ``[SceneNavigation]`` section of ``frontend.cfg``. See the default
    file provided with Met.3D for examples.

The number and layout of displayed scene views can
be changed by selecting one of the presets in the "View" menu (the
preset configurations in the menu are also available through the
keyboard short-cuts Alt+0..6). The default maximum number of scene views
is four. For example, :numref:`fig_firststeps_change_viewlayout` shows the layout
"one large view and three small views" (Alt+5).

.. _fig_firststeps_change_viewlayout:
.. figure:: ./figs/firststeps_change_viewlayout.png
    :figwidth: 100 %
    :align: left
    :alt: A figure should appear here
    
    Met.3D window layout with one large and three small scene views. Views 1, 2 
    and 3 all show "Scene 1", but from different viewpoints.

Similar to actor configurations, viewpoints (*camera positions*) can be saved
to file and restored at a later time. To save a given viewpoint, select the
"System" tab on the left of the Met.3D window. Similar to actor properties,
properties affecting the scene views are arranged in a property tree.
Open the property for the scene view for which you would like to save the
camera, and choose the "modify camera/save" property.

Multiple scene views can show the same scene. In the example in 
:numref:`fig_firststeps_change_viewlayout`, views 1, 2 and 3
all show "Scene 1", but from different viewpoints. To achieve this, open
the scene management dialog and specify the scenes that the views
display in the lower left area of the dialog (cf. 
:numref:`fig_firststeps_scenemanagement`). Also, in view 3
the vertical scaling of the scene is different to views 1 and 2. The
vertical scaling can also be changed in the "System" tab on the left side of
the Met.3D window. As an example, modify the
"rendering/vertical scaling" parameter for view 1 and observe the
difference. With "interaction/sync camera with view", it is possible to
synchronize the camera viewpoints of two scenes. This is useful if two
different scenes are to be examined from the same viewpoint.

Actors can be assigned to multiple scenes by selecting the corresponding
scenes in the scene management dialog 
(:numref:`fig_firststeps_scenemanagement_02`). This way, actors
such as graticule, base map or volume bounding box can be shared among
different scenes (representing "static" content) and combined with
different forecast actors ("dynamic" content) in the individual scenes.

For actors that allow interaction with the user, the scene views provide
an "interaction mode". The interaction mode can be enabled by pressing
"i" while a scene view is selected, or by checking the corresponding
property in the scene view's property tree. While the interaction mode
is enabled for a scene view, the text "Interaction mode" appears at the
bottom of the view and the camera is frozen.

As an example, the "movable poles" actor supports user interaction. It
allows the user to move a pole within the scene by dragging a handle
attached to a pole. The following steps add the actor to the current
configuration:

-  In the scene management dialog, create a new instance of the "movable
   poles" actor and add the instance to "Scene 1". As no specific pole
   has been defined yet, there is nothing to see so far.

-  In the actor's property tree, the "actor properties" group contains
   the entry "add pole". Click on the button to create a new pole. By
   default, it is placed at (lon/lat) = (0/0). Enter "Y=45" to manually
   specify the latitude. The pole is now located in western France.
   :numref:`fig_firststeps_add_pole` shows the
   corresponding Met.3D window (note that the "one large, two small
   views" layout was activated by pressing Alt+4).

.. _fig_firststeps_add_pole:
.. figure:: ./figs/firststeps_add_pole.png
    :figwidth: 100 %
    :align: left
    :alt: A figure should appear here
    
    Met.3D window with a movable pole.
   
-  Click on the large scene view ("view 1") and press "i" to activate
   the interaction mode. Now, small spheres that act as handles appear
   at the top and bottom of the pole. Move the mouse pointer over the
   bottom handle of the pole. The handles are now highlighted in red
   (:numref:`fig_firststeps_add_pole_imode`).

.. _fig_firststeps_add_pole_imode:
.. figure:: ./figs/firststeps_add_pole_imode.png
    :figwidth: 100 %
    :align: left
    :alt: A figure should appear here
    
    Met.3D window with a movable pole, in interaction mode.
   
-  Click on the handle and drag the pole. Note how the pole's position
   is updated in all scene views that display the scene.


.. _first_steps_adding_fc_data:

Adding forecast data to the scene
---------------------------------

Next, we add a horizontal cross-section that displays some forecast
data. The goal is to create a forecast product that shows colour-coded
wind speed, overlain with contour lines of geopotential height and wind
barbs. 

.. note:: In the following, we assume that forecast data from the 
    `European Centre for Medium Range Weather Forecasts`_ (ECMWF) of wind speed,
    u-component and v-component of horizontal wind, and of geopotential
    height is available [1]_.

.. _European Centre for Medium Range Weather Forecasts: http://www.ecmwf.int


Configuration of the data pipeline
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Unlike other visualization tools, Met.3D does not allow the user to
select and load a specific data file at runtime. Instead, **data
pipelines** need to be configured before the tool is started. A pipeline
provides access to a dataset that can be distributed over several files.
All files of a dataset need to be located in a single directory [2]_.
Here, We assume that a pipeline for ECMWF ensemble forecast data in
NetCDF-CF format shall be configured.

-  Open the file ``met.3d-base/met.3d/config/pipeline.cfg`` and find the
   section ``[NWPPipeline]``. Our pipeline is specified by:

   ::

        1\name=ECMWF ENS EUR_LL10
        1\path=/home/local/data/mss/grid/ecmwf/netcdf_Oct15
        1\fileFilter=*ecmwf_ensemble_forecast*EUR_LL10*.nc
        1\schedulerID=MultiThread
        1\memoryManagerID=NWP
        1\fileFormat=CF_NETCDF
        1\enableRegridding=true

   The prefix ``1\`` specifies the number of the pipeline (here, the
   ``size`` parameter in the ``[NWPPipeline]`` needs to be set to at
   least 1). ``name`` specifies the name under which the dataset can be
   accessed in Met.3D, ``path`` specifies the directory in which the
   forecast data files are located. All files in the specified directory
   that match the ``fileFilter`` specified next are considered to belong
   to the dataset.
   ``schedulerID`` can be set to ``MultiThread`` or
   ``SingleThread``. With the first choice, data processing tasks will
   be distributed over the available CPU cores in your system.
   ``memoryManagerID`` needs to correspond to a memory manager instance
   defined in the ``[MemoryManager]`` section of the configuration file
   (see the template for examples, a memory manager needs to be given a
   name and amount of CPU memory it can consume). ``fileFormat`` can be
   set to either ``CF_NETCDF``\  [3]_ or ``ECMWF_GRIB``\  [4]_. 
   If ``enableRegridding`` is set to ``true``,
   Met.3D's regridding module will be activated.

-  For the given example, simply adjust the ``path`` to match that of
   your system.

Adding a horizontal section
~~~~~~~~~~~~~~~~~~~~~~~~~~~

Restart Met.3D. The dataset is now registered within the tool with the
data source identification ``ECMWF ENS EUR_LL10``.

-  In the scene management dialog, create a "horizontal cross-section".
   Add the section to the current scene.

-  To map the wind speed data to colour, a transfer function (i.e. a
   colour map) is required. Create and add a "1D transfer function" and
   close the scene management dialog. The Met.3D window now looks like
   :numref:`fig_firststeps_add_hsec_and_colourbar`
   (where the default settings of colour bar and section may vary
   depending on the exact Met.3D version). The horizontal section by
   default contains an instance of a graticule actor that replicates the
   graticule and coast lines at the elevation of the section.

.. _fig_firststeps_add_hsec_and_colourbar:
.. figure:: ./figs/firststeps_add_hsec_and_colourbar.png
    :figwidth: 100 %
    :align: left
    :alt: A figure should appear here
    
    An empty horizontal section and a colour bar (1D transfer function) have 
    been added to the scene.
   
-  All actors that display forecast data make use of "variables" that
   represent a given forecast parameter (e.g. wind speed) and store the
   actor-related settings that correspond to this parameter. For
   example, in the case of a horizontal section, these settings include
   whether the parameter shall be visualized as line contours or as
   filled contours and, in the latter case, which colour map shall be
   used. To add a variable to the horizontal section, select "add new
   variable" in the actor's "variables" group in the property tree. The
   dialog that opens is shown in 
   :numref:`fig_firststeps_add_variable_to_hsec`.
   It lists all available forecast parameters that are available from
   the registered data sources.

.. _fig_firststeps_add_variable_to_hsec:
.. figure:: ./figs/firststeps_add_variable_to_hsec.png
    :figwidth: 100 %
    :align: left
    :alt: A figure should appear here
    
    Data source selection dialog. The dialog lists all forecast parameters that 
    are available from the registered data pipelines.   
   
-  From the list, select the data source
   ``ECMWF ENS EUR_LL10 ENSFilter`` and the variable
   ``Windspeed_hybrid``. The suffix "ENSFilter" in the data source name
   indicates that in the data pipeline, a module that computes
   statistical quantities from the ensemble (e.g. mean or standard
   deviation) has been attached to the original dataset. Confirm the
   forecast parameter selection. In the follow-up dialog about a
   synchronization control select "Synchronization" to synchronize the
   variable with the global time and ensemble settings.

-  The variable appears in the property tree of the horizontal section
   actor. In the variable's subgroup "rendering", find the properties
   "transfer function" and "render mode". Select the transfer function
   that you have created above and set the render mode to "filled
   contours". The scene now looks like the screenshot in 
   :numref:`fig_firststeps_hsec_choose_colourbar`.

.. _fig_firststeps_hsec_choose_colourbar:
.. figure:: ./figs/firststeps_hsec_choose_colourbar.png
    :figwidth: 100 %
    :align: left
    :alt: A figure should appear here
    
    A variable containing horizontal wind speed has been added to the horizontal 
    section. The colour bar (1D transfer function) has been connected to the 
    variable.
  
-  Next, the colour map needs to be adjusted. Open the property tree of
   the colour map. In the "actor properties/range" subgroup, set
   "decimals" to 0 and the minimum and maximum value to 10 and 80,
   respectively. The colour map type is set to "predefined" (vs. HCL,
   see :ref:`actors_transferfunction`). Open the "predefined" group and select
   "hot\_wind". Also check the "reverse" field. Now, the horizontal
   sections displays the wind speed as shown in 
   :numref:`fig_firststeps_hsec_modify_colourbar`.

.. _fig_firststeps_hsec_modify_colourbar:
.. figure:: ./figs/firststeps_hsec_modify_colourbar.png
    :figwidth: 100 %
    :align: left
    :alt: A figure should appear here
    
    Modification of the colour map to reflect the range of values in the wind 
    speed variable.
   
-  Go back to the horizontal section actor's property tree and add a
   second variable. This time, choose ``Geopotential_height_hybrid``.
   For this variable, we set "render mode" to "line contours". Potential
   contour values can be specified in the text fields "thin contour
   levels" and "thick contour levels". The strings can either be a list
   of values (e.g. ``5000,5500,6000``) or three values of format
   ``[from,to,step]``. An example of the latter is ``[0,26000,40]``,
   which will display a contour line every 40 m between 0 and 26000 m.
   Note that the range of values is chosen to reflect all possible
   values that might be encountered at any vertical location of the
   section. Met.3D will recognize which of the values are applicable to
   a given elevation and only render the actually visible lines. Put
   ``[0,26000,40]`` into the "thin contour levels" field and
   ``[0,26000,200]`` into the "thick contour levels" field. The result
   is shown in :numref:`fig_firststeps_hsec_add_geop_contours`.

.. _fig_firststeps_hsec_add_geop_contours:
.. figure:: ./figs/firststeps_hsec_add_geop_contours.png
    :figwidth: 100 %
    :align: left
    :alt: A figure should appear here
    
    Contour lines of geopotential height have been added to the section.
   
-  To complete the forecast product, add two more variables to the
   actor: ``u-component_of_wind_hybrid`` and
   ``v-component_of_wind_hybrid``. Leave the "render mode" for both
   variables set to "disabled". Instead, open the "actor properties/wind
   barbs" property group of the horizontal section actor. At the bottom
   of the group, assign the u and v-components to the corresponding
   fields and click on "enabled". For the screenshot in 
   :numref:`fig_firststeps_hsec_add_windbarbs`, I
   have also changed the colour of the barbs to blue.
   
.. _fig_firststeps_hsec_add_windbarbs:
.. figure:: ./figs/firststeps_hsec_add_windbarbs.png
    :figwidth: 100 %
    :align: left
    :alt: A figure should appear here
    
    Wind barbs have been added to the section.
   
The forecast product has been completed. Modify the "actor
properties/slice position" property to move the section up and down, and
use the time and ensemble navigation buttons in the top left of the
Met.3D window to change time and/or ensemble member. Of course, the
horizontal section configuration can be saved to a configuration file.
Similar to the actors in `Adding actors to the scene`_, a
saved horizontal section actor configuration can be loaded at runtime in
the scene management dialog. Alternatively, it can be listed in the
``frontend.cfg`` file to be loaded during start-up.

You can also have multiple instances of an actor in a scene. 
:numref:`fig_firststeps_hsec_stacked` shows an example of two
horizontal sections stacked on top of each other (the lower one at 925
hPa and the upper one at 200 hPa). The pole is placed in the centre of
the low pressure system at 925 hPa, its intersection with the upper
section showing the relation of the position of low-level centre to the
jet stream.

.. _fig_firststeps_hsec_stacked:
.. figure:: ./figs/firststeps_hsec_stacked.png
    :figwidth: 100 %
    :align: left
    :alt: A figure should appear here
    
    Two identical horizontal sections stacked on top of each other. The vertical 
    pole illustrates the relation between low pressure centre at 925 hPa and 
    the jet stream at 200 hPa.


Footnotes
    
.. [1]
   Unfortunately, We cannot provide a sample dataset at the present time.
   If you have access to ECMWF data, please contact me so we can let you
   know how to obtain a suitable dataset.

.. [2]
   This is a typical application case when forecast data distributed
   over multiple files are automatically downloaded by shell scripts
   from the forecast provider (e.g. ECMWF) as soon as they are
   available.

.. [3]
   CF-compliant NetCDF files, e.g. as generated from ECMWF GRIB files with
   Unidata's "netcdf-java" library, and files used by the DLR Mission Support
   System.
   We are interested in feedback about success or problems with other model data.

.. [4]
   Only GRIB messages that are interpolated to a regular
   longitude-latitude grid in the horizontal will be considered. For
   model level and pressure level fields, all messages that correspond
   to a single 3D data field need to be put in the same file. Note that
   a GRIB pipeline needs to index all GRIB files located in the
   specified directory when they are first read. This may take several
   minutes. After the index has been created, do not alter the GRIB
   files.

   