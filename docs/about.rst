About Met.3D
============

Met.3D is being developed as a research effort to improve the visualization of
meteorological data in research and forecasting.

Detailed information about the software and its uses can be found in the 
following publications:

-   Rautenhaus, M., Kern, M., Schäfler, A., and Westermann, R.:
    **Three-dimensional visualization of ensemble weather forecasts -- Part 1:
    The visualization tool Met.3D (version 1.0)**, 
    `Geosci. Model Dev., 8, 2329-2353`_,
    doi:10.5194/gmd-8-2329-2015, 2015.

    
-   Rautenhaus, M., Grams, C. M., Schäfler, A., and Westermann, R.:
    **Three-dimensional visualization of ensemble weather forecasts --Part 2:
    Forecasting warm conveyor belt situations for aircraft-based field campaigns**,
    `Geosci. Model Dev., 8, 2355-2377`_,
    doi:10.5194/gmd-8-2355-2015, 2015.

-   Rautenhaus, M., Grams, C. M., Schäfler, A., and Westermann, R.:
    **GPU based interactive 3D visualization of ECMWF ensemble forecasts**,
    `ECMWF Newsletter, vol. 138 (Winter 2014)`_, pp. 34-38, 2014.

.. _Geosci. Model Dev., 8, 2329-2353: http://www.geosci-model-dev.net/8/2329/2015/gmd-8-2329-2015.html
.. _Geosci. Model Dev., 8, 2355-2377: http://www.geosci-model-dev.net/8/2355/2015/gmd-8-2355-2015.html
.. _ECMWF Newsletter, vol. 138 (Winter 2014): http://www.ecmwf.int/en/elibrary/14581-newsletter-no138-winter-2013/14
   

.. note::   
    Currently, Met.3D research and development is continued, among others,
    in the context of the German 
    `Transregional Collaborative Research Center "Waves to Weather"`_ 
    (see the Waves to Weather `cross-cutting activity "Visualisation"`_).
    During fall 2016, Met.3D will be used for weather forecasting during the
    `NAWDEX field campaign`_.

.. _Transregional Collaborative Research Center "Waves to Weather": http://www.wavestoweather.de
.. _cross-cutting activity "Visualisation": http://www.wavestoweather.de/cca/visualization/index.html
.. _NAWDEX field campaign: http://www.pa.op.dlr.de/nawdex/index.html