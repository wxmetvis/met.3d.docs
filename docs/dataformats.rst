Supported data and file formats
===============================

This section provides an overview of the data and file formats supported
by Met.3D. 

- Met.3D supports gridded, structured data with a **regular, georeferenced 
  longitude-latitude grid** in the horizontal, and either **levels of constant
  pressure** (in the following "pressure levels") or **hybrid sigma-pressure
  levels** (as used, e.g., by the ECMWF integrated forecast system) in the
  vertical.
  
- Multiple datasets (that can have different grids) and ensemble datasets are
  natively supported.

- Also, trajectory data is supported.
  
- Currently, gridded data can be read from NetCDF files following the
  `Climate and Forecast (CF) Metadata Conventions`_ and from ECMWF GRIB files;
  trajectory data can be read from NetCDF files with a custom formatting.

.. note:: If your data cannot be read by Met.3D (i.e., if you don't have access
    to your datasets after start-up), a (admittedly currently limited) set of
    error messages is displayed by the software in the start-up output on the
    text console. Please contact us if you need help.
    
.. note:: Display of non-georeferenced data on regular grids (e.g., output
    from **LES models**) is possible but requires some hacking on the NetCDF
    side. Please contact us if you are interested. We are working on making
    data import for such data easily possible in a future Met.3D version.

    
Internal data formats in Met.3D
-------------------------------

Gridded, structured data
~~~~~~~~~~~~~~~~~~~~~~~~

Met.3D supports gridded, structured data with a **regular longitude-latitude
grid** in the horizontal, and either **levels of constant pressure** (in the 
following "pressure levels") or **hybrid sigma-pressure levels** (as used, e.g.,
by the ECMWF integrated forecast system) in the vertical.

Internally, a single 2D or 3D scalar data field (i.e., one forecast variable,
one time step, one ensemble member) is treated as the smallest data entity.
A single data field is stored in a class derived from :code:`MStructuredGrid`,
these objects store all contextual information required to visualise the
field (including latitude and longitude coordinates, pressure or hybrid level,
time step and additional metadata). Example of derived classes are
:code:`MRegularLonLatGrid` for 2D fields,
:code:`MRegularLonLatStructuredPressureGrid` for 3D fields on pressure levels,
:code:`MLonLatHybridSigmaPressureGrid` for 3D fields on hybrid sigma-pressure
levels.
Time series and ensemble sets are composed of these basic data field entities.

Trajectory data
~~~~~~~~~~~~~~~

Trajectories are stored as bundles of trajectories (class :code:`MTrajectories`).
A single trajectory in a bundle consists of a simple list of positions in
longitude-latitude-pressure space that resemble the trajectory. All trajectories
in a bundle share the same time information, i.e. the time for position *n* is
the same for all trajectories. This data layout corresponds to the smallest
data entity being the bundle of all trajectories computed for a single ensemble
member for the same time steps.


Gridded data in NetCDF format
-----------------------------

Gridded, structured data can be read from NetCDF files that follow the
`Climate and Forecast (CF) Metadata Conventions`_.
Define a dataset consisting of CF-compliant NetCDF files by adding the following
entry to your ``pipeline.cfg``:

   ::

        1\name=YOUR DATASET NAME
        1\path=/your/path/data/netcdf
        1\fileFilter=*ecmwf_ensemble_forecast*EUR_LL10*.nc
        1\schedulerID=MultiThread
        1\memoryManagerID=NWP
        1\fileFormat=CF_NETCDF
        1\enableRegridding=true

All files in the specified path that match the given file filter will be used
for the dataset. See the file ``default_pipeline.cfg.template`` for further
comments on the meaning of the individual keywords.

.. note:: Forecast variables, time steps and ensemble members can be arbitrarily
    distributed over the files that match the file filter. You can store
    all ensemble members in one file but have different files for each time
    step, or vice versa. Or everything can be stored in a single file.


The following example contains a NetCDF header of a file containing an ensemble
forecast on pressure levels:

.. code-block:: none
    :linenos:

    netcdf somegriddeddata.pl {
    dimensions:
        lon = 101 ;
        lat = 41 ;
        isobaric = 12 ;
        time = 1 ;
        ens0 = 51 ;
    variables:
        float lat(lat) ;
                lat:units = "degrees_north" ;
        float lon(lon) ;
                lon:units = "degrees_east" ;
        float isobaric(isobaric) ;
                isobaric:units = "hPa" ;
                isobaric:long_name = "Isobaric surface" ;
                isobaric:positive = "down" ;
        int time(time) ;
                time:units = "Hour since 2012-10-15T00:00:00.000Z" ;
                time:standard_name = "time" ;
        int ens0(ens0) ;
                ens0:standard_name = "ensemble_member_id" ;
                
        float Geopotential_isobaric(time, ens0, isobaric, lat, lon) ;
                Geopotential_isobaric:long_name = "Geopotential @ Isobaric surface" ;
                Geopotential_isobaric:units = "m2.s-2" ;
        float Temperature_isobaric(time, ens0, isobaric, lat, lon) ;
                Temperature_isobaric:long_name = "Temperature @ Isobaric surface" ;
                Temperature_isobaric:units = "K" ;
                
        ...
    }
        
- The **latitude and longitude dimensions** are recognised according to their
  ``units`` keyword; cf. the `longitude/latitude section in CF-conventions`_.

.. _longitude/latitude section in CF-conventions: http://cfconventions.org/cf-conventions/v1.6.0/cf-conventions.html#latitude-coordinate
        
- The **vertical pressure level dimension** requires ``units`` of pressure, as well
  the ``positive`` attribute being defined; cf. the `vertical coordinate section in CF-conventions`_.

.. _vertical coordinate section in CF-conventions: http://cfconventions.org/cf-conventions/v1.6.0/cf-conventions.html#vertical-coordinate
        
- Time **time dimension** is identified by its ``units`` attribute; cf. the
  `time coordinate section in CF-conventions`_.

.. _time coordinate section in CF-conventions: http://cfconventions.org/cf-conventions/v1.6.0/cf-conventions.html#time-coordinate

.. note:: The time encoded in the ``units`` attribute of the time dimension is
    used as the forecast base/initialisation time.

.. note:: The **ensemble dimension** is currently not specified in the CF conventions.
    Met.3D simply searches for a variable with a ``standard_name`` attribute
    set to ``ensemble_member_id``. For now, the ``_CoordinateAxisType`` used
    by the netcdf-java library (``_CoordinateAxisType = "Ensemble"``) is also
    acceptable. (See the Met.3D source code method ``NcCFVar::getEnsembleVar()`` 
    in ``nccfvar.cpp``).    
    
        
The following example contains a NetCDF header of a file containing an ensemble
forecast on hybrid sigma-pressure levels; the required keyword of the vertical
variable are different; cf. `Appendix D of the CF-conventions`_.

.. _Appendix D of the CF-conventions: http://cfconventions.org/cf-conventions/v1.6.0/cf-conventions.html#dimensionless-v-coord

.. code-block:: none
    :linenos:

    netcdf somegriddeddata.ml {
    dimensions:
        lon = 101 ;
        lat = 41 ;
        hybrid = 62 ;
        time = 1 ;
        ens0 = 51 ;
    variables:
        float lat(lat) ;
                lat:units = "degrees_north" ;
        float lon(lon) ;
                lon:units = "degrees_east" ;
        float hybrid(hybrid) ;
                hybrid:units = "sigma" ;
                hybrid:long_name = "Hybrid level" ;
                hybrid:positive = "down" ;
                hybrid:standard_name = "atmosphere_hybrid_sigma_pressure_coordinate" ;
                hybrid:formula = "p(time,level,lat,lon) = ap(level) + b(level)*ps(time,lat,lon)" ;
                hybrid:formula_terms = "ap: hyam b: hybm ps: Surface_pressure_surface" ;
        int time(time) ;
                time:units = "Hour since 2012-10-15T12:00:00.000Z" ;
                time:standard_name = "time" ;
        int ens0(ens0) ;
                ens0:_CoordinateAxisType = "Ensemble" ;

        double hyam(hybrid) ;
                hyam:long_name = "hybrid A coefficient at layer midpoints" ;
                hyam:units = "Pa" ;
        double hybm(hybrid) ;
                hybm:long_name = "hybrid B coefficient at layer midpoints" ;
                hybm:units = "1" ;

        float Temperature_hybrid(time, ens0, hybrid, lat, lon) ;
                Temperature_hybrid:long_name = "Temperature @ Hybrid level" ;
                Temperature_hybrid:units = "K" ;
        float Specific_humidity_hybrid(time, ens0, hybrid, lat, lon) ;
                Specific_humidity_hybrid:long_name = "Specific humidity @ Hybrid level" ;
                Specific_humidity_hybrid:units = "kg/kg" ;
   
        ...
    }
    

.. _Climate and Forecast (CF) Metadata Conventions: http://cfconventions.org/cf-conventions/v1.6.0/cf-conventions.html


Gridded data in GRIB format
---------------------------

Met.3D provides support for GRIB files written by ECMWF's `grib_api`_ and `ecCodes`_
libraries (as output, e.g. by the ECMWF MARS archive or written by Metview).

.. _grib_api: https://software.ecmwf.int/wiki/display/GRIB/Home
.. _ecCodes: https://software.ecmwf.int/wiki/display/ECC/ecCodes+Home

In your ``pipeline.cfg`` file, simply change the ``fileFormat`` entry to
``ECMWF_GRIB``:

   ::

        1\name=YOUR DATASET NAME
        1\path=/your/path/data/grib
        1\fileFilter=*ecmwf_ensemble_forecast*EUR_LL10*.grb
        1\schedulerID=MultiThread
        1\memoryManagerID=NWP
        1\fileFormat=ECMWF_GRIB
        1\enableRegridding=true

- GRIB messages may be arbitrarily distributed over different files that match
  the specified ``fileFilter``.

- As for NetCDF files, support is only provided for a horizontally regular
  longitude/latitude grid (GRIB *gridType* = ``regular_ll``) and either pressure
  (GRIB *typeOfLevel* = ``isobaricInhPa``) or hybrid sigma-pressure levels
  (GRIB *typeOfLevel* = ``hybrid``) in the vertical.
  
- Hybrid sigma-pressure model levels additionally require the surface pressure
  field to be present in the dataset (GRIB *shortName* = ``sp``).
  
- Analysis (GRIB *dataType* = ``an``), forecast (GRIB *dataType* = ``fc``) and 
  perturbed/control (i.e., ensemble forecast; GRIB *dataType* = ``pf/cf``) 
  messages are interpreted.
  
- For 3D fields, a consistent consecutive list of vertical levels must be
  present **for all time steps and ensemble members** of a dataset.
  Levels need not be complete (i.e., there can be missing levels at the top
  or bottom) but the same levels need to be provided for all data fields in
  a dataset.
  
- For a given dataset, all GRIB messages must have the same horizontal extent.

   
Trajectory data in NetCDF format
--------------------------------

Since the CF-conventions only provide `limited support for trajectory data`_
(in particular no ensemble dimension), we're currently using a custom NetCDF
layout that follows the CF-versions. There are a few limitations:

- There is only one time dimension per NetCDF file, which corresponds to
  the time of the particle positions along the trajectories. If you have
  a series of trajectory bundles started at different times (i.e., a bundle
  of trajectories for each forecast time step as used for `WCB detection`_),
  you need one NetCDF file per trajectory bundle.

The following example contains a NetCDF header of a file containing an ensemble
forecast on pressure levels:

.. _limited support for trajectory data: http://cfconventions.org/cf-conventions/v1.6.0/cf-conventions.html#_trajectory_data

.. code-block:: none
    :linenos:

    netcdf sometrajectorydata {
    dimensions:
        time = 17 ;
        trajectory = 215332 ;
        ensemble = 51 ;
        start_lon = 101 ;
        start_lat = 41 ;
        start_isobaric = 52 ;
        time_interval = 8 ;
    variables:
        double time(time) ;
                time:standard_name = "time" ;
                time:long_name = "time" ;
                time:units = "hours since 2012-10-19 06:00:00" ;
                time:trajectory_starttime = "2012-10-19 06:00:00" ;
                time:forecast_inittime = "2012-10-17 00:00:00" ;
        float lon(ensemble, trajectory, time) ;
                lon:standard_name = "longitude" ;
                lon:long_name = "longitude" ;
                lon:units = "degrees_east" ;
        float lat(ensemble, trajectory, time) ;
                lat:standard_name = "latitude" ;
                lat:long_name = "latitude" ;
                lat:units = "degrees_north" ;
        float pressure(ensemble, trajectory, time) ;
                pressure:standard_name = "air_pressure" ;
                pressure:long_name = "pressure" ;
                pressure:units = "hPa" ;
                pressure:positive = "down" ;
                pressure:axis = "Z" ;
        float start_lon(start_lon) ;
                start_lon:long_name = "longitude of start grid" ;
                start_lon:units = "degrees_east" ;
        float start_lat(start_lat) ;
                start_lat:long_name = "latitude of start grid" ;
                start_lat:units = "degrees_north" ;
        float start_isobaric(start_isobaric) ;
                start_isobaric:long_name = "Isobaric surface of start grid" ;
                start_isobaric:units = "hPa" ;
                start_isobaric:positive = "down" ;
                start_isobaric:axistype = "pressure levels" ;
        float time_interval(time_interval) ;
                time_interval:long_name = "time interval" ;
                time_interval:units = "hours" ;
        float delta_pressure_per_time_interval(ensemble, trajectory, time_interval) ;
                delta_pressure_per_time_interval:long_name = "max. delta pressure of trajectory in time interval around start time" ;
                delta_pressure_per_time_interval:units = "hPa" ;
    }
   
- In addition to the air parcel time, Met.3D requires the two time attributes
  ``trajectory_starttime`` and ``forecast_inittime`` that define the time at
  which the trajectory was started, as well as the forecast initialisation/base
  time of the forecast data on which the trajectory was computed.
  
- The ``start_lon``, ``start_lat`` and ``start_isobaric`` variables are optional
  and define the grid from which the trajectory bundle was started (used for
  `WCB detection`_).
  
- Similarly, the ``time_interval`` and ``delta_pressure_per_time_interval``
  variables are optional and define pre-computed values for `WCB detection`_.
  Contact us if you're interested.
  
.. _WCB detection: http://www.geosci-model-dev.net/8/2355/2015/