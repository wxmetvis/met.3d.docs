Met.3D - a research software for interactive 3-D visualization of gridded atmospheric data
==========================================================================================

.. image:: https://anaconda.org/met3d/met3d/badges/version.svg
   :target: https://anaconda.org/met3d/met3d

.. image:: https://anaconda.org/met3d/met3d/badges/latest_release_date.svg
   :target: https://anaconda.org/met3d/met3d

Welcome to the online documentation of Met.3D, the **open-source visualization research software** for
**interactive, three-dimensional visual analysis** of **gridded atmospheric data**.

If you are new to Met.3D, we recommend that you read :doc:`/user_docs/introduction` page to get an overview of
the software and of this documentation.
The sidebar on the left should let you easily access the documentation for your topic of interest.
You can also use the search function in the top-left corner.


What is Met.3D?
---------------

The following video demonstrates typical use of Met.3D.
Open numerical weather prediction data (ICON-EU model) is downloaded from the
German Weather Service (DWD) open data server.
Example configurations for Met.3D visual analysis modules
(called "actors") are loaded to showcase some of the visualization
functionality that Met.3D offers.

.. raw:: html

    <div style="position: relative; width: 96%; height: 0; padding-bottom: 60%;">
    <iframe src="https://lecture2go.uni-hamburg.de/o/iframe/?obj=69875" title="Video: Met.3D v1.13: Illustration of using visualization templates to analyze open weather forecast data from the German Weather Service (DWD)" frameborder="0" allowfullscreen="" style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe>
    </div>


Get involved
------------

Met.3D is an open-source project whose development is lead by the `Visual Data Analysis Group`_
at the `Hub of Computing and Data Science`_ of the `University of Hamburg`_, Germany.
We are always looking for new users and contributors, and we
can always use your feedback to improve the software and its documentation.
If you don't understand something, or cannot find what you are looking for in the docs,
help us by letting us know!

Sign up for the `Met.3D newsletter`_, submit an issue or merge request
on the Gitlab repository, or help extend this online documentation.

.. note:: Met.3D is open-source, and `available on Gitlab`_. The software
    is licensed under the `GNU General Public License, Version 3`_.

.. note:: The original Met.3D reference publication has been published in 
    `Geoscientific Model Development`_ and is `available online`_:

    Rautenhaus, M., Kern, M., Schäfler, A., and Westermann, R.:
    "Three-dimensional visualization of ensemble weather forecasts -- Part 1:
    The visualization tool Met.3D (version 1.0)", Geosci. Model Dev., 8,
    2329-2353, doi:10.5194/gmd-8-2329-2015, 2015.

.. _available on Gitlab: https://gitlab.com/wxmetvis/met.3d
.. _GNU General Public License, Version 3: https://www.gnu.org/licenses/gpl-3.0.en.html
.. _Geoscientific Model Development: http://geosci-model-dev.net/
.. _available online: http://www.geosci-model-dev.net/8/2329/2015/gmd-8-2329-2015.html
.. _Met.3D main webpage: https://met3d.wavestoweather.de
.. _Visual Data Analysis Group: https://www.hcds.uni-hamburg.de/en/research/fg-vda.html
.. _Hub of Computing and Data Science: https://www.hcds.uni-hamburg.de/en.html
.. _University of Hamburg: https://www.uni-hamburg.de/en.html
.. _Met.3D newsletter: https://lists.uni-hamburg.de/mailman/listinfo/met3d-news.hcds


.. toctree::
    :maxdepth: 2
    :caption: User Documentation
    :hidden:

    /user_docs/introduction
    /user_docs/about
    /user_docs/installation
    /user_docs/getting_started
    /user_docs/actors
    /user_docs/data_handling
    /user_docs/examples

.. toctree::
    :maxdepth: 2
    :caption: Developer Documentation
    :hidden:
    
    /dev_docs/architecture
    /dev_docs/contribution
    /dev_docs/data_analysis_framework
    /dev_docs/property_framework
    /dev_docs/architecture/trajectory_pipeline
    /dev_docs/radar
