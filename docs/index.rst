Welcome to Met.3D
=================

.. sidebar:: News

   - Met.3D version 1.1 has been released! See :doc:`release_notes` for more
     information.
   
Met.3D is an **open-source visualisation tool** for 
**interactive, three-dimensional visualisation** of 
**numerical ensemble weather predictions** and similar numerical
**atmospheric model datasets**. 
The tool is implemented in `C++`_ and `OpenGL 4`_ and
runs on standard commodity hardware. Its only "special" requirement is an 
OpenGL 4.3 capable graphics card. 

.. note:: Met.3D is open-source, and `available on Gitlab`_. The software
    is licensed under the `GNU General Public License, Version 3`_.

Met.3D currently runs under Linux.
It has originally been designed for weather forecasting during atmospheric 
research field campaigns, however, is
not restricted to this application. Besides being used as a visualisation tool,
Met.3D is intended to serve as a framework to implement and evaluate new 3D and
ensemble visualisation techniques for the atmospheric sciences.

.. note:: A Met.3D reference publication has been published in 
    `Geoscientific Model Development`_ and is `available online`_:

    Rautenhaus, M., Kern, M., Schäfler, A., and Westermann, R.:
    "Three-dimensional visualization of ensemble weather forecasts -- Part 1:
    The visualization tool Met.3D (version 1.0)", Geosci. Model Dev., 8,
    2329-2353, doi:10.5194/gmd-8-2329-2015, 2015.

Met.3D is developed at the `Computer Graphics & Visualization Group`_, Technische
Universität München, Garching, Germany. We hope you find the tool useful for
your work, too. Please let us know about your experiences.

.. _C++: https://isocpp.org/
.. _OpenGL 4: https://www.opengl.org/
.. _available on Gitlab: https://gitlab.com/wxmetvis/met.3d
.. _GNU General Public License, Version 3: https://www.gnu.org/licenses/gpl-3.0.en.html
.. _Geoscientific Model Development: http://geosci-model-dev.net/
.. _available online: http://www.geosci-model-dev.net/8/2329/2015/gmd-8-2329-2015.html
.. _Computer Graphics & Visualization Group: http://wwwcg.in.tum.de/

The documentation for Met.3D is organised into the following sections:

* :ref:`user-docs`
* :ref:`feature-docs`
* :ref:`about-docs`

Information about development is also available:

* :ref:`dev-docs`

.. attention:: The documentation you are reading is **work in progress**. We are
    adding bits and pieces whenever we find time. If you don't find the information
    you are looking for, please contact us. If you like to contribute to the
    documentation, please let us know as well!

.. _user-docs:

.. toctree::
   :maxdepth: 2
   :caption: User Documentation

   release_notes
   getting_started
   installation_qmake
   installation_cmake
   first_steps
   
.. _feature-docs:

.. toctree::
   :maxdepth: 2
   :glob:
   :caption: Feature Documentation
   
   actors
   dataformats

.. _about-docs:

.. toctree::
   :maxdepth: 2
   :caption: About Met.3D
   
   about
   contributors
   
.. _dev-docs:

.. toctree::
   :maxdepth: 2
   :caption: Developer Documentation
   
   developer
   contribution
   architecture
   
