Contributors
============

The open-source version of Met.3D is developed by

* Marc Rautenhaus
* Bianca Tost
* Michael Kern
* Alexander Kumpf
* Christoph Heidelmann
* Fabian Schöttl