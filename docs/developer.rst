Met.3D developer documentation
==============================

Large parts of the Met.3D source code have already been documented with the `Doxygen`_
code documentation system. A `Doxygen`_ configuration file is available in the
`Met.3D repository`_ (in the ``doc/doxygen`` subdirectory). Please run `Doxygen`_
locally to build the corresponding documentation.

.. _Doxygen: http://www.stack.nl/~dimitri/doxygen/
.. _Met.3D repository: https://bitbucket.org/wxmetvis/met.3d


Contribution
------------

If you are contributing to the Met.3D code base, please carefully read the
:doc:`contribution`. They contain information on the used GIT workflow and
coding conventions. As the project is continuously growing, please adhere to
the listed conventions.


Architecture documentation
------------------------------------

Conceptual descriptions of the architecture of selected features in Met.3D are 
provided in :doc:`architecture`. This section will be expanded in the 
future.


