Met.3D contribution guidelines
==============================

This section describes basic guidelines for contributing to the Met.3D project.

:doc:`./contribution/git_workflow` describes the basic workflow for fixing bugs or adding
new features to the code.

:doc:`./contribution/cpp_coding_style` and :doc:`./contribution/glsl_coding_style` 
describe the project's code conventions.

:doc:`./contribution/documentation` outlines how you can contribute to this online documentation.

:doc:`./contribution/toolbar` introduces how to create and add an icon to the Met.3D toolbar.

.. toctree:: 
    :maxdepth: 1
    :hidden:

    ./contribution/git_workflow
    ./contribution/cpp_coding_style
    ./contribution/glsl_coding_style
    ./contribution/documentation
    ./contribution/toolbar
    