\documentclass[11pt]{report}

% Language setting
\usepackage[english]{babel}

% Set page size and margins
\usepackage[a4paper, twoside, top=2.5cm,bottom=2.5cm,inner=3cm,outer=2.5cm]{geometry}

% font
\usepackage[T1]{fontenc}
% \usepackage{helvet}
\renewcommand{\familydefault}{\sfdefault}

\usepackage[
backend=biber,
style=authoryear,
sorting=ynt
]{biblatex}
\usepackage{csquotes}
\addbibresource{MeineBachelorarbeit.bib}
\DeclareDelimFormat{nameyeardelim}{\addcomma\space}

% Useful packages
\usepackage{amsmath}
\usepackage{graphicx}
\usepackage[colorlinks=true, allcolors=blue]{hyperref}
\usepackage[section]{placeins} % puts a FloatBarrier at the beginning of each section
\usepackage{parskip} % Add space between paragraphs
%%% to be able to use subfigures: %%%
\usepackage[font=footnotesize]{caption}
\usepackage{subcaption}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\usepackage{textcomp} % needed for gensymb
\usepackage{gensymb} % e.g. \degree
\usepackage{wrapfig} % wrap text around figure
\usepackage{physics} %derivatives
\usepackage{multirow} %multirow tables
\usepackage{listings} %include code
\usepackage{minted} %include code
\setminted{fontsize=\footnotesize}
\newenvironment{code}{\captionsetup{type=listing}}{} %code environment with page break
\usepackage{svg} %to display vector graphics
\usepackage{pdfpages} % include Eidesstattliche Erklärung
\usepackage[htt]{hyphenat} % break ttt

\usepackage{colortbl}	
\usepackage{xcolor}
\usepackage{soul}
\renewcommand{\familydefault}{\sfdefault}
\definecolor{uhhred}{cmyk}{0,100,100,0}


\begin{document}

\subsection*{2.6.1 Determining the position of the target}
\label{sec:position_calc}
In order to visualize the echoes measured by the radar in three dimensions, it is necessary to know their position in space. Met.3D represents positions on the Earth's surface in geographic coordinates (latitude and longitude) and the height above the Earth's surface expressed as pressure in hPa. However, the positions of the radar data points are given in a spherical coordinate system consisting out of elevation angles, azimuth angles and the range along the beam. These radar coordinates must therefore be transformed into the coordinate system of Met.3D.

\subsubsection{Calculating the height of the target}
\label{sec:height}

There are several things to bear in mind when calculating the height of a radar data point above the surface. Firstly, the Earth's surface is curved due to its spherical shape. Therefore, even if the radar beam were straight, along the beam the perpendicular distance between the Earth's surface and the positions on the beam would increase according to the inverse curvature of the Earth. An observer on the Earth's surface would therefore perceive the beam as bending away from the Earth. 

However, the radar beam does not actually traverse the Earth's atmosphere in a straight line. This is due to the refractive index of the atmosphere for electromagnetic waves, which tends to decrease in the troposphere with increasing altitude.
As a result, radar waves travel faster at higher altitudes than closer to the surface and tend to be diffracted downwards towards the Earth's surface. However, it should be noted that the refractive index is dependent on temperature, pressure and vapour pressure. Under certain weather conditions, this can lead to increased refraction (sub-refraction) or decreased refraction (super-refraction). 

The curvature $C$ of the beam can be specified as the change in refractive index $n$ with height $H$  
\begin{equation}
C= \pdv{n}{H}. 
\end{equation}

Under temperature and pressure conditions where standard refraction applies, the beam curvature is $C = -39 \cdot 10^{-6}\;km^{-1}$.
The curvature can also be expressed as the inverse radius of a circle on which the refracted beam travels. For the radar beam, this refraction radius $R_R$ is about four times the radius of the Earth $R_E$.

\begin{equation}
    R_{R} = \frac{1}{C} = 25641\text{ km} \approx 4.02 \cdot R_E
\end{equation}

\begin{figure}[h]
\centering
     \begin{subfigure}[t]{0.48\textwidth}
         \centering
         \includegraphics[width=\textwidth]{RadarTrigonometry_elevation0_with_beam_new.webp}
         \caption{elevation angle $\alpha = 0$.}
         \label{fig:RadarTrig_a}
     \end{subfigure}
     \hfill
     \begin{subfigure}[t]{0.48\textwidth}
         \centering
         \includegraphics[width=\textwidth]{RadarTrigonometry_elevation1_with_beam_new.webp}
         \caption{elevation angle $\alpha > 0$}
        \label{fig:RadarTrig_b}
     \end{subfigure}
\caption{The radar beam follows the curved purple line. At the exemplary marked measuring point P it has travelled the range $s$ from the radar station on the refraction circle with radius $R_R$, which is approximately 4 times the Earth's radius $R_E$. The perpendicular height of the point over the Earth's surface is labelled as $z_p$. }
\label{fig:RadarTrig}
\end{figure}

Figure \ref{fig:RadarTrig_a} shows the relations between the curvature of the Earth and the curvature of the radar beam in the case when the elevation angle is zero, i.e. when the radar pulse is emitted horizontally to the Earth's surface. The figure is not to scale for reasons of better visibility. Figure \ref{fig:RadarTrig_b} shows the same relations under the more realistic condition that the radar pulse is emitted under an elevation angle $\alpha > 0$. To visualize the radar data, we need to determine the coordinates of any measurement point on the radar beam, for example the selected point P. Known for the point are the elevation angle $\alpha$, the azimuth angle $\delta$, and the range distance $s$, which the radar beam has travelled between the radar location and the data point. Figure \ref{fig:RadarTrig_height} shows the angles and distances used in the following equations.

\begin{figure}[h]
\centering
\includegraphics[width=0.6\textwidth]{RadarTrigo_angles_new.webp}
\caption{The height $z_p$ of the point P over the Earth's surface can be calculated using the Earth's radius $R_E$, the refraction radius $R_R$, the elevation angle $\alpha$, the range s and the angles $\beta$ and $\kappa$ following from it, as well as a coordinate transformation between the polar coordinate systems of the refraction circle (x',y') and the Earth (x,y).}
\label{fig:RadarTrig_height}
\end{figure}

From $s$ and the refraction radius $R_R$, $ \beta = s/R_{R}$ can be calculated.


\FloatBarrier
We define a Cartesian coordinate system that has its origin in the centre of the refraction circle, with the axes x' and y'.
In this system, the coordinates of point P ($x'_P$, $y'_P$) depend on the angle $\kappa$:
\begin{align}
    \kappa &= 90\degree + \alpha - \beta \\
    x'_P &= R_{R} \cdot \cos(\kappa) \\
    y'_P &= R_{R} \cdot \sin(\kappa)
\end{align}

These values can be transformed into the x, y coordinates of a Cartesian coordinate system with its origin in the centre of the Earth:
\begin{align}
    x_P = x_P' + d \\
    y_P = y_P' - c  
\end{align}

For the distances $c$ and $d$, the following relationships result from figure \ref{fig:RadarTrig_height}, with $h_0$ as the height of the centre of the antenna above mean sea level:
\begin{align}
    \frac{c+R_E + h_0}{R_{R}} &= \cos(\alpha)	\\
    \frac{d}{R_{R}} &= \sin(\alpha) 
\end{align}
		
Which lead to the following expressions for $c$ and $d$
\begin{align}
   d &= R_{R} \cdot \sin(\alpha) \\
    c &= R_{R} \cdot \cos(\alpha)  - (R_E + h_0) 
\end{align}

and for $x_P$ and $y_P$:
\begin{align}
    x_P &= R_{R} \cdot \cos(\kappa) +  R_{R} \cdot \sin(\alpha) \\
    y_P &= R_{R} \cdot \sin(\kappa) - R_{R} \cdot \cos(\alpha) + (R_E + h_0)
\end{align}
In this coordinate system, the height $z_p$ of the measured point P above the Earth's surface, can be calculated by subtracting the Earth's radius from the distance of the point from the centre of the Earth.
\begin{equation}
    z_p = \sqrt{x_P^2 + y_P^2} - R_E
    \label{eq:z_P}
\end{equation}
The angle $\Phi_E$ between the radar station, the centre of the Earth and the measuring point is given by the following relationship:
\begin{align}
\Phi_E = \arcsin{ (\frac{x_P}{\sqrt{x_P^2 + y_P^2}})}
\end{align}


An alternative method to calculate the height of the radar beam is described in the book by \textcite{rinehart_radar_2010} on pages 57ff. This method imagines a fictitious earth with an effective radius $R_F$, in which the radar beam can be assumed to be straight. 
In order to avoid complications by the transformation from the system of a fictitious earth into the  coordinate system of Met.3D, I decided to use the equations derived above, which are based on the actual curved ray path. A similar, but mathematically slightly different approach was applied by \textcite{fischer_interactive_2019}.

\subsubsection{Calculating the position along a radar beam intersecting with a given height}
The last section has shown how to determine the height for a position along the radar beam. In some cases, for example if one is interested in horizontal sections at a certain altitude, the opposite direction is also relevant: how to calculate at which position along a radar beam a certain height is reached. To resolve equation \ref{eq:z_P} for $s$ is mathematically complicated because of the many involved trigonometric functions. A simpler derivaftion is possible by using the already mentioned alternative method, which combines the beam curvature into the curvature of a fictitious earth with radius $R_F$ instead and which therefore can assume a straight-line beam path. According to \textcite{rinehart_radar_2010}, $R_F = R_R/3 \approx \frac{4}{3} \cdot R_E$. 

\begin{figure}[h]
\centering
\includegraphics[width=0.4\textwidth]{RadarTrigonometry_elevation1_with_beam_effective_earth.webp}
\caption{An alternative method to calculate the height imagines a fictitious earth with a larger effective radius $R_F$ correcting for the beam curvature, resulting in the radar beam following the straight purple line. This method provides an easier way to calculate the position $P$ along the radar beam where it intersects with a given height $z_p$. }
\label{fig:RadarTrig_range_from_height}
\end{figure}

Figure \ref{fig:RadarTrig_range_from_height} shows the relevant relations in this derivation. By application of the cosine theorem we get for $z_P$ the alternative equation

\begin{equation}
    z_P = \sqrt{(R_F + h_0)^2 + s^2 - 2 \cdot (R_F + h_0) \cdot s \cdot \cos{\gamma}} - R_F.
\end{equation}

It can be transformed into a quadratic equation which (ignoring the unphysical negative solution) can be resolved for s:

\begin{equation}
    s = (R_F + h_0) \cdot \cos{\gamma} + \sqrt{(R_F + h_0)^2 \cdot \cos{\gamma}^2 + 2 \cdot R_F \cdot (z_P - h_0) + z_P^2 - h_0^2}.
\end{equation}
Since $\gamma = 90° + \alpha$ and therefore $\cos(\gamma) = -\sin(\alpha)$, the position along the beam only depends on the Earth's radius, the height of the radar antenna, the elevation angle $\alpha$ and the altitude of the object P.

\begin{equation}
    s (z_P) = \sqrt{(R_F + h_0)^2 \cdot \sin{\alpha}^2 + 2  \cdot R_F \cdot (z_P - h_0) + z_P^2 - h_0^2} - (R_F + h_0) \cdot \sin{\alpha}
    \label{eq:height_to_range}
\end{equation}

\subsubsection{Transformation of the height coordinate into atmospheric pressure}
While the height of targets measured by radar is typically given in metres above sea level, 
in numerical weather simulations it is common to express the altitude in atmospheric pressure levels in hPa. To be able to display the radar data together with simulation data in Met.3D, the height $z_p$ therefore has to be converted into atmospheric pressure. 
For this conversion I use the \texttt{pressure2metre\_standardICAO} implementation in Met.3D, according to the profile of the ICAO standard atmosphere (\cite{kraus_atmosphare_2001}). %H.Kraus, Die Atmosphäre der Erde, Springer 2001, 470pp, Sections II.1.4 and II.6.1.2.

\subsubsection{Transforming to geographic coordinates}

\begin{figure}[h!]
\centering
\includegraphics[width=0.5\textwidth]{sphericalTriangle_new.webp}
\caption{Spherical triangle connecting the radar station, the projection of the measuring point on the Earth's surface, and the North Pole, from which the longitude $\lambda_P$ and the latitude $\varphi_P$ of the measuring point can be calculated.}
\label{fig:RadarTrig_triangle}
\end{figure}

The combination of the azimuth angle $\delta$ and the angle $\Phi_E$  between the radar station, the centre of the Earth and the measuring point, are now converted into the geographic coordinate system.
For this, one can imagine a spherical triangle on the Earth's surface (Fig. \ref{fig:RadarTrig_triangle}), whose vertices are the North Pole, the radar station and the projection of the measuring point onto the Earth's surface (P). We can apply the spherical law of cosines here, which relates the sides and angles of spherical triangles and reads in its general form as

\begin{equation}
    \cos(a) =  \cos(b)\cos(c) + \sin(b)\sin(c)\cos(A).
\end{equation}

In the spherical law of cosines, the sides of the triangle are expressed by midpoint angles (in our case the angle between both vertices and the centre of the Earth).  The edges between the North Pole and the radar station and between the North Pole and the measuring point depend on the respective latitudes of the station $\varphi_S$ and the measuring point $\varphi_P$: $a = 90\degree - \varphi_P$, $b = 90\degree - \varphi_S$ and $c$ is the angle $\Phi_E$ between the radar site and the measuring point, which was derived in the last section. With this information, the spherical law of cosines can be written as
\begin{equation}
   \cos(90\degree - \varphi_P) = \cos(90\degree - \varphi_S) \cdot \cos(\Phi_E)  + \sin(90\degree - \varphi_S) \cdot \sin(\Phi_E)  \cdot\cos(\delta).
\end{equation}
With the transformations between cosine and sine, $\cos(90\degree - x) = \sin(x)$ and $\sin(90\degree - x) = \cos(x)$, follows
\begin{equation}
    \sin( \varphi_P) = \sin(\varphi_S) \cdot \cos(\Phi_E)  + \cos(\varphi_S) \cdot \sin(\Phi_E) \cdot\cos(\delta).
\end{equation}
The \textbf{latitude} of the measuring point, $\varphi_P$, is therefore
\begin{align}
\varphi_P &= \arcsin(\sin(\varphi_S) \cdot \cos(\Phi_E)  + \cos(\varphi_S) \cdot \sin(\Phi_E) \cdot\cos(\delta)).
\end{align}

The general definition of the spherical law of sines is
\begin{equation}
    \frac{\sin(a)}{\sin (A)} = \frac{\sin(b) }{\sin(B)} = \frac{\sin(c) }{ \sin(C)}.
\end{equation}
This is used to calculate the angle between radar site and measuring point: %[TODO: wie ist der Winkel genau definiert? Zeichnung einfügen!]:
\begin{align}
    \sin(\gamma) &= \frac{\sin(\delta) \cdot \sin(\Phi_E) }{ \sin( 90\degree -  \varphi_P)} \\
	&= \frac{\sin(\delta) \cdot \sin(\Phi_E) }{ \cos(\varphi_P)}
\end{align}

\begin{equation}
    \gamma = \arcsin(\frac{\sin(\delta) \cdot \sin(\Phi_E) }{ \cos(\varphi_P)})
\end{equation}
This angle $\gamma$ has to be added to the longitude of the radar site to get the \textbf{longitude} of the measurement point, $\lambda_P$:
\begin{align}
   \lambda_P = \lambda_S + \gamma
\end{align}

Thus, the geographic coordinates of the measuring point ($\varphi_P$, $\lambda_P$) can be calculated from the radar coordinates and the geographic coordinates of the radar site.


\end{document}
