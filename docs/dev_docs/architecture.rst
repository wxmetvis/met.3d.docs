Architecture overview
=====================

An overview of the Met.3D architecture can be found 
in Chapter 5.3 of Marc's thesis, available at 
`<http://mediatum.ub.tum.de/doc/1253343/1253343.pdf>`_.


An overview over how Met.3D handles radar data can be found here  :doc:`/dev_docs/radar`.

The trajectory pipeline received an overhaul in Met.3D 1.13.
The pipeline was changed to a dynamical concept, where seed
points, point filters and trajectory filters can be configured
in Met.3D interactively. Details on the pipeline from a 
developer perspective can be found :doc:`here<architecture/trajectory_pipeline>`.


.. toctree:: 
    :maxdepth: 1
    :hidden:

    architecture/trajectory_pipeline
