Radar data in Met.3D for developers
===================================

This section gives an overview of how weather radar data is handled internally in Met.3D.

Radar Pipeline
--------------

Actors load radar data as an **MNWPActorVariable**. On top of other variable properties, 
radar variables also have MRadarProperties, and MRadarRegridProperties if they are regridded, for settings specific to radar data and regridding.

The radar variable data selected in the *add variable* dialog is loaded by **MDWDHDF5RadarReader** into an **MRadarGrid** object.
This grid comprises the data of all elevations of one variable/location/time step, 
and contains an instance of **MRadarElevation** for each elevation. MRadarElevation holds the data and metadata specific to this elevation sweep.


Rendering
---------

The radar actor iterates over all elevation in a radar grid and renders its grid 
cells as boxes. For each corner, elevation angle, azimuth angle and range along 
the radar beam are converted to longitude, latitude and pressure in the geometry shaders. 
The equations implemented in `radarToLonLatPressure` in `radar_utils.fx.glsl` to convert a point P are explained in the bachelor's thesis 
*3D Visualization of Weather Radar Data in Met.3D* by Susanne Fuchs in section 
2.1.6 *Determining the position of the target*.

.. figure:: ./figs/radar/RadarTrigo_angles_new.webp
    :figwidth: 54%
    :align: left
    :alt: Diagram of distances and angles needed to calculate pressure/height coordinate of P.

    Determining the pressure/height coordinate of P.

.. figure:: ./figs/radar/sphericalTriangle_new.webp
    :figwidth: 33%
    :align: left
    :alt: Diagram of distances and angles needed to calculate longitude and latitude of P.

    Determining longitude and latitude of P.


.. raw:: html

    <p style="clear: both;"></p>

Regridder: Radar to regular lon lat remapping
----------------------------------------------

To use all of Met.3D's actors for numerical weather prediction data, radar data can be interpolated to a regular lat lon grid, 
see :doc:`/user_docs/data_handling/remapping_radar_to_regular_grid`.

The approach used is a nearest neighbour interpolation that transforms each grid point in the 
regular grid to radar coordinates and evaluates which radar grid cell contains this point. 
It is implemented in `inverseTransformationInterpolation` in MRadarGrid and also in `lonLatWorldZToRadarIndices` in volume_radar_utils.fx.glsl.

Height :math:`z_p` is transformed to elevation angle :math:`\alpha`. For that, the transformation :math:`\alpha -> z_p`  described in the last section is solved for :math:`\alpha`:

(with *d* = direct distance between radar antenna and P)

.. math::
    \alpha = \arcsin(\frac{(R_e + h_0)^2 + d^2 - (R_e + z_p)^2}{2 * (R_e + h_0) * d}) -  \frac{\pi - \beta}{2}


Longitude and latitude are converted to an azimuth angle in function `gcAzimuth_deg` by calculating the bearing angle as described in 
https://www.movable-type.co.uk/scripts/latlong.html in section **Bearing**.

The range bin is received by calculating the length of the curved ray and dividing it by the distance between two bins.

.. math::
    \beta &= \arcsin(d / (2 * R_R)) * 2 \\
    rayDist &= \beta * R_R \\
    binIndex &= floor((rayDist - rangeStart)/ binDistance)


Direct Volume Rendering
-----------------------

DVR in the raycaster actor can be used for radar data regridded to a regular grid or radar 
data on the original grid. The first works the same as for NWP data.
In the second case, to sample the radar volume at a world position, `sampleRadarVolumeAtPos` 
in `volume_radar_utils.fx.glsl` also uses the inverse transformation in ``lonLatWorldZToRadarIndices`` to find the closest radar cell.
It uses a nearest neighbour approach.