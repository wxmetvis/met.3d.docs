GLSL coding style
=================

Where applicable, also use the above C++ style for coding GLSL shader programs.

General file structure
----------------------

Met.3D uses the `glfx framework`_. glfx unifies vertex, geometry and fragment 
shader programs in a single source file, a ``.glsl`` file. 
Use the same license and copyright header as for the C++ files. Following the
header, the general structure of the ``.glsl`` files in Met.3D is:

.. _glfx framework: https://code.google.com/archive/p/glfx/

.. code-block:: GLSL
    :linenos:

    /****************************************************************************
    ***                             CONSTANTS                                 ***
    *****************************************************************************/

    // Use the `const` datatype instead of `#define`.
    const float MISSING_VALUE = -999.E9;

    
    /****************************************************************************
    ***                             INTERFACES                                ***
    *****************************************************************************/

    // Interfaces connect different shader stages.
    interface GStoFS
    {
        smooth vec4 colour;
    };


    /****************************************************************************
    ***                             UNIFORMS                                  ***
    *****************************************************************************/

    // Definition of uniform variables common to all shader stages.
    uniform mat4 mvpMatrix;


    /****************************************************************************
    ***                             INCLUDES                                  ***
    *****************************************************************************/

    // Include files.
    #include "filename.glsl"

    /****************************************************************************
    ***                           VERTEX SHADER                               ***
    *****************************************************************************/

    shader VSmain(in vec2 worldXY, out vec3 worldPos)
    {
        // .. some code ..
    }


    /****************************************************************************
    ***                          GEOMETRY SHADER                              ***
    *****************************************************************************/

    shader GSmain(in vec3 worldPos[], out GStoFS output)
    {
        // .. some code ..
    }


    /****************************************************************************
    ***                          FRAGMENT SHADER                              ***
    *****************************************************************************/

    shader FSmain(in GStoFS input, out vec4 fragColor)
    {
        // .. some code ..
    }


    /****************************************************************************
    ***                             PROGRAMS                                  ***
    *****************************************************************************/

    // You can define multiple shader programs using the following syntax:
    //    vs(number_of_gl_version)=functionName();
    //    gs(number_of_gl_version)=functionName() : in(geom_in), out(geom_out, max_vertices=max, stream=num)
    //    fs(number_of_gl_version)=functionName();

    program SomeFancyOpenGlShader
    {
        vs(420)=VSmain();
        gs(420)=GSmain() : in(points), out(line_strip, max_vertices = 4);
        fs(420)=FSmain();
    };

Comments
--------

* Since there are no header files in GLSL, please put Doxygen-style comments
  for a function above the function definition.