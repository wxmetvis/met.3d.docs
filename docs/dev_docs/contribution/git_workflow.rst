GIT workflow
============

The Met.3D main repository is hosted on `GitLab`_, with a mirror maintained 
on `Bitbucket`_. We use the `issue tracker`_ on GitLab. 
We welcome bug and issue reports from users. If you want to report an issue,
please create an account on `GitLab`_ and contact us with your account. We will
add you to the project as an issue reporter.

There are many GIT workflows out there, as described in this GitLab
`overview article`_.
To contribute code to Met.3D, please fork the main repository, change the code
in a feature branch in your own fork, and create a merge request to feed your 
work back into the main repository. 
If you are working on a specific issue, don't forget to assign the issue to 
yourself in the issue tracker.
Read GitLab's `Project forking workflow`_ for more detailed instructions.

Also read and follow the GitLab `documentation for merge requests`_, issues, etc.
when working with the code. Our small difference is that your feature branches live in
your forked repository instead of the main repository.

.. _GitLab: https://gitlab.com/wxmetvis/met.3d
.. _Bitbucket: https://bitbucket.org/wxmetvis/met.3d
.. _issue tracker: https://gitlab.com/wxmetvis/met.3d/issues
.. _overview article: https://gitlab.com/help/workflow/gitlab_flow.md
.. _Project forking workflow: https://gitlab.com/help/workflow/forking_workflow.md
.. _documentation for merge requests: https://gitlab.com/help/workflow/gitlab_flow.md#mergepull-requests-with-gitlab-flow

GIT recipes
-----------

This section contains short recipes for common git tasks.

* Synchronize between different repositories

  - To update the master branch of your repository from the public main 
    repository, execute in your local clone:
    
    + ``git remote add public_main https://gitlab.com/wxmetvis/met.3d.git`` (only
      once, check with ``git remote -v`` if you have already added the remote repository)
    
    + ``git checkout master``
    
    + ``git fetch public_main master``
    
    + ``git merge FETCH_HEAD``
    
    + ``git push origin master``
    
  - To move a branch between two repositories A and B, assuming you are working
    in your local copy of A and want to copy branch ``example_branch`` from B
    to A. Assume that ``example_branch`` branches off ``master`` in B and ``master``
    is the same in A and B.
    
    + ``git remote add repo_B <URL of B>`` (only
      once, check with ``git remote -v`` if you have already added the remote repository)
      
    + ``git checkout master``
    
    + ``git checkout -b example_branch``
    
    + ``git fetch repo_B example_branch``
    
    + ``git merge FETCH_HEAD``