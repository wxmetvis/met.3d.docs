Toolbar & Icons
===============

The toolbar consists of icons, one for each actor. See the 
:ref:`toolbar user documentation<Toolbar User Documentation>` for details. 

If you would like to create a new icon for a (new) actor, make sure that the new toolbar icon

* is grayscale,
  
* has a size of 112 x 112 pixels,
  
* does not contain text or letters,
  
* does not contain fine structures,

* is simple and conveys the functionality of the actor in an abstract way.

You can create such icons using a variety of software solutions. We recommend creating
the icon as a vector graphic using `Inkscape <https://inkscape.org>`_, and exporting it 
as a PNG file with an optional alpha channel.

The icons are located in the repository in ``resources/icons/actors/``. If you implement
a new actor, just add the icon to the above folder, give it a meaningful name, and reimplement 
the ``MActor::getIconFileName()`` method to return the filename of the icon.