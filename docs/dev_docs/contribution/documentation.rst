Documentation
=============

This document will outline, how you can contribute to this online documentation.
Firstly though, if you never worked with a *Read the Docs* documentation, 
we recommend the following online resources to learn all 
about how you can edit *reStructuredText (.rst)* files:

* `Getting started with RST`_
* `Cheat Sheet`_ for the RST syntax and Sphinx


How to edit RST files
---------------------

This section will describe, how you can edit the ``.rst`` files with **Visual Studio Code**.
Even though the **Visual Studio Code** setup is described, any text editor can edit *reStructuredText*.


Installing Sphinx
~~~~~~~~~~~~~~~~~

Sphinx is needed to be able to compile and preview the RST files.
It can be installed using `conda`_.
Here, we use the `miniconda`_ distribution. 
We recommend installing miniconda into a directory that provides enough disk space 
(default is in you home directory, you may want to use a different directory).

Install miniconda::

    wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh
    chmod +x Miniconda3-latest-Linux-x86_64.sh
    ./Miniconda3-latest-Linux-x86_64.sh


After installing conda, you can create a conda environment for this documentaion, 
so that it won't interfere with any other conda environment. 
Afterwards you can activate the environment.

Create and activate Met.3D documentation conda environment::

    conda create -n met3d_docs
    conda activate met3d_docs


Afterwards, you can install the `sphinx` package using

Install Sphinx::

    conda install -c conda-forge sphinx


You would also need to install the *Read the Docs* sphinx theme, to correctly
preview the documentation. 
The theme will be installed using PIP, which should have been installed with Sphinx.

Install Read the Docs theme and the PDF embedding module::

    pip install sphinx_rtd_theme sphinx_simplepdf


Installing Visual Studio Code
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Visual Studio Code is available on their `website`_.
It can also be downloaded from the `Snap Store`_, if you use Ubuntu, for example.
After installing it, you can open the project folder with it.
In case of this documentation, you would open the ``docs`` folder in the repository.

.. note:: 

    Make sure you are within the created conda environment when using VS Code. On Linux,
    you can simply start VS Code from the command line with activated conda environment.
    On Windows, you can change the Python interpreter within VS code to the proper one.


Installing RST extensions
~~~~~~~~~~~~~~~~~~~~~~~~~

Visual Studio Code still needs some extensions to be able to edit RST files.
For that, head to the extension tab on the left hand task bar in the editor.

There install the following extensions:

* `reStructuredText`_: For RST language support.
* `reStructuredText Syntax highlighting`_: For syntax highlighting.
* `Esbonio`_: Server for auto-compiling and previewing RST files.
* `LTeX`_: To enable spell checking in the RST files.


After installing these extensions, go to *File → Preferences → Settings* and
search for ``esbonio.sphinx.buildDir``. There you can set the build directory of 
the resulting html files you can use for preview.


Editing the RST files
~~~~~~~~~~~~~~~~~~~~~

If everything is installed correctly, you should be able to edit the ``.rst`` files.
Every time you edit them, the **reStructuredText** extension will automatically build the html files.
You can also manually trigger a re-build, 
by pressing on the ``esbonio`` button on the lower right of the editor.


Contribution guidelines
-----------------------

This section will go over the contribution guidelines for the documentation.


File structure
~~~~~~~~~~~~~~

The documentation follows a simple file structure.
Each page, that has sub-pages, has to have a folder where all sub-pages belong to.

Example::

    index.rst
    user_docs
    |-getting_started.rst
    |-getting_started
    | |-first_steps.rst
    | |-figs

In the example, ``first_steps.rst`` is a sub-page of ``getting_started.rst``. 
That is, why it is located in the ``getting_started`` folder.

Figures are located in another sub-folder called ``figs``.
This folder contains all figures for the sub-pages of ``getting_started``.


Figures
~~~~~~~

Figures are always located in a ``figs`` folder next to the page.
Figures should be saved as a ``.webp`` file with lossy compression at low quality to save space
You can convert all images in a folder to this format by using image-magick's ``mogrify`` command:

Example::

    mogrify -format webp -quality 30 -define webp:lossless=false *.png

The example converts all ``.png`` files in the folder to lossy compressed ``.webp`` files.
The quality is set to 30%, though that depends on the image and its readability.

Generally, avoid uploading too large images.


.. _`Getting started with RST`: https://sphinx-tutorial.readthedocs.io/step-1/
.. _`Cheat Sheet`: https://sphinx-tutorial.readthedocs.io/cheatsheet/
.. _`website`: https://code.visualstudio.com/
.. _`Snap Store`: https://snapcraft.io/code
.. _`reStructuredText`: https://marketplace.visualstudio.com/items?itemName=lextudio.restructuredtext
.. _`reStructuredText Syntax highlighting`: https://marketplace.visualstudio.com/items?itemName=trond-snekvik.simple-rst
.. _`Esbonio`: https://marketplace.visualstudio.com/items?itemName=swyddfa.esbonio
.. _`LTeX`: https://marketplace.visualstudio.com/items?itemName=valentjn.vscode-ltex
.. _`conda`: https://docs.conda.io/projects/conda/en/latest/
.. _miniconda: https://docs.conda.io/en/latest/miniconda.html