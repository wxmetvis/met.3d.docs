Getting started
===============

This document will show you how to get up and running with Met.3D, and
provide you with a short overview of how to use the software. 
For installation, please follow the notes provided in the sections
:doc:`installation_qmake` and :doc:`installation_cmake`, depending on which
build system you prefer.
The following sections assume that the tool has been successfully installed.

:doc:`first_steps` contains a tutorial for the first steps with
the Met.3D. :doc:`actors` provides an overview of available
visualization modules. 

.. note::
    Please note that Met.3D is being developed within a research project. We
    do our best to fix bugs in the software. However, you may
    encounter bugs when using Met.3D. Please let us know about any bug you
    encounter, so we can improve the software. Please also regularly check
    the open-source repository for software updates and this page for user 
    guide updates. 
    
.. note::    
    There is more functionality in Met.3D (in part experimental) than described in
    this user guide. We will complete the user guide in the future.
